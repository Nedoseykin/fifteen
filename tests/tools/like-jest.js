function describe(description, callback) {
  callback.textIndent = describe.caller
    ? describe.caller.textIndent + 1
    : 0;

  return new Describe(description, callback);
}

function Describe(description, callback) {
  this.create(description, callback);
}

Describe.prototype.create = function describeCreate(description, callback) {
  this.description = description;
  this.callback = callback;
  this.textIndent = callback.textIndent;

  this
    .log()
    .callback();
}

Describe.prototype.log = function describeLog() {
  console.log('%c%s', `color: blue; font-weight: bold; ${
    this.textIndent ? `text-indent: ${this.textIndent * 16}px` : ''
  }`, this.description);

  return this;
}

function it(description, callback) {
  callback.textIndent = it.caller
    ? it.caller.textIndent + 1
    : 0;
  callback.description = description;

  return new It(description, callback);
}

function It(description, callback) {
  this.description =description;
  this.callback = callback;

  this.callback();
}

function expect(value) {
  const description = expect.caller.description;
  const textIndent = expect.caller.textIndent;

  return new Expect(value, description, textIndent);
}

function Expect(value, description, textIndent) {
  this.create(value, description, textIndent);
}

Expect.prototype.create = function assertCreate(value, description, textIndent) {
  this.value = value;
  this.description = description;
  this.textIndent = textIndent;

  return this;
}

Expect.prototype.toEqual = function assertToEqual(comparator, callback) {
  let passed = false;
  switch (true) {
    case Array.isArray(this.value) && Array.isArray(comparator):
      passed = toEqualArrays(this.value, comparator, callback);
      break;

    default:
  }

  return this.log(passed, this.description, this.textIndent);
}

Expect.prototype.toBe = function assertToBe(comparator) {
  return this.log(this.value === comparator, this.description, this.textIndent);
}

Expect.prototype.log = function expectLog(isPassed) {
  const color = isPassed ? 'green' : 'red';
  const textIndent = 16 * this.textIndent;
  const message = `${isPassed ? 'v PASSED': 'x FAILED'}: ${this.description}`;

  console.log('%c%s', `color: ${color}; text-indent: ${textIndent}px`, message);

  return this;
}

function toEqualArrays(arr1, arr2, getFilter) {
  return arr1.length === arr2.length
    && arr1.filter(getFilter(arr2)).length === arr1.length;
}
