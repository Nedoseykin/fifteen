let store;

function setup(state) {
  store = {
    getState() {
      return state;
    }
  };
}

describe('common-utils', function() {
  describe('validateMatrix', function() {
    describe('when matrix is 3x3', function() {
      setup({
        cellsInRow: 3,
      });

      it('should return `true`', function() {
        const matrix = [
          1, 2, 3,
          4, 5, 6,
          7, 8, 0,
        ];

        expect(validateMatrix(matrix)).toBe(true);
      });

      it('should return `true`', function() {
        const matrix = [
          1, 2, 3,
          4, 5, 0,
          6, 7, 8,
        ];

        expect(validateMatrix(matrix)).toBe(true);
      });

      it('should return `false`', function() {
        const matrix = [
          1, 2, 3,
          4, 5, 6,
          8, 7, 0,
        ];

        expect(validateMatrix(matrix)).toBe(false);
      });

      it('should return `false`', function() {
        const matrix = [
          8, 1, 6,
          5, 2, 4,
          0, 3, 7,
        ];

        expect(validateMatrix(matrix)).toBe(false);
      });
    });

    describe('when matrix is 4x4', function() {
      setup({
        cellsInRow: 4,
      });

      it('should return `true`', function() {
        const matrix = [
          1, 2, 3, 4,
          5, 6, 7, 8,
          9, 10, 11, 12,
          13, 14, 15, 0,
        ];

        expect(validateMatrix(matrix)).toBe(true);
      });

      it('should return `false`', function() {
        const matrix = [
          1, 2, 3, 4,
          5, 6, 7, 8,
          9, 10, 11, 0,
          12, 13, 14, 15,
        ];

        expect(validateMatrix(matrix)).toBe(false);
      });
    });
  });
});
