describe('list-utils', function() {
  describe('getItemsByParentId', function() {
    const item_3_2_children = [
      { id: 'Item_3_2_1' },
      { id: 'Item_3_2_2' },
      { id: 'Item_3_2_3' },
    ];

    const item_2_children = [
      { id: 'Item_2_1' },
      {
        id: 'Item_2_2',
        // children: item_3_2_children,
      },
    ]

    const item_3_children = [
      { id: 'Item_3_1' },
      {
        id: 'Item_3_2',
        children: item_3_2_children,
      },
    ]

    const items = [
      { id: 'Item_1' },
      {
        id: 'Item_2',
        children: item_2_children,
      },
      {
        id: 'Item_3',
        children: item_3_children,
      },
      { id: 'Item_4' },
    ];

    describe('when children of item exists', function() {
      it('should return children of item with id: `Item_3`', function() {
        const result = getItemsByParentId(items, 'Item_3');
        const expectedResult = item_3_children;

        expect(result).toEqual(expectedResult, arraysFilter);
      });

      it('should return children of item with id: `Item_3_2`', function() {
        const result = getItemsByParentId(items, 'Item_3_2');
        const expectedResult = item_3_2_children;

        expect(result).toEqual(expectedResult, arraysFilter);
      });

      it('should return children of item with id: `Item_2`', function() {
        const result = getItemsByParentId(items, 'Item_2');
        const expectedResult = item_2_children;

        expect(result).toEqual(expectedResult, arraysFilter);
      });
    });

    describe('when children of item is undefined', function() {
      it('should return empty array: []', function() {
        const result = getItemsByParentId(items, 'Item_1');
        const expectedResult = [];

        expect(result).toEqual(expectedResult, arraysFilter);
      });
    });
  });
});

function arraysFilter(arr2) {
  return function(arr1Item, i) {
    return arr1Item.id === arr2[i].id;
  }
}
