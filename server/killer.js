const path = require('path');
const fs = require('fs');
const ps = require('ps-node');

const fileName = path.resolve(__dirname, 'process', 'server.pid');
const isExists = fs.existsSync(fileName);

new Promise((resolve, reject) => {
  if (!isExists) {
    reject({ message: 'File is not found' });
  }
  fs.readFile(fileName, 'utf-8', (err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data);
    }
  });
})
  .then(pid => {
    ps.kill(pid, (err) => {
      if (err) {
        throw new Error(err);
      }

      console.log(`Process of server with PID: ${pid} has been killed`);
      process.exit(0);
    });
  })
  .catch(err => {
    console.log(err);
    process.exit(1);
  });
