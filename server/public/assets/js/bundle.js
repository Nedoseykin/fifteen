const REM = parseInt(getComputedStyle(document.documentElement)['font-size'], 10);

const FIELD = {
  CELLS_IN_ROW: 4,
  SIZE: 800,
};

const PLATE = {
  MAX_SPEED: 16,
};

const LAYOUT_TYPE = {
  IN_ORDER: 'IN_ORDER',
  MIXED: 'MIXED',
};

const GAME_MODES = {
  NOT_STARTED: 'NOT_STARTED',
  STARTED: 'STARTED',
  COMPLETED: 'COMPLETED',
};

const LOGIN_FORM_MODES = {
  LOGIN: 'LOGIN',
  REGISTRATION: 'REGISTRATION',
};
const URLS = {
  LOGIN: '/auth/login',
  REGISTER: '/auth/register',
  LOGOUT: '/auth/logout',
  SAVE_GAME: '/api/fifteen/v1/game',
  GET_GAMES_BY_HASH: '/api/fifteen/v1/games/hash',
  GET_GAMES_BY_SIZE: '/api/fifteen/v1/games/size'
};
const MODALS = {
  MAIN_MENU: 'MAIN_MENU',
  ACCOUNT_MENU: 'ACCOUNT_MENU',
  LOGIN_FORM: 'LOGIN_FORM',
  ERROR_MESSAGE: 'ERROR_MESSAGE',
  GALLERY: 'GALLERY',
};
function isExist(v) {
  return v !== undefined && v !== null;
}

// strings
function isFilledString(str) {
  return (typeof str === 'string') && str.length > 0;
}

// arrays
function isFilledArray(arr) {
  return Array.isArray(arr) && arr.length > 0;
}

function getLast(arr) {
  return isFilledArray(arr) ? arr[arr.length - 1] : null;
}

function getFirst(arr) {
  return isFilledArray(arr) ? arr[0] : null;
}

function addIfNotIncluded(arr, item) {
  return arr.includes(item) ? arr : arr.concat([item]);
}

function removeItem(arr, item) {
  return arr.filter(function(arrItem) { return arrItem !== item });
}

function removeLast(arr) {
  return arr.slice(0, arr.length - 1);
}

// asynchronous
function delay(ms) {
  return new Promise(function(resolve) {
    setTimeout(resolve, ms);
  });
}

// classNames
function getClassNamesList(namesList) {
  const list = Array.isArray(namesList)
    ? namesList
    : [namesList];
  return list.map(function(name) {
    return [name, true];
  });
}

/**
 * Checker - checks if key is in item and item[key] value equals to filter[key]
 * @typedef {function} Checker
 * @param {string} key
 * @param {Object} item
 * @param {Object} filter
 * @returns {boolean} The result of checking
 */

/**
 * findIndex
 * @param {Array} arr
 * @param {Object} filter
 * @param {Checker|undefined} checker
 * @returns {number} Index of item in the array or -1
 */
function findIndex(arr, filter, checker) {
  if (!(isFilledArray(arr) && filter)) {
    return -1;
  }

  const keys = Object.keys(filter);

  for (let i = 0; i < arr.length; i++) {
    let test = true;

    for (let j = 0; j < keys.length; j++) {
      const key = keys[j];
      if (checker) {
        test = checker(key, arr[i], filter);
      } else {
        test = (key in arr[i]) && (filter[key] === arr[i][key]);
      }
      if (test === false) {
        break;
      }
    }

    if (test === true) {
      return i;
    }
  }

  return -1;
}

function findItem(arr, filter, checker) {
  const i = findIndex(arr, filter, checker);

  return i > -1 ? arr[i] : null;
}

function randomFromRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function platesToMatrix(plates) {
  const cellsInRow = Math.sqrt(plates.length + 1);

  return Array(Math.pow(cellsInRow, 2))
    .fill(0)
    .map(function(value, i) {
      const fieldY = Math.floor(i / cellsInRow);
      const fieldX = i - fieldY * cellsInRow;
      const plate = findItemByProps(
        plates,
        { fieldX: fieldX, fieldY: fieldY },
      );

      return plate ? plate.props.value : value;
    });
}
function obj(value) {
  return new Obj(value);
}

function Obj(value) {
  this.create(value);
}

Obj.prototype.create = function objCreate(value) {
  this.value = value || {};

  return this.value;
}

Obj.prototype.set = function objSet(model, value) {
  this.value[model] = value;

  return this.value;
};
function getAncestor(descendant) {
  return Object.getPrototypeOf(descendant.prototype);
}
/**
 * returns children of menu item by item`s id
 * @param {Array} items
 * @param {string} parentId
 * @returns {Array} Children of item by item`s id
 */
function getItemsByParentId(items, parentId) {
  if (!parentId) {
    return items;
  }

  let result = [];

  if (isFilledArray(items)) {
    for (let i = 0; i < items.length; i++) {
      if (items[i].id === parentId) {
        result = isFilledArray(items[i].children)
          ? items[i].children
          : [];
      } else if (Array.isArray(items[i].children)) {
        result = getItemsByParentId(items[i].children, parentId);
      }
      if (isFilledArray(result) || items[i].id === parentId) {
        break;
      }
    }
  }

  return result;
}
function roundedRect(context, cx, cy, width, height, radius, isFilled) {
  const x = cx - width / 2;
  const y = cy - height / 2;

  context.beginPath();
  context.moveTo(x, y + radius);
  context.lineTo(x, y + height - radius);
  context.arcTo(x, y + height, x + radius, y + height, radius);
  context.lineTo(x + width - radius, y + height);
  context.arcTo(x + width, y + height, x + width, y + height-radius, radius);
  context.lineTo(x + width, y + radius);
  context.arcTo(x + width, y, x + width - radius, y, radius);
  context.lineTo(x + radius, y);
  context.arcTo(x, y, x, y + radius, radius);
  context.closePath();
  context.stroke();
  if (isFilled) {
    context.fill();
  }
}

function getFieldCoordinates(clientX, clientY, element) {
  const cellsInRow = store.getState().cellsInRow;
  const rect = element.getBoundingClientRect();
  const x = clientX - rect.left;
  const y = clientY - rect.top;
  const size = rect.width / cellsInRow;
  const fieldX = Math.floor(x / size);
  const fieldY = Math.floor(y / size);

  return { x: fieldX, y: fieldY };
}

/**
 * Draw field with squares by hash in given context
 * @param {string} hash Hash of layout
 * @param {Object} ctx Context of canvas
 * @returns {void}
 */
function drawField(hash, ctx) {
  const layout = hash.split('-')
    .map(function(value) { return parseInt(value, 10); });
  const cellsInRow = Math.sqrt(layout.length);
  const size = 800 / cellsInRow;
  layout.forEach(function(value, i) {
    if (value > 0) {
      const row = Math.floor(i / cellsInRow)
      const cy = row * size + size / 2;
      const cx = (i - row * cellsInRow) * size + size / 2;
      ctx.fillStyle = 'rgba(255, 255, 255, 1)';
      roundedRect(ctx, cx, cy, size - 4, size - 4, 8, true);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.font = `bold ${size / 2}px sans-serif`;
      ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
      ctx.fillText(`${value}`, cx, cy + 2);
      const isProperPosition = value === i + 1;
      if (isProperPosition) {
        ctx.fillStyle = 'gold';
        ctx.fillText(`${value}`, cx - 2, cy);
      }
    }
  });
}
/**
 * Layout - Array of numbers. Length equals to square of `cellsInRow`
 * @typedef {number[]} Layout
 */

/**
 * Creates random layout
 * @param {number} cellsInRow
 * @returns {number[]} Random layout
 */
function createRandomLayout(cellsInRow) {
  const count = Math.pow(cellsInRow, 2);
  const layout = Array(count).fill(0);
  for (let i = 0; i < count - 1; i++) {
    let value = 0;
    do {
      value = randomFromRange(1, count - 1);
    } while(layout.includes(value));

    let j = 0;
    do {
      j = randomFromRange(0, count - 1);
    } while (layout[j] > 0);
    layout[j] = value;
  }

  return layout;
  // return [4, 12, 9, 3, 10, 8, 13, 2, 1, 7, 0, 14, 6, 11, 5, 15];
}

/**
 * Validates given layout whether it solvable or not
 * @param {number[]} layout
 * @returns {boolean} Validation result
 */
function validateLayout(layout) {
  const cellsInRow = Math.sqrt(layout.length);
  const k = Math.floor(layout.indexOf(0) / cellsInRow) + 1;

  const n = layout
    .filter(function(item) { return item > 0; })
    .reduce(function(acc, value, i, arr) {
      const inc = arr
        .slice(i + 1, arr.length)
        .filter(function(item) {
          return item < arr[i];
        }).length;

      return acc + inc;
    }, 0);

  return  cellsInRow % 2 === 0
    ? (n + k) % 2 === 0
    : n % 2 === 0;
}

/**
 * Returns random solvable layout
 * @param {number} cellsInRow
 * @returns {number[]} Solvable layout
 */
function getValidLayout(cellsInRow) {
  let layout = createRandomLayout(cellsInRow);

  while(!validateLayout(layout)) {
    const i = layout.indexOf(0);
    layout.splice(i, 1);
    const lastIndex = layout.length - 1;
    const beforeLastIndex = layout.length - 2;
    const last = getLast(layout);
    layout[lastIndex] = layout[beforeLastIndex];
    layout[beforeLastIndex] = last;
    layout.splice(i, 0, 0);
  }

  return layout;
}

function getHashFromLayout(layout) {
  return isFilledArray(layout) ? layout.join('-') : '';
}

function getLayoutFromHash(hash) {
  return isFilledString(hash)
    ? hash.split('-').map(function(value) { return parseInt(value, 10) })
    : [];
}

/* function getLastLayoutFromState(state) {
  const lastAction = getLast(state.actions) || { matrix: [] };

  return lastAction.matrix;
} */

function getFirstLayoutFromState(state) {
  const lastAction = getFirst(state.actions) || { matrix: [] };

  return lastAction.matrix;
}
/**
 * Layout - array of numbers
 * @typedef {number[]} Layout
 */

/**
 * Converts Layout to array of plates or creates array of plates in order
 * @param{Layout|undefined} layout
 * @returns {Plate[]} Array of plates
 */
function getPlates(layout) {
  const cellsInRow = store.getState().cellsInRow;
  let arr = layout
    || new Array(Math.pow(cellsInRow, 2) - 1).fill(null);

  const size = FIELD.SIZE / cellsInRow;

  return arr
    .map(function(item, i) {
      if (item === 0) {
        return null;
      }
      const value = item || i + 1;
      const y = Math.floor(i / cellsInRow);
      const x = i - cellsInRow * y;

      return new Plate({
        cx: x * size + size / 2,
        cy: y * size + size / 2,
        fieldX: x,
        fieldY: y,
        size: size,
        value: value,
      });
    })
    .filter(Boolean);
}

function moveSelectedValue(state, deltaX, deltaY) {
  let selectedValue = state.selectedValue;
  let plate = state.plates.find(function(item) {
    return item.props.value === selectedValue;
  });
  let x = plate.props.fieldX;
  let y = plate.props.fieldY;
  do {
    x = correctCoordinate(x + deltaX);
    y = correctCoordinate(y + deltaY);
    plate = state.plates.find(function(item) {
      return item && item.props.fieldX === x && item.props.fieldY === y;
    });
  } while (!plate);

  return plate.props.value;
}

function correctCoordinate(coordinate, checkEdge) {
  const cellsInRow = store.getState().cellsInRow;
  let result = coordinate;
  if (result < 0) {
    result = checkEdge ? 0 : cellsInRow - 1;
  }
  if (result > cellsInRow - 1) {
    result = checkEdge ? cellsInRow - 1 : 0;
  }

  return result;
}

function filterByProp(items, filter) {
  return items.filter(filter);
}

function findIndexByProp(items, field, value) {
  return items.findIndex(function(item) {
    return item.props[field] === value;
  });
}

function findByProp(items, field, value) {
  return items.find(function(item) {
    return item.props[field] === value;
  });
}

function createFilterByProp(prop) {
  return function(selectedItem) {
    return function(item) {
      return item.props[prop] === selectedItem.props[prop];
    }
  };
}

function propsChecker(key, item, filter) {
  return (key in item.props) && (item.props[key] === filter[key]);
}

function findItemByProps(arr, filter) {
  return findItem(arr, filter, propsChecker);
}

function getMovableValues(selectedPlate, group, field, delta) {
  let movableGroup = [selectedPlate];
  let nextCoordinate = selectedPlate.props[field];
  let nextPlate = null;

  do {
    nextPlate = findByProp(group, field, nextCoordinate + delta);
    if (nextPlate) {
      movableGroup.push(nextPlate);
      nextCoordinate = nextPlate.props[field];
    }
  } while (nextPlate);

  const lastPlate = getLast(movableGroup);
  const newCoordinate = correctCoordinate(lastPlate.props[field] + delta, true);

  return newCoordinate !== lastPlate.props[field]
    ? movableGroup.map(function(item) { return item.props.value })
    : [];
}

function getFullscreenElement() {
  return  document.fullscreenElement
    || document.mozFullScreenElement
    || document.msFullscreenElement
    || document.webkitFullscreenElement;
}

function toggleScreenMode() {
  const fullscreenElement = getFullscreenElement();

  if (!fullscreenElement) {
    document.documentElement
      .requestFullscreen()
      .then(() => {});
  } else {
    if (document.exitFullscreen) {
      document
        .exitFullscreen()
        .then(() => {});
    }
  }
}

/**
 * Action
 * @typedef {Object} Action
 * @property {number} selectedValue
 * @property {number[]} matrix
 */

/**
 * Encodes actions
 * @param {Action[]} actions
 * @returns {string} Encoded string
 */
function prepareToSave(actions) {
  const prepared = {
    start: actions[0],
    movements: actions
      .slice(1, actions.length)
      .map(function(action) { return action.selectedValue; }),
    end: getLast(actions),
  };

  return JSON.stringify(prepared);
}

/**
 * Returns a type of layout. Whether it is in order or mixed
 * @param {number[]} layout Layout
 * @returns {string} A type of layout
 */
function getTypeOfLayout(layout) {
  let result = LAYOUT_TYPE.IN_ORDER;

  for (let i = 0; i < layout.length; i++) {
    const value = layout[i];

    if (value > 0 && value !== i + 1) {
      result = LAYOUT_TYPE.MIXED;
      break;
    }
  }

  return result;
}
function request(url, options) {
  const cbSuccess = options.cbSuccess;
  const cbError = options.cbError;
  const fetchOptions = {
    method: options.method || 'GET',
    headers: options.headers || {},
    body: options.body,
  };

  fetchOptions.headers.accepts = 'application/json';

  let responseOk;
  let responseStatus;

  return fetch(url, fetchOptions)
    .then(function(res) {
      responseOk = res.ok;
      responseStatus = res.status;
      const contentType = res.headers.get('content-type');
      const sessionToken = res.headers.get('session-token');

      if (contentType.includes('json')) {
        return res.json();
      }
      if (contentType.includes('text') && !sessionToken) {
        return res.text();
      }
      return Promise.resolve(sessionToken);
    })
    .then(loadedData => {
      if (!responseOk) {
        throw {
          status: responseStatus || 500,
          message: loadedData && loadedData.message || 'Network error',
        };
      }
      return cbSuccess && cbSuccess(loadedData);
    })
    .catch(err => {
      return cbError ? cbError(err) : console.error(err);
    });
}

function catchNetworkError(payload) {
  const _payload = payload || {};
  return function(err) {
    const state = store.getState();
    Object.keys(_payload).forEach(function(key) {
      state[key] = payload[key];
    });

    const errorTitle = 'Network request error';

    if (err.status === 401) {
      state.auth.sessionToken = '';
      localStorage.removeItem('sessionToken');
    }

    const modals = state.modals.includes(MODALS.ERROR_MESSAGE)
      ? state.modals
      : state.modals.concat([MODALS.ERROR_MESSAGE]);

    return Object.assign({}, state, {
      errorMessages: state.errorMessages.concat([{
        title: errorTitle,
        message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
      }]),
      modals: modals,
    });
  }
}

function setAuthData(state, payload) {
  return request(URLS.LOGIN, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
    cbSuccess: function(loadedData) {
      const sessionToken = typeof loadedData === 'string' ? loadedData : '';
      localStorage.setItem('login', payload.login);
      localStorage.setItem('sessionToken', sessionToken)

      return loadedData && loadedData.isValid === false
        ? Object.assign({}, state, {
          auth: {
            login: payload.login,
            validation: loadedData,
          },
        })
        : Object.assign({}, state, {
          modals: [],
          auth: {
            login: payload.login,
            sessionToken: loadedData,
          },
        });
    },
    cbError: function(err) {
      return Object.assign({}, state, {
        auth: {
          login: payload.login,
          sessionToken: '',
          error: err,
        },
        errorMessages: state.errorMessages.concat([{
          title: 'Error',
          message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
        }]),
        modals: state.modals.concat([MODALS.ERROR_MESSAGE]),
      });
    }
  });
}

function setRegisterData(state, payload) {
  return request(
    URLS.REGISTER,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
      cbSuccess: function (loadedData) {
        const sessionToken = typeof loadedData === 'string' ? loadedData : '';
        localStorage.setItem('login', payload.login);
        localStorage.setItem('sessionToken', sessionToken)

        return loadedData && loadedData.isValid === false
          ? Object.assign({}, state, {
            auth: {
              login: payload.login,
              email: payload.email,
              password: payload.password,
              confirmPassword: payload.confirmPassword,
              validation: loadedData,
            },
          })
          : Object.assign({}, state, {
            modals: [],
            auth: {
              login: payload.login,
              sessionToken: loadedData,
            },
          });
      },
      cbError: function (err) {
        return Object.assign({}, state, {
          auth: {
            login: payload.login,
            sessionToken: '',
            error: err,
          },
          errorMessages: state.errorMessages.concat([{
            title: 'Error',
            message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
          }]),
          modals: state.modals.concat([MODALS.ERROR_MESSAGE]),
        });
      },
    },
  )
}

function logout(state) {
  localStorage.removeItem('sessionToken');
  const sessionToken = state.auth.sessionToken;
  return request(URLS.LOGOUT, {
    method: 'PUT',
    headers: {
      'session-token': sessionToken,
    },
    cbSuccess: function() {
      const currState = store.getState();

      return Object.assign({}, currState, {
        auth: {
          login: currState.auth.login,
          sessionToken: '',
          error: null,
        },
        games: [],
      });
    },
    cbError: function(err) {
      return Object.assign({}, store.getState(), {
        auth: {
          login: state.auth.login,
          sessionToken: '',
          error: err,
        },
        errorMessages: state.errorMessages.concat([{
          title: 'Network error',
          message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
        }]),
        modals: state.modals.concat([MODALS.ERROR_MESSAGE]),
      })
    },
  });
}

function saveGame(state, game) {
  const sessionToken = state.auth.sessionToken;
  const gameMode = state.gameMode;
  const layoutType = state.layoutType;
  const isSaved = state.isSaved;
  const isValid = gameMode === GAME_MODES.COMPLETED
    && layoutType === LAYOUT_TYPE.MIXED
    && isSaved === false;

  return isValid
    ? request(URLS.SAVE_GAME, {
      method: 'POST',
      headers: {
        'session-token': sessionToken,
        'Content-Type': 'application/json',
      },
      body: game,
      cbSuccess: function() {
        return Object.assign({}, store.getState(), {
          modals: [],
          isSaved: true,
        });
      },
      cbError: catchNetworkError(),
    })
    : state;
}

function getGamesByCurrentLayout(state) {
  const layout = isFilledArray(state.actions)
    ? state.actions[0].matrix
    : [];

  const hashBase64 = btoa(getHashFromLayout(layout));

  const url = `${URLS.GET_GAMES_BY_HASH}/${hashBase64}`;

  return request(url, {
    headers: {
      'session-token': state.auth.sessionToken,
    },
    cbSuccess: function(games) {
      return Object.assign({}, store.getState(), {
        games: games,
        gamesLoadStatus: 2
      })
    },
    cbError: catchNetworkError({
      games: [],
      gamesLoadStatus: 3,
    }),
  });
}

function getGamesBySize(state) {
  const url = `${URLS.GET_GAMES_BY_SIZE}/${state.cellsInRow}`;
  return request(url, {
    headers: {
      'session-token': state.auth.sessionToken,
    },
    cbSuccess: function(gallery) {
      return Object.assign({}, store.getState(), {
        gallery: gallery,
        galleryLoadStatus: 2,
      });
    },
    cbError: catchNetworkError({
      gallery: [],
      galleryLoadStatus: 3,
    }),
  });
}
/**
 * Sets plates and adds new action to state
 * @param {Object} state
 * @param {Plate[]} plates
 * @returns {Object} New state
 */
function setPlates(state, plates) {
  const newState = Object.assign({}, state, {
    plates: plates,
  });
  return Object.assign({}, newState, {
    actions: addAction(newState),
  });
}

function setLayout(state, layout) {
  const newLayout = layout || getValidLayout(state.cellsInRow);

  return Object.assign({}, state, {
    plates: getPlates(newLayout),
    actions: [{ selectedValue: 1, matrix: newLayout }],
    canceledActions: [],
    gameMode: GAME_MODES.STARTED,
    layoutType: getTypeOfLayout(newLayout),
    isSaved: false,
  });
}

function moveSelection(state, deltaX, deltaY) {
  return state.gameMode === GAME_MODES.STARTED
    ? Object.assign({}, state, {
        selectedValue: moveSelectedValue(state, deltaX, deltaY),
      })
    : state;
}

function movePlatesValue(state, deltaX, deltaY) {
  if (state.gameMode === GAME_MODES.COMPLETED) {
    return state;
  }

  let plates = state.plates.slice();

  const selectedPlate = findByProp(plates, 'value', state.selectedValue);

  // get group
  const groupFilter = deltaX !== 0
    ? createFilterByProp('fieldY')
    : createFilterByProp('fieldX');
  const group = filterByProp(plates, groupFilter(selectedPlate));

  // get movable values
  const delta = deltaX !== 0 ? deltaX : deltaY;
  const field = deltaX !== 0 ? 'fieldX' : 'fieldY';
  const movableValues = getMovableValues(selectedPlate, group, field, delta);

  // change props in movable plates in plates
  movableValues.forEach(function(value) {
    const i = findIndexByProp(plates, 'value', value);
    const patch = {};
    patch[field] = plates[i].props[field] + delta;
    plates[i].setProps(patch);
  });

  const gameMode = (state.layoutType === LAYOUT_TYPE.MIXED)
    && (getTypeOfLayout(platesToMatrix(state.plates)) === LAYOUT_TYPE.IN_ORDER)
      ? GAME_MODES.COMPLETED
      : state.gameMode;

  // return plates;
  return Object.assign({}, state, {
    plates: plates,
    actions: movableValues.length > 0
      ? addAction(state)
      : state.actions,
    gameMode: gameMode,
  });
}

function addAction(state) {
  return state.actions.slice().concat([{
    selectedValue: state.selectedValue,
    matrix: platesToMatrix(state.plates),
  }]);
}

function undoAction(state) {
  let actions = state.actions;
  const canceledActions = state.canceledActions;
  if (actions.length > 1) {
    const canceled = getLast(actions);
    actions = actions.slice(0, actions.length - 1);
    canceledActions.push(canceled);
  }

  const selectedValue = doLastAction(actions, state.plates);

  return Object.assign({}, state, {
    selectedValue: selectedValue,
    actions: actions,
    canceledActions: canceledActions,
  });
}

function redoAction(state) {
  let canceledActions = state.canceledActions;
  const actions = state.actions;
  if (canceledActions.length > 0) {
    const renewed = getLast(canceledActions);
    canceledActions = canceledActions.slice(0, canceledActions.length - 1);
    actions.push(renewed);
  }

  const selectedValue = doLastAction(actions, state.plates);

  return Object.assign({}, state, {
    selectedValue: selectedValue,
    canceledActions: canceledActions,
    actions: actions,
  });
}

/**
 * Sets props to plates based on last action matrix
 * Matrix items should have proper indexes to calculate x and y
 * @param {Object[]} actions
 * @param {Plate[]} plates
 * @returns {number} Selected value
 */
function doLastAction(actions, plates) {
  const action = getLast(actions);
  const cellsInRow = Math.sqrt(action.matrix.length);

  if (action) {
    action.matrix
      .forEach(function(value, i) {
        if (value > 0) {
          const fieldY = Math.floor(i / cellsInRow);
          const fieldX = i - fieldY * cellsInRow;

          const plateIndex = findIndexByProp(plates, 'value', value);

          plates[plateIndex].setProps({
            value: value,
            fieldX: fieldX,
            fieldY: fieldY,
          });
        }
      });
  }

  return action && action.selectedValue;
}

function moveByClick(state, fieldCoordinates) {
  const selectedPlate = findItemByProps(state.plates, {
    fieldX: fieldCoordinates.x,
    fieldY: fieldCoordinates.y,
  })

  if (!selectedPlate) {
    return state;
  }

  return getMovedPlates(state, selectedPlate);
}

function getMovedPlates(state, selectedPlate) {
  let newState = Object.assign({}, state, {
    selectedValue: selectedPlate.props.value,
  });

  const options = [
    { deltaX: -1, deltaY: 0 }, // left
    { deltaX: 1, deltaY: 0 }, // right
    { deltaX: 0, deltaY: -1 }, // up
    { deltaX: 0, deltaY: 1 }, // down
  ];

  for (let i = 0; i < options.length; i++) {
    const deltaX = options[i].deltaX;
    const deltaY = options[i].deltaY;
    const actionsLength = newState.actions.length;
    newState = movePlatesValue(newState, deltaX, deltaY);
    if (newState.actions.length > actionsLength) {
      break;
    }
  }

  return newState;
}

function setLoadedGame(state, loadedGame) {
  let newState = Object.assign({}, state, {
    selectedValue: 1,
    plates: getPlates(loadedGame.start.matrix),
    actions: [{
      selectedValue: 1,
      matrix: loadedGame.start.matrix,
    }],
    canceledActions: [],
  });

  loadedGame.movements.forEach(function(value) {
    newState = getMovedPlates(newState, findByProp(newState.plates, 'value', value))
  });

  return newState;
}
const EVENTS = {
  RESIZE_WINDOW: 'RESIZE_WINDOW',
  FULLSCREEN_CHANGE: 'FULLSCREEN_CHANGE',
};
function EventEmitter() {
  this.handlers = {};
}

EventEmitter.prototype.emit = function eventEmitterEmit(eventName, value) {
  const eventHandlers = this.handlers[eventName];
  if (Array.isArray(eventHandlers)) {
    eventHandlers.forEach(function(handler) {
      handler(value);
    });
  }

  return this;
}

EventEmitter.prototype.addEventListener = function eventEmitterAddEventListener(eventName, handler) {
  if (!Array.isArray(this.handlers[eventName])) {
    this.handlers[eventName] = [handler];
  } else if (!this.handlers[eventName].includes(handler)) {
    this.handlers[eventName].push(handler);
  }

  return this;
}

EventEmitter.prototype.clear = function eventEmitterClear() {
  this.handlers = {};

  return this;
}
const initialState = {
  plates: [],
  selectedValue: 1,
  actions: [],
  canceledActions: [],
  modals: [],
  errorMessages: [],
  cellsInRow: FIELD.CELLS_IN_ROW,
  layoutType: '',
  gameMode: GAME_MODES.NOT_STARTED,
  isSaved: false,
  games: [],
  gamesLoadStatus: 2,
  gallery: [],
  galleryLoadStatus: 2,
  auth: {
    login: localStorage.getItem('login') || '',
    sessionToken: localStorage.getItem('sessionToken') || '',
    loadStatus: 2,
  },
  isTouchScreen: false,
};
const AC_SET_STATE = 'AC_SET_STATE';
const AC_RESET_APP = 'AC_RESET_APP';
const AC_SET_PLATES = 'AC_SET_PLATES';
const AC_SET_LAYOUT = 'AC_SET_LAYOUT';
const AC_SELECTED_TO_LEFT = 'AC_SELECTED_TO_LEFT';
const AC_SELECTED_TO_RIGHT = 'AC_SELECTED_TO_RIGHT';
const AC_SELECTED_TO_UP = 'AC_SELECTED_TO_UP';
const AC_SELECTED_TO_DOWN = 'AC_SELECTED_TO_DOWN';
const AC_PLATES_TO_LEFT = 'AC_PLATES_TO_LEFT';
const AC_PLATES_TO_RIGHT = 'AC_PLATES_TO_RIGHT';
const AC_PLATES_TO_UP = 'AC_PLATES_TO_UP';
const AC_PLATES_TO_DOWN = 'AC_PLATES_TO_DOWN';
const AC_UNDO_ACTION = 'AC_UNDO_ACTION';
const AC_REDO_ACTION = 'AC_REDO_ACTION';
const AC_MOVE_BY_CLICK = 'AC_MOVE_BY_CLICK';
const AC_OPEN_MODAL = 'AC_OPEN_MODAL';
const AC_CLOSE_MODAL = 'AC_CLOSE_MODAL';
const AC_CLOSE_LAST_MODAL = 'AC_CLOSE_LAST_MODAL';
const AC_CLOSE_ALL_MODALS = 'AC_CLOSE_ALL_MODALS';
const AC_REMOVE_LAST_ERROR_MESSAGE = 'AC_REMOVE_LAST_ERROR_MESSAGE';
const AC_SET_FIELD_SIZE = 'AC_SET_FIELD_SIZE';
const SET_LOADED_GAME = 'SET_LOADED_GAME';
const SET_AUTH_LOAD_STATUS = 'SET_AUTH_LOAD_STATUS';
const SET_AUTH_DATA = 'SET_AUTH_DATA';
const SET_REGISTER_DATA = 'SET_REGISTER_DATA';
const AC_LOGOUT = 'AC_LOGOUT';
const AC_SAVE_GAME = 'AC_SAVE_GAME';
const AC_GET_GAMES_BY_CURRENT_LAYOUT = 'AC_GET_GAMES_BY_CURRENT_LAYOUT';
const AC_SET_GAMES_LOAD_STATUS = 'AC_SET_GAMES_LOAD_STATUS';
const AC_GET_GALLERY = 'AC_GET_GALLERY';
const AC_SET_GALLERY_LOAD_STATUS = 'AC_SET_GALLERY_LOAD_STATUS';
const AC_SET_IS_TOUCH_SCREEN = 'AC_SET_IS_TOUCH_SCREEN';

function acSetState(state) {
  return { type: AC_SET_STATE, payload: state };
}

function acResetApp() {
  return { type: AC_RESET_APP };
}

function acSetPlates(plates) {
  return { type: AC_SET_PLATES, payload: plates };
}

function acSetLayout(layout) {
  return { type: AC_SET_LAYOUT, payload: layout };
}

function acSelectedToLeft() {
  return { type: AC_SELECTED_TO_LEFT };
}

function acSelectedToRight() {
  return { type: AC_SELECTED_TO_RIGHT };
}

function acSelectedToUp() {
  return { type: AC_SELECTED_TO_UP };
}

function acSelectedToDown() {
  return { type: AC_SELECTED_TO_DOWN };
}

function acPlatesToLeft() {
  return { type: AC_PLATES_TO_LEFT };
}

function acPlatesToRight() {
  return { type: AC_PLATES_TO_RIGHT };
}

function acPlatesToUp() {
  return { type: AC_PLATES_TO_UP };
}

function acPlatesToDown() {
  return { type: AC_PLATES_TO_DOWN };
}

function acUndoAction() {
  return { type: AC_UNDO_ACTION };
}

function acRedoAction() {
  return { type: AC_REDO_ACTION };
}

function acSelectByCoordinate(x, y) {
  return { type: AC_MOVE_BY_CLICK, payload: { x: x, y: y } };
}

function acOpenModal(modalId) {
  return { type: AC_OPEN_MODAL, payload: modalId };
}

function acCloseModal(modalId) {
  return { type: AC_CLOSE_MODAL, payload: modalId };
}

function acCloseLastModal() {
  return { type: AC_CLOSE_LAST_MODAL };
}

function acCloseAllModals() {
  return { type: AC_CLOSE_ALL_MODALS };
}

function acRemoveLastErrorMessage() {
  return { type: AC_REMOVE_LAST_ERROR_MESSAGE };
}

function acSetFieldSize(size) {
  return { type: AC_SET_FIELD_SIZE, payload: size };
}

function acSetLoadedGame(loadedGame) {
  return { type: SET_LOADED_GAME, payload: loadedGame };
}

function acSetAuthLoadStatus(status) {
  return {
    type: SET_AUTH_LOAD_STATUS,
    payload: status,
  };
}

function acSetAuthData(login, password) {
  return {
    type: SET_AUTH_DATA,
    payload: {
      login: login,
      password: password,
    },
  };
}

function acSetRegisterData(login, email, password, confirmPassword) {
  return {
    type: SET_REGISTER_DATA,
    payload: {
      login: login,
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    },
  };
}

function acLogout() {
  return { type: AC_LOGOUT };
}

function acSaveGame(game) {
  return { type: AC_SAVE_GAME, payload: game };
}

function acGetGamesByCurrentLayout() {
  return { type: AC_GET_GAMES_BY_CURRENT_LAYOUT };
}

function acSetGamesLoadStatus(loadStatus) {
  return { type: AC_SET_GAMES_LOAD_STATUS, payload: loadStatus };
}

function acGetGallery() {
  return { type: AC_GET_GALLERY };
}

function acSetGalleryLoadStatus(loadStatus) {
  return { type: AC_SET_GALLERY_LOAD_STATUS, payload: loadStatus };
}

function acSetIsTouchScreen(isTouchScreen) {
  return { type: AC_SET_IS_TOUCH_SCREEN, payload: isTouchScreen };
}
function reducer(state, action) {
  const type = action.type;
  const payload = action.payload;

  switch (type) {
    case AC_SET_STATE:
      return Object.assign({}, state, payload);

    case AC_RESET_APP:
      return Object.assign({}, initialState, {
        cellsInRow: state.cellsInRow,
        modals: state.modals,
        auth: state.auth,
      });

    case AC_SET_PLATES:
      return setPlates(state, payload);

    case AC_SET_LAYOUT:
      return setLayout(state, payload);

    case AC_SELECTED_TO_LEFT:
      return moveSelection(state, -1, 0);

    case AC_SELECTED_TO_RIGHT:
      return moveSelection(state, 1, 0);

    case AC_SELECTED_TO_UP:
      return moveSelection(state, 0, -1);

    case AC_SELECTED_TO_DOWN:
      return moveSelection(state, 0, 1);

    case AC_PLATES_TO_LEFT:
      return movePlatesValue(state, -1, 0);

    case AC_PLATES_TO_RIGHT:
      return movePlatesValue(state, 1, 0);

    case AC_PLATES_TO_UP:
      return movePlatesValue(state, 0, -1);

    case AC_PLATES_TO_DOWN:
      return movePlatesValue(state, 0, 1);

    case AC_UNDO_ACTION:
      return undoAction(state);

    case AC_REDO_ACTION:
      return redoAction(state);

    case AC_MOVE_BY_CLICK:
      return moveByClick(state, payload);

    case AC_OPEN_MODAL:
      return Object.assign({}, state, {
        modals: addIfNotIncluded(state.modals, payload),
      });

    case AC_CLOSE_MODAL:
      return Object.assign({}, state, {
        modals: removeItem(state.modals, payload),
      });

    case AC_CLOSE_LAST_MODAL:
      return Object.assign({}, state, {
        modals: removeLast(state.modals),
      });

    case AC_CLOSE_ALL_MODALS:
      return Object.assign({}, state, {
        modals: [],
      });

    case AC_REMOVE_LAST_ERROR_MESSAGE: {
      const errorMessages = removeLast(state.errorMessages);
      const i = state.modals.indexOf(MODALS.ERROR_MESSAGE);
      const modals = state.modals.slice();
      if (!isFilledArray(errorMessages)) {
        modals.splice(i, 1);
      }

      return Object.assign({}, state, {
        errorMessages: errorMessages,
        modals: modals,
      });
    }

    case AC_SET_FIELD_SIZE:
      return Object.assign({}, state, {
        cellsInRow: payload,
      });

    case SET_LOADED_GAME:
      return setLoadedGame(state, payload);

    case SET_AUTH_LOAD_STATUS: {
      const auth = Object.assign({}, state.auth, {
        loadStatus: payload,
      });

      return Object.assign({}, state, {
        auth: auth,
      });
    }

    case SET_AUTH_DATA:
      return setAuthData(state, payload);

    case SET_REGISTER_DATA:
      return setRegisterData(state, payload);

    case AC_LOGOUT:
      return logout(state);

    case AC_SAVE_GAME:
      return saveGame(state, payload);

    case AC_GET_GAMES_BY_CURRENT_LAYOUT:
      return getGamesByCurrentLayout(state);

    case AC_SET_GAMES_LOAD_STATUS:
      return Object.assign({}, state, {
        gamesLoadStatus: payload,
      });

    case AC_GET_GALLERY:
      return getGamesBySize(state);

    case AC_SET_GALLERY_LOAD_STATUS:
      return Object.assign({}, state, {
        galleryLoadStatus: payload,
      });

    case AC_SET_IS_TOUCH_SCREEN:
      return Object.assign({}, state, {
        isTouchScreen: payload,
      });

    default:
      return state;
  }
}
/**
 * Dispatcher
 * @typedef Dispatcher
 * @property {number} id
 * @property {function} handler
 */

function Store(reducer, initialState) {
  this.configureStore(reducer, initialState)
}

Store.prototype.configureStore = function configureStore(reducer, initialState) {
  if (!reducer || (typeof reducer !== 'function')) {
    throw new Error('Reducer should be a function');
  }

  this.state = initialState || {};

  this.reducer = reducer;

  this.dispatchers = [];

  return this;
}

Store.prototype.getState = function storeGetState() {
  return this.state;
}

Store.prototype.dispatch = function storeDispatch(action) {
  // console.log('dispatch: action: ', action);
  if (action instanceof Promise) {
    action.then(resolvedAction => this.dispatch(resolvedAction));
  } else if (!('type' in action)) {
    return this.getState();
  } else if (action.type === AC_SET_STATE
    && action.payload instanceof Promise) {
    action.payload.then(resolvedPayload => {
      action.payload = resolvedPayload;
      this.dispatch(action);
    });
  }

  const newState = this.reducer(this.getState(), action);

  if (newState instanceof Promise) {
    newState.then(resolvedNewState => this.dispatch(acSetState(resolvedNewState)));
  } else {
    this.state = Object.assign({}, this.state, newState);
    this.dispatchers.forEach(function(dispatcher) {
      dispatcher.handler(this.state);
    }.bind(this));
  }

  return this;
}

Store.prototype.subscribe = function storeSubscribe(handler) {
  let id = 0;
  this.dispatchers.forEach(function(dispatcher) {
    id = Math.max(id, dispatcher.id);
  }.bind(this));
  id += 1;

  this.dispatchers.push({ id: id, handler: handler });

  handler(this.state);

  return function storeUnsubscribe() {
    this.dispatchers = this.dispatchers.filter(function(dispatcher) {
      return dispatcher.id !== id;
    });
  }.bind(this);
}
function Controller() {
  this.startListen = function startListen() {
    window.addEventListener('resize', handleResize, false);
    document.addEventListener('keydown', handleKeyDown, false);
    // document.addEventListener('fullscreenchange ', handleFullscreenChange, false);
    document.onfullscreenchange = handleFullscreenChange;
    document.addEventListener('touchend', handleTouchEnd, false);

    return this;
  }.bind(this);

  this.stopListen = function stopListen() {
    window.removeEventListener('resize', handleResize);
    document.removeEventListener('keydown', handleKeyDown);
    document.removeEventListener('fullscreenchange', handleFullscreenChange);

    return this;
  }

  function handleResize() {
    eventEmitter.emit(EVENTS.RESIZE_WINDOW);
  }

  function handleFullscreenChange() {
    const isFullscreen = getFullscreenElement();
    eventEmitter.emit(EVENTS.FULLSCREEN_CHANGE, Boolean(isFullscreen));
  }

  function handleTouchEnd() {
    document.removeEventListener('touchend', handleTouchEnd);
    store.dispatch(acSetIsTouchScreen(true));
  }

  function handleKeyDown(event) {
    event.stopPropagation();

    const keyCode = event.keyCode;
    const altKey = event.altKey;
    const ctrlKey = event.ctrlKey;

    if (event.isComposing || keyCode === 229) {
      return;
    }

    const isModal = isFilledArray(store.getState().modals);

    switch (true) {
      case keyCode === 37 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToLeft());
        break;

      case keyCode === 37 && !isModal:
        store.dispatch(acSelectedToLeft());
        break;

      case keyCode === 39 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToRight());
        break;

      case keyCode === 39 && !isModal:
        store.dispatch(acSelectedToRight());
        break;

      case keyCode === 38 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToUp());
        break;

      case keyCode === 38 && !isModal:
        store.dispatch(acSelectedToUp());
        break;

      case keyCode === 40 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToDown());
        break;

      case keyCode === 40 && !isModal:
        store.dispatch(acSelectedToDown());
        break;

      case keyCode === 90 && ctrlKey && !isModal:
        store.dispatch(acUndoAction());
        break;

      case keyCode === 89 && ctrlKey && !isModal:
        store.dispatch(acRedoAction());
        break;

      case keyCode === 77 && ctrlKey:
        store.dispatch(acOpenModal(MODALS.MAIN_MENU));
        break;

      case keyCode === 27:
        store.dispatch(acCloseLastModal());
        break;

      case keyCode === 186:
        console.log(store.getState());
        break;

      case keyCode === 17:
        // ctrl
        break;

      default:
        // console.log('document: key down: keyCode: ', keyCode);
    }
  }
}
function NetworkService() {
  const layout = getFirstLayoutFromState(store.getState());

  this.props = {
    layout: layout,
    cellsInRow: store.getState().cellsInRow,
  };

  this.update = function(props) {
    this.props = Object.assign({}, this.props, props);

    let newState = null;

    if (this.props.galleryLoadStatusShouldBeReset) {
      newState = obj(newState).set('galleryLoadStatus',0);
    }

    if (this.props.gamesLoadStatusShouldBeReset) {
      newState = obj(newState).set('gamesLoadStatus', 0);
    }

    if (this.props.gamesShouldBeLoaded) {
      newState = obj(newState).set('gamesLoadStatus',1);
      store.dispatch(acGetGamesByCurrentLayout());
    }

    if (props.galleryShouldBeLoaded) {
      newState = obj(newState).set('galleryLoadStatus',1);
      store.dispatch(acGetGallery());
    }

    if (newState) {
      store.dispatch(acSetState(newState));
    }
  }.bind(this);

  const mapStateToProps = function(state) {
    const isLogged = isFilledString(state.auth.sessionToken);

    const layout = getFirstLayoutFromState(state);
    const previousHash = getHashFromLayout(this.props.layout);
    const currentHash = getHashFromLayout(layout);
    const isLayoutChanged = previousHash !== currentHash;

    const isSizeChanged = (state.cellsInRow !== this.props.cellsInRow)
      || (isLayoutChanged && !isFilledString(previousHash));

    const props = {
      cellsInRow: state.cellsInRow,
      gamesShouldBeLoaded: isLogged
        && isFilledArray(state.actions)
        && state.gamesLoadStatus === 0,
      gamesLoadStatusShouldBeReset: state.gamesLoadStatus > 0
        && (!isLogged || isLayoutChanged),
      galleryShouldBeLoaded: isLogged && state.galleryLoadStatus === 0,
      galleryLoadStatusShouldBeReset: isSizeChanged
        || (state.galleryLoadStatus > 0 && !isLogged),
      isLayoutChanged: isLayoutChanged,
      layout: layout,
    };

    this.update(props);
  }.bind(this);

  store.subscribe(mapStateToProps);
}
function menuIcon(props) {
  const className = props.className;

  return `
    <svg
      class="icon${className ? ` ${className}` : ''}"
      width="32px"
      height="32px"
      viewBox="0 0 32 32"
      preserveAspectRatio="xMidYMid meet"
      fill="transparent"
      stroke="currentcolor"
      stroke-width="2"
      stroke-linejoin="round"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
    >
      <path d="M1 4 H31 M1 12 H31 M1 20 H31 M1 28 H31" />
    </svg>
  `;
}
function accountIcon(props) {
  const className = props.className;

  return `
    <svg
      class="icon${className ? ` ${className}` : ''}"
      width="32px"
      height="32px"
      viewBox="0 0 32 32"
      preserveAspectRatio="xMidYMid meet"
      fill="transparent"
      stroke="currentcolor"
      stroke-width="2"
      stroke-linejoin="round"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
    >
      <circle cx="15" cy="8" r="7" />
      <path d="M1 28 A15 10 0 0 1 31 28 M1 28 A15 3 0 0 0 31 28" />
    </svg>
  `;
}
function spinnerIcon(props) {
  const className = props.className;

  return `
    <svg
      class="icon${className ? ` ${className}` : ''}"
      width="3rem"
      height="3rem"
      viewBox="0 0 48 48"
      preserveAspectRatio="xMidYMid meet"
      fill="transparent"
      stroke="currentcolor"
      stroke-width="4"
      stroke-linejoin="round"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
    >
      <path d="M2 22 A22 22 0 1 1 23 46" />
    </svg>
  `;
}
function Component(tagName, props, children) {
  this.create(tagName, props, children);
}

Component.prototype.create = function componentCreate(tagName, props, children) {
  this.props = Object.assign({
    children: children || [],
  }, props || {});
  this.element = tagName
    ? document.createElement(tagName)
    : null;
  this.props.children.forEach(function(child) {
    switch (true) {
      case (child instanceof Component):
        this.element.append(child.getElement());
        break;

      case isExist(child):
        this.element.append(child);
        break;

      default:
    }
  }.bind(this));

  return this;
}

Component.prototype.getElement = function componentGetElement() {
  return this.element;
}

Component.prototype.render = function componentRender() {
  return this;
}

Component.prototype.setProps = function componentSetProps(props) {
  this.props = Object.assign({}, this.props, props);

  return this;
}

/**
 * ListMenuItem
 * @typedef {Object} ListMenuItem
 * @property {string} className
 * @property {string} id
 * @property {string} title
 * @property {boolean} disabled
 * @property {function} onClick
 */

/**
 * ListMenuProps
 * @typedef {Object} ListMenuProps
 * @property {string} activeParentId
 * @property {string} className
 * @property {ListMenuItem[]|[]} items
 */

/**
 * ListMenu
 * @param {ListMenuProps} props
 */
function ListMenu(props) {
  this.create(props);
}

ListMenu.prototype = Object.create(Component.prototype);

ListMenu.prototype.constructor = ListMenu;

ListMenu.prototype.create = function listCreate(props) {
  getAncestor(ListMenu).create.call(this, 'ul', props, []);
  [
    ['list-menu', true],
    [this.props.className, Boolean(this.props.className)],
  ]
    .forEach(function(item) {
      if (item[1] === true) {
        this.element.classList.add(item[0]);
      }
    }.bind(this));

  return this;
}

ListMenu.prototype.render = function(props) {
  this.props = Object.assign({}, this.props, props);
  // console.log('=================================');
  // console.log('ListMenu: render: this.props', { ...this.props });

  const activeParentId = this.props.activeParentId;
  const items = this.props && isFilledArray(this.props.items)
    ? getItemsByParentId(this.props.items, activeParentId)
    : [];

  // console.log('ListMenu: render: items', [...items]);

  this.props.children = items.map(function(item) {
    const li = document.createElement('li');
    li.id = item.id;
    li.innerHTML = `${item.title}`;
    [
      ['list-menu__item', true],
      ['list-menu__item--disabled', item.disabled],
      [item.className, Boolean(item.className)],
    ]
      .forEach(function(className) {
        if (className[1]) {
          li.classList.add(className[0]);
        }
      }.bind(this));
    if (isFilledArray(item.children)) {
      li.addEventListener('click', function handleGroupMenuClick() {
        this.render({
          activeParentId: item.id,
        });
      }.bind(this));
    }
    if (typeof item.onClick === 'function') {
      const cbGoToItem = function goToItem(itemId) {
        this.render({
          activeParentId: itemId,
        })
      }.bind(this);
      li.addEventListener('click', function() {  item.onClick(cbGoToItem); });
    }
    return li;
  }.bind(this));

  this.element.innerHTML = '';
  this.props.children
    .forEach(function(item) {
      this.element.append(item);
    }.bind(this));

  // console.log('ListMenu: render: this.props', { ...this.props });
  // console.log('------------------------------------');

  return this;
}
/**
 * ModalProps
 * @typedef ModalProps
 * @property {string} id Id of the modal. Required for proper work
 * @property {boolean} isCentered
 * @property {Array} children
 * @property {boolean} isOpen
 * @property {function} onClose
 */

/**
 * Modal
 * @param {Object} props
 * @param {Array} children
 */
function Modal(props, children) {
  this.create(props, children);
}

Modal.prototype = Object.create(Component.prototype);

Modal.prototype.constructor = Modal;

Modal.prototype.create = function modalCreate(props, children) {
  if (!props.id) {
    throw new Error('Modal does not have required prop `id`');
  }
  const superclass = getAncestor(Modal);
  superclass.create.call(this, 'div', props, children);
  [
    ['modal', true],
    ['modal--centered', this.props.isCentered],
    ['modal--visible', this.props.isOpen],
  ]
    .concat(this.props.classNamesList)
    .filter(Boolean)
    .forEach(function(item) {
      if (item[1]) {
        this.element.classList.add(item[0]);
      }
    }.bind(this));

  this.element.addEventListener('click', function() {
    this.props.onClose && this.props.onClose();
    store.dispatch(acCloseModal(this.props.id));
  }.bind(this), false);

  document.getElementById('modal-root').append(this.element);

  const mapStateToProps = function(state) {
    const newProps = {
      isOpen: getLast(state.modals) === this.props.id,
    };

    this.render(newProps)
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this;
}

Modal.prototype.destroy = function modalDestroy() {
  this.element.removeEventListener('click', this.props.onClose);
  document.getElementById('modal-root').removeChild(this.element);
}

Modal.prototype.show = function modalShow() {
  this.element.classList.add('modal--visible');
}

Modal.prototype.hide = function modalHide() {
  this.element.classList.add('modal--is-hiding');
  delay(300)
    .then(function() {
      this.element.classList.remove('modal--visible');
      this.element.classList.remove('modal--is-hiding');
    }.bind(this));
}

Modal.prototype.render = function modalRender(props) {
  const newProps = props || { isOpen: this.props.isOpen };
  if (this.props.isOpen !== newProps.isOpen) {
    this.props = Object.assign({}, this.props, newProps);
    if (this.props.isOpen === true) {
      this.show();
    } else {
     this.hide();
    }
  }

  return this;
}
function ModalForm(props, children) {
  this.create(props, children);
}

ModalForm.prototype = Object.create(Modal.prototype);

ModalForm.prototype.constructor = ModalForm;

ModalForm.prototype.create = function modalFormCreate(props, children) {
  this.form = document.createElement('form');
  this.form.classList.add('modal-form__form');
  this.form.setAttribute('autocomplete', 'off');
  this.form.addEventListener('click', function(e) {
    e.stopPropagation();
  });
  this.form.name = props.name;

  if (isFilledArray(children)) {
    children.forEach(function(child) {
      this.form.append(child);
    }.bind(this));
  }

  const classNamesList = getClassNamesList(['modal-form'])
    .concat(props.classNamesList)
    .filter(Boolean);

  getAncestor(ModalForm).create.call(
    this,
    {
      id: props.id,
      isOpen: props.isOpen,
      isCentered: props.isCentered,
      classNamesList: classNamesList,
    },
    [this.form],
  );
}
function LoginForm() {
  this.create();
}

LoginForm.prototype = Object.create(ModalForm.prototype);

LoginForm.prototype.constructor = LoginForm;

LoginForm.prototype.create = function loginFormCreate() {
  this.props = {
    // mode: LOGIN_FORM_MODES.LOGIN,
    // children: getChildren(),
  };

  getAncestor(LoginForm).create.call(
    this,
    {
      id: MODALS.LOGIN_FORM,
      classNamesList: getClassNamesList(['login-form']),
      isOpen: false,
      isCentered: true,
      name: 'loginForm',
    },
    [],
    // this.props.children,
  );

  this.props.mode = LOGIN_FORM_MODES.LOGIN;

  const mapStateToProps = function(state) {
    const props = {
      validation: state.auth.validation || { login: '', password: '' },
    };
    this.render(props);
  }.bind(this);

  store.subscribe(mapStateToProps);
}

LoginForm.prototype.render = function loginFormRender(props) {
  getAncestor(LoginForm).render.call(this, props);

  this.props = Object.assign({}, this.props, props);

  function getFieldValue(form, field) {
    const _form = document.forms[form];

    return _form[field] ? _form[field].value : '';
  }

  const values = {
    login: getFieldValue('loginForm', 'login'),
    email: getFieldValue('loginForm', 'email'),
    password: getFieldValue('loginForm', 'password'),
    confirmPassword: getFieldValue('loginForm', 'confirmPassword'),
  };

  this.form.innerHTML = '';

  this.getChildren(values).forEach(function(child) {
    this.form.append(child);
  }.bind(this));

  ['login', 'email', 'password', 'confirmPassword'].forEach(function(id) {
    let elm = document.getElementById(`error-message-${id}`);
    if (elm) {
      elm.innerHTML = (this.props.validation && this.props.validation[id]) || '';
    }
  }.bind(this));
}

LoginForm.prototype.getChildren = function loginFormGetChildren(values) {
  const mode = this.props.mode;
  const LOGIN = LOGIN_FORM_MODES.LOGIN;
  const REGISTRATION = LOGIN_FORM_MODES.REGISTRATION;

  // content
  const content = document.createElement('div');
  content.classList.add('modal-form__content');

  // controls
  [
    { id: 'login', type: 'text', label: 'Login', value: values.login, modes: [LOGIN, REGISTRATION] },
    { id: 'email', type: 'text', label: 'e-mail', value: values.email, modes: [REGISTRATION] },
    { id: 'password', type: 'password', label: 'Password', value: values.password, modes: [LOGIN, REGISTRATION] },
    { id: 'confirmPassword', type: 'password', label: 'Confirm password', value: values.confirmPassword, modes: [REGISTRATION] },
  ]
    .filter(function (item) {
      return item.modes.includes(mode);
    })
    .forEach(function(item) {
      const elm = document.createElement('div');
      elm.classList.add('modal-form__control');
      elm.innerHTML = `
          <label for="${item.id}">${item.label}</label>
          <input type="${item.type}" id="${item.id}" name="${item.id}" value="${item.value}" placeholder="enter your ${item.id} here">
          <p id="error-message-${item.id}" class="control-error-message"></p>
        `;

      content.append(elm);
    });

  // toolbar
  const switchModeLabel = mode === LOGIN ? 'Registration' : 'Sign in';
  const toolbar = document.createElement('div');
  toolbar.classList.add('modal-form__toolbar');
  toolbar.innerHTML = `
      <span id="modeButton" class="form-toolbar-text-button">${switchModeLabel}</span>
      <button type="button" id="loginButton" class="form-toolbar-button">
        Submit
      </button>
    `;
  toolbar.querySelector('#modeButton').addEventListener('click', function() {
    this.props.mode = mode === LOGIN ? REGISTRATION : LOGIN;
    delay(100)
      .then(function() {
        this.render({});
      }.bind(this))
  }.bind(this));
  toolbar.querySelector('#loginButton').addEventListener('click', function() {
    const form = document.forms.loginForm;
    const login = form.login.value || '';
    const email = form.email ? form.email.value : '';
    const password = form.password.value || '';
    const confirmPassword = form.confirmPassword ? form.confirmPassword.value : '';
    form.password.value = '';
    if (form.confirmPassword) {
      form.confirmPassword.value = '';
    }
    if (mode === LOGIN) {
      store.dispatch(acSetAuthData(login, password));
    } else {
      store.dispatch(acSetRegisterData(
        login,
        email,
        password,
        confirmPassword,
      ));
    }
  });

  return [content, toolbar];
}
function ErrorMessage(props) {
  this.create(props);
}

ErrorMessage.prototype = Object.create(ModalForm.prototype);

ErrorMessage.prototype.constructor = ErrorMessage;

ErrorMessage.prototype.create = function errorMessageCreate(props) {
  this.props = props || {};
  this.props.children = getChildren();

  getAncestor(ErrorMessage).create.call(
    this,
    {
      id: MODALS.ERROR_MESSAGE,
      classNamesList: getClassNamesList(['error-message']),
      isOpen: false,
      isCentered: true,
      name: 'errorMessageForm',
    },
    this.props.children,
  );

  const mapStateToProps = function(state) {
    const errorMessage = isFilledArray(state.errorMessages)
      ? getLast(state.errorMessages)
      : { title: 'Error', message: 'Something went wrong!' };
    const props = {
      title: errorMessage.title,
      message: errorMessage.message,
    };

    this.render(props);
  }.bind(this);

  store.subscribe(mapStateToProps);

  function getChildren() {
    const header = document.createElement('h3');
    header.classList.add('error-message__header');
    header.id = 'errorMessageHeader';
    // header.innerHTML = 'Error';

    const content = document.createElement('section');
    content.classList.add('error-message__content');

    const message = document.createElement('p');
    message.classList.add('modal-form__control');
    message.id = 'errorMessageContentText';
    // message.innerHTML = `Something went wrong!`;
    content.append(message);

    const toolbar = document.createElement('div');
    toolbar.classList.add('modal-form__toolbar');
    toolbar.innerHTML = `
      <button type="button" id="errorMessageGotItButton" class="form-toolbar-button">
        Got it!
      </button>
    `;
    toolbar.querySelector('#errorMessageGotItButton')
      .addEventListener('click', function() {
        store.dispatch(acRemoveLastErrorMessage());
      });

    return [header, content, toolbar];
  }
}

ErrorMessage.prototype.render = function errorMessageRender(props) {
  getAncestor(ErrorMessage).render.call(this, props);

  this.props = Object.assign({}, this.props, props);

  document.getElementById('errorMessageHeader')
    .innerHTML = this.props.title;

  document.getElementById('errorMessageContentText')
    .innerHTML = this.props.message;
}
function Preloader() {
  this.create();
}

Preloader.prototype = Object.create(Component.prototype);

Preloader.prototype.constructor = Preloader;

Preloader.prototype.create = function preloaderCreate() {
  this.content = document.createElement('div');
  this.content.classList.add('preloader-content');
  this.content.innerHTML = `
    ${spinnerIcon({ className: 'preloader-icon' })}
    <span class="preloader-message">Fetching...</span>
  `;

  getAncestor(Preloader).create.call(
    this,
    'div',
    {
      id: MODALS.PRELOADER,
      isOpen: false,
      isCentered: true,
      classNamesList: getClassNamesList(['preloader', 'modal', 'modal--centered']),
    },
    [this.content],
  );

  this.props.classNamesList.forEach(function(item) {
    let action = 'add';
    if (item[1] === false) {
      action = 'remove';
    }
    this.getElement().classList[action](item[0]);
  }.bind(this));

  document.getElementById('modal-root')
    .append(this.getElement());

  const mapStateToProps = function(state) {
    const newProps = Object.assign({}, this.props, {
      isOpen: state.galleryLoadStatus === 1
        || state.gamesLoadStatus === 1
        || state.auth.loadStatus === 1,
    });

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this.render(this.props);
};

Preloader.prototype.show = function preloaderShow() {
  Modal.prototype.show.call(this);
};

Preloader.prototype.hide = function preloaderHide() {
  Modal.prototype.hide.call(this);
};

Preloader.prototype.render = function preloaderRender(props) {
  getAncestor(Preloader).render.call(this, props);

  if (this.props.isOpen !== props.isOpen) {
    this.props.isOpen = props.isOpen;
    if (this.props.isOpen) {
      this.show();
    } else {
      this.hide()
    }
  }
};
/**
 * ModalMenuProps
 * @typedef {Object} ModalMenuProps
 * @property {string} id Id of modal. Required
 * @property {boolean} isOpen
 * @property {boolean} isCentered
 * @property {string} className
 * @property {string[]} classNamesList
 * @property {function} onClose
 */
function ModalMenu(props) {
  this.create(props);
}

ModalMenu.prototype = Object.create(Modal.prototype);

ModalMenu.prototype.constructor = ModalMenu;

ModalMenu.prototype.create = function modalMenuCreate(props) {
  this.list = new ListMenu({
    className: 'modal-menu__list',
    items: [],
  }).render({
    items: props.items.map(function(item) {
      return Object.assign({}, item, {
        className: item.className || 'modal-menu__list-item',
        disabled: item.disabled === true,
      });
    }),
  });

  this.form = document.createElement('form');
  this.form.classList.add('modal-menu__form');
  this.form.append(this.list.element);
  this.form.addEventListener('click', function(e) {
    e.stopPropagation();
  });

  const classNamesList = getClassNamesList(['modal-menu'])
    .concat(props.classNamesList)
    .filter(Boolean);

  getAncestor(ModalMenu).create.call(
    this,
    {
      id: props.id,
      isOpen: false,
      isCentered: true,
      classNamesList: classNamesList,
    },
    [this.form],
  );
}
function toRootOfMenu(goTo) {
  goTo('');
}

function toItem(itemId) {
  return function(goTo) {
    goTo(itemId);
  }
}

function goBackItem(onClick) {
  return {
    id: 'Back',
    title: '..',
    className: 'modal-menu__list-item',
    onClick: onClick,
  };
}

function gotItAndCloseItem() {
  return {
    id: 'GotItAndClose',
    title: 'Got it!',
    className: 'modal-menu__list-item',
    onClick: function(goTo) {
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
      delay(200)
        .then(function() {
          toRootOfMenu(goTo);
        });
    },
  };
}const helpMenuItems = [
  goBackItem(toRootOfMenu),
  {
    id: 'Control',
    title: 'Control',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'Mouse',
        title: 'Mouse',
        className: 'modal-menu__list-item',
        children: [
          goBackItem(toItem('Control')),
          {
            id: 'AboutMouse',
            title: 'Click on square which you want to move. To move a group of squares - just click on a last one and it will push others in line.',
            className: 'modal-menu__text-item',
          },
          gotItAndCloseItem(),
        ],
      },
      {
        id: 'Keyboard',
        title: 'Keyboard',
        className: 'modal-menu__list-item',
        children: [
          goBackItem(toItem('Control')),
          {
            id: 'MoveSelection',
            title: 'Press key with <b>arrows</b> and select a square which you want to move.',
            className: 'modal-menu__text-item',
          },
          {
            id: 'MoveSquares',
            title: 'To move squares just hold <b>Ctrl</b> and press an <b>arrow</b>. You can move one squares or a group depends on selection.',
            className: 'modal-menu__text-item',
          },
          {
            id: 'Undo',
            title: 'To <b>Undo</b> / <b>Redo</b> movement - press <b>Ctrl-Z</b> / <b>Ctrl-Y</b>.',
            className: 'modal-menu__text-item',
          },
          gotItAndCloseItem(),
        ],
      },
    ],
  },
  {
    id: 'FieldSize',
    title: 'Field size',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'ChangeFieldSize',
        title: 'To change field size just select <b>Menu</b> -> <b>Change field size</b> and press button with necessary count of squares.',
        className: 'modal-menu__text-item',
      },
      gotItAndCloseItem(),
    ],
  },
  {
    id: 'Layout',
    title: 'Layout',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'ChangeLayout',
        title: 'To change layout just select <b>Menu</b> -> <b>Select new layout</b> and press button with layout in order or random one.',
        className: 'modal-menu__text-item',
      },
      {
        id: 'ChangeLayoutFromGallery',
        title: 'Registered users can select layout from gallery <b>Account menu</b> -> <b>Load layout from gallery</b> and click on you like one. '
          + 'Also they can save the result after game is completed <b>Account menu</b> -> <b>Save game</b>',
        className: 'modal-menu__text-item',
      },
      gotItAndCloseItem(),
    ],
  },
  {
    id: 'About',
    title: 'About the game',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'Goal',
        title: 'Move squares to get it in right order. Try to use minimal count of movements.',
        className: 'modal-menu__text-item',
      },
      {
        id: 'Wikipedia',
        title: '<a href="https://en.wikipedia.org/wiki/15_puzzle" target="_blank">15 Puzzle in Wikipedia</a>',
        className: 'modal-menu__text-item',
      },
      gotItAndCloseItem(),
    ],
  },
];
const layoutsMenuItems = [
  goBackItem(toRootOfMenu),
  {
    id: 'LayoutInOrder',
    title: 'Layout in order',
    className: 'modal-menu__list-item',
    onClick: function(goTo) {
      store.dispatch(acResetApp());
      store.dispatch(acSetPlates(getPlates()));
      store.dispatch(acCloseAllModals());
      delay(200)
        .then(function() { goTo(''); });
    },
  },
  {
    id: 'RandomLayout',
    title: 'Random layout',
    className: 'modal-menu__list-item',
    onClick: function(goTo) {
      const layout = getValidLayout(store.getState().cellsInRow);

      store.dispatch(acResetApp());
      store.dispatch(acSetLayout(layout));
      store.dispatch(acCloseAllModals());
      delay(200)
        .then(function() { goTo(''); });
    },
  }
];
const fieldSizeItems = [goBackItem(toRootOfMenu)]
  .concat([3, 4, 5, 6, 8].map(function(count) {
    return {
      id: `size-${count}`,
      title: `${count} x ${count}`,
      className: 'modal-menu__list-item',
      onClick: function(goTo) {
        store.dispatch(acResetApp());
        store.dispatch(acSetFieldSize(count));
        goTo('SelectNewLayout');
      },
    };
  }));
const mainMenuItems = [
  {
    id: 'SelectNewLayout',
    title: 'Select new layout',
    children: layoutsMenuItems.slice(),
  },
  {
    id: 'FieldSizeMenu',
    title: 'Change field size',
    children: fieldSizeItems,
  },
  {
    id: 'ChangeScreenMode',
    title: 'Fullscreen mode',
    onClick: function() {
      toggleScreenMode();
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
    }.bind(this),
  },
  /* {
    id: 'QuickSaveLocally',
    title: 'Quick save locally',
    onClick: function() {
      const payload = prepareToSave(store.getState().actions);
      console.log('Save game...', payload);
      localStorage.setItem(`${store.getState().cellsInRow}`, payload);
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
    },
  },
  {
    id: 'LoadLocallySaved',
    title: 'Load locally saved game',
    onClick: function() {
      try {
        let payload = JSON.parse(localStorage.getItem(`${store.getState().cellsInRow}`));
        console.log('Load last game: ', payload);
        store.dispatch(acSetLoadedGame(payload));
        store.dispatch(acCloseModal(MODALS.MAIN_MENU));
      } catch (e) {
        console.log('The last game for current field`s size was not found');
      }
    },
  }, */
  {
    id: 'HelpMenu',
    title: 'Help',
    children: helpMenuItems,
  },
];
function MainMenu() {
  this.create();
}

MainMenu.prototype = Object.create(ModalMenu.prototype);

MainMenu.prototype.constructor = MainMenu;

MainMenu.prototype.create = function mainMenuCreate() {
  getAncestor(MainMenu).create.call(this, {
    id: MODALS.MAIN_MENU,
    items: mainMenuItems,
    isFullscreen: Boolean(getFullscreenElement()),
    classNamesList: getClassNamesList(['main-menu']),
  });

  const changeFullscreen = function changeFullscreenMode(isFullscreen) {
    this.render({ isFullscreen: isFullscreen });
  }.bind(this);

  eventEmitter.addEventListener(EVENTS.FULLSCREEN_CHANGE, changeFullscreen);

  return this;
}

MainMenu.prototype.render = function mainMenuRender(props) {
  const superclass = getAncestor(MainMenu);
  superclass.render.call(this, props);

  const screenModeItem = this.element.querySelector('#ChangeScreenMode');
  if (screenModeItem) {
    screenModeItem.innerHTML = `${this.props.isFullscreen ? 'Windowed' : 'Fullscreen'} mode`;
  }

  return this;
}
function Gallery() {
  this.create();
}

Gallery.prototype = Object.create(ModalMenu.prototype);

Gallery.prototype.constructor = Gallery;

Gallery.prototype.create = function galleryCreate() {
  getAncestor(Gallery).create.call(this, {
    id: MODALS.GALLERY,
    items: [],
    classNamesList: getClassNamesList(['modal-menu-gallery']),
  });

  this.form.classList.add('gallery-form');
  this.list.element.classList.add('gallery-list');

  const mapStateToProps = function(state) {
    const newProps = {
      gallery: state.gallery,
      isOpened: state.modals.includes(MODALS.GALLERY),
    };

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this;
};

Gallery.prototype.render = function galleryRender(props) {
  getAncestor(Gallery).render.call(this, props);
  this.props = Object.assign({}, this.props, props);

  if (this.props.isOpened) {
    this.list.element.innerHTML = '';
    Array.isArray(this.props.gallery)
    && this.props.gallery.forEach(function(item) {
      const hash = item.hash;
      const items = item.items;
      const li = document.createElement('li');
      li.classList.add('gallery-item');
      li.dataset.hash = hash;
      // todo make constant for canvas size
      li.innerHTML = `
        <canvas width="800" height="800" class="gallery-item__field" />
      `;
      // todo separate handler
      li.addEventListener('click', function(e) {
        const liHash = e.currentTarget.dataset.hash;
        const layout = getLayoutFromHash(liHash);
        store.dispatch(acResetApp());
        store.dispatch(acSetLayout(layout));
        store.dispatch(acCloseAllModals());
      })
      const canvas = li.querySelector('canvas');
      const ctx = canvas.getContext('2d');

      drawField(hash, ctx);
      // todo make function for rectangle with text below
      const bestCount = items[0].count;
      const bestLogin = items[0].login;
      const cx = 800 / 2;
      const cy = 800 / 2;
      const fontSize = 800 * 0.05;
      ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
      roundedRect(ctx, cx, cy, 0.9 * 800, 0.25 * 800, 8, true);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.font = `bold ${fontSize}px sans-serif`;
      ctx.fillStyle = 'gold';
      ctx.fillText(`Games played: ${items.length}`, cx, cy - fontSize);
      ctx.fillText(`Best result: ${bestCount} by ${bestLogin}`, cx, cy + fontSize);

      this.list.element.append(li);
    }.bind(this));
  }
};
const accountMenuItems = [
  {
    id: 'AccountMenuLogin',
    title: 'Login',
    onClick: function() {
      store.dispatch(acOpenModal(MODALS.LOGIN_FORM));
    }.bind(this),
  },
  {
    id: 'QuickSaveLocally',
    title: 'Quick save locally',
    onClick: function() {
      const payload = prepareToSave(store.getState().actions);
      console.log('Save game...', payload);
      localStorage.setItem(`${store.getState().cellsInRow}`, payload);
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
    },
  },
  {
    id: 'LoadLocallySaved',
    title: 'Load locally saved game',
    onClick: function() {
      try {
        let payload = JSON.parse(localStorage.getItem(`${store.getState().cellsInRow}`));
        console.log('Load last game: ', payload);
        store.dispatch(acSetLoadedGame(payload));
        store.dispatch(acCloseModal(MODALS.MAIN_MENU));
      } catch (e) {
        console.log('The last game for current field`s size was not found');
      }
    },
  },
  {
    id: 'AccountMenuSaveInService',
    title: 'Save game',
    onClick: function() {
      const payload = prepareToSave(store.getState().actions);
      store.dispatch(acSaveGame(payload));
      delay(100)
        .then(function() {
          store.dispatch(acSetGamesLoadStatus(0));
          return delay(100)
        })
        .then(function() {
          store.dispatch(acSetGalleryLoadStatus(0));
        })
    }.bind(this),
  },
  {
    id: 'AccountMenuLoadFromGallery',
    title: 'Load layout from gallery',
    onClick: function() {
      store.dispatch(acOpenModal(MODALS.GALLERY));
    },
  },
  {
    id: 'AccountMenuLogout',
    title: 'Logout',
    onClick: function() {
      store.dispatch(acLogout());
      store.dispatch(acCloseAllModals());
    }.bind(this),
  },
];
function AccountMenu() {
  this.create();
}

AccountMenu.prototype = Object.create(ModalMenu.prototype);

AccountMenu.prototype.constructor = AccountMenu;

AccountMenu.prototype.create = function accountMenuCreate() {
  getAncestor(AccountMenu).create.call(this, {
    id: MODALS.ACCOUNT_MENU,
    items: accountMenuItems,
    isFullscreen: Boolean(getFullscreenElement()),
    classNamesList: getClassNamesList(['main-menu']),
  });

  const mapStateToProps = function(state) {
    const newProps = {
      login: state.auth.login || '',
      isLogged: isFilledString(state.auth.sessionToken),
      isReadyToSave: state.gameMode === GAME_MODES.COMPLETED
        && state.layoutType === LAYOUT_TYPE.MIXED
        && !state.isSaved,
      isGalleryAvailable: isFilledArray(state.gallery),
    };

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this.render({});
};

AccountMenu.prototype.render = function accountMenuRender(props) {
  getAncestor(AccountMenu).render.call(this, props);
  this.props = Object.assign({}, this.props, props);
  const isLogged = this.props.isLogged;
  const isReadyToSave = this.props.isReadyToSave;
  const login = this.props.login;
  const isGalleryAvailable = this.props.isGalleryAvailable;

  [
    {
      id: 'AccountMenuLogin',
      isVisible: !isLogged,
    },
    {
      id: 'AccountMenuRegister',
      isVisible: !isLogged,
    },
    {
      id: 'AccountMenuSaveInService',
      isVisible: isLogged && isReadyToSave,
    },
    {
      id: 'AccountMenuLoadFromGallery',
      isVisible: isLogged && isGalleryAvailable,
    },
    {
      id: 'AccountMenuLogout',
      isVisible: isLogged,
      title: `Logout: ${login}`,
    },
  ]
    .forEach(function(item) {
      const action = item.isVisible ? 'remove' : 'add';
      const li = this.element.querySelector(`#${item.id}`);
      li && li.classList[action]('list-menu__item--disabled');
      if (li && 'title' in item) {
        li.innerHTML = item.title;
      }
    }.bind(this));

  return this;
};
function Page(props, children) {
  this.create(props, children);
}

Page.prototype = Object.create(Component.prototype);

Page.prototype.constructor = Page;

Page.prototype.create = function pageCreate(props, children) {
  const superclass = Object.getPrototypeOf(Page.prototype);
  superclass.create.call(this, 'div', props, children);
  this.element.classList.add('page');
  if (this.props.className) {
    this.element.classList.add(this.props.className);
  }

  const root = document.getElementById('app-root');
  root.innerHTML = '';
  root.appendChild(this.element);

  return this;
}

Page.prototype.render = function pageRender() {
  return this;
}
/**
 * Plate
 * @param {Object} props Props
 * @property {number} cx Center X
 * @property {number} cy Center Y
 * @property {number} size Size of side
 * @property {number} value Value
 * @property {boolean} isSelected
 * @property {number} fieldX Coordinate X in field`s model
 * @property {number} fieldY Coordinate Y in field`s model
 */
function Plate(props) {
  this.create(props);
}

Plate.prototype = Object.create(Component.prototype);

Plate.prototype.constructor = Plate;

Plate.prototype.create = function plateCreate(props) {
  this.props = Object.assign({}, props);

  const mapStateToProps = function mapStateToProps(state) {
    const matrix = isFilledArray(state.actions)
      ? getLast(state.actions).matrix
      : null;

    this.props.isProperPosition = !!matrix
      && matrix[this.props.value - 1] === this.props.value;
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this;
}

Plate.prototype.render = function plateRender(context) {
  // calculation of final coords
  const cx = this.props.fieldX * this.props.size + this.props.size / 2;
  const cy = this.props.fieldY * this.props.size + this.props.size / 2;

  // calculation of next coords in canvas
  if (cx !== this.props.cx || cy !== this.props.cy) {
    let deltaX = cx - this.props.cx;
    let deltaY = cy - this.props.cy;
    deltaX = deltaX !== 0 ? deltaX / Math.abs(deltaX) * PLATE.MAX_SPEED : 0;
    deltaY = deltaY !== 0 ? deltaY / Math.abs(deltaY) * PLATE.MAX_SPEED : 0;
    const newCx = this.props.cx + deltaX;
    const newCy = this.props.cy + deltaY;

    let newDeltaX = cx - newCx;
    let newDeltaY = cy - newCy;
    newDeltaX = newDeltaX !== 0 ? newDeltaX / Math.abs(newDeltaX) * PLATE.MAX_SPEED : 0;
    newDeltaY = newDeltaY !== 0 ? newDeltaY / Math.abs(newDeltaY) * PLATE.MAX_SPEED : 0;

    this.props.cx = newDeltaX / deltaX > 0 ? newCx : cx;
    this.props.cy = newDeltaY / deltaY > 0 ? newCy : cy;
  }

  // gradient
  const gradient = context.createRadialGradient(
    this.props.cx,
    this.props.cy,
    this.props.size * 0.425,
    this.props.cx,
    this.props.cy,
    this.props.size * 0.375,
  );

  const fill0 = 'rgba(255, 255, 255, 0.8)';

  const fill1 = 'rgba(255, 255, 255, 1)';

  gradient.addColorStop(0, fill0);
  gradient.addColorStop(1, fill1);

  // context.fillStyle = gradient;

  context.fillStyle = this.props.isSelected
    ? gradient // 'rgba(255, 255, 255, 0.8)'
    : 'rgba(255, 255, 255, 1)';
  context.strokeStyle = 'rgba(0, 0, 0, 0.5)';
  roundedRect(
    context,
    this.props.cx,
    this.props.cy,
    this.props.size,
    this.props.size,
    8,
    true,
  );
  context.textAlign = 'center';
  context.textBaseline = 'middle';
  context.font = `bold ${this.props.size / 2}px sans-serif`;
  context.fillStyle = 'rgba(0, 0, 0, 0.5)';
  context.fillText(`${this.props.value}`, this.props.cx, this.props.cy + 2);
  if (this.props.isProperPosition) {
    context.fillStyle = 'gold';
    context.fillText(`${this.props.value}`, this.props.cx - 2, this.props.cy);
  }

  return this;
}

Plate.prototype.getElement = function() {
  return null;
}
function Field(props, children) {
  this.create(props, children);

  const handleMouseDown = function mouseDown(e) {
    const fieldCoordinates = getFieldCoordinates(e.clientX, e.clientY, this.element);

    store.dispatch(acSelectByCoordinate(
      fieldCoordinates.x,
      fieldCoordinates.y,
    ));
  }.bind(this);

  this.element.addEventListener('mousedown', handleMouseDown, false);

  const mapStateToProps = function(state) {
    this.props.plates = state.plates;
    this.props.selectedValue = state.gameMode === GAME_MODES.STARTED
      ? state.selectedValue
      : 0;
  }.bind(this);

  store.subscribe(mapStateToProps);
}

Field.prototype = Object.create(Component.prototype);

Field.prototype.constructor = Field;

Field.prototype.create = function fieldCreate(props, children) {
  const superclass = Object.getPrototypeOf(Field.prototype);
  superclass.create.call(
    this,
    'canvas',
    Object.assign({ plates: [] }, props),
    children,
  );
  this.element.classList.add('field');
  [
    [ 'id', 'field' ],
    [ 'height', FIELD.SIZE ],
    [ 'width', FIELD.SIZE ],
  ]
    .forEach(function(attr) {
      this.element.setAttribute(attr[0], attr[1])
    }.bind(this));

  return this;
}

Field.prototype.render = function fieldRender() {
  const context = this.element.getContext('2d');

  const boundRenderer = function renderer() {
    const plates = this.props.plates;
    const selectedValue = this.props.selectedValue;

    context.clearRect(0, 0, FIELD.SIZE, FIELD.SIZE);
    if (Array.isArray(plates)) {
      plates.forEach(function(item) {
        item.setProps({ isSelected: selectedValue === item.props.value });
        if (item.render) {
          item.render(context, plates);
        }
      }.bind(this));
    }
    requestAnimationFrame(boundRenderer);
  }.bind(this);

  requestAnimationFrame(boundRenderer);
}
function Fifteen(props = {}) {
  this.create(props);

  const resize = function() { this.resize(); }.bind(this);

  eventEmitter
    .addEventListener(EVENTS.RESIZE_WINDOW, resize);

  const mapStateToProps = function(state) {
    const games = state.games || [];
    const count = isFilledArray(state.actions)
      ? state.actions.length - 1
      : 0;

    const newProps = {
      count: count,
      bestResult: isFilledArray(games) ? games[0].count : 0,
      isTouchScreen: state.isTouchScreen,
    };

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);
}

Fifteen.prototype = Object.create(Page.prototype);

Fifteen.prototype.constructor = Fifteen;

Fifteen.prototype.create = function fifteenCreate(props) {
  const extendedProps = Object.assign(
    {}, props, { className: 'page-fifteen' },
  );
  const children = [
    getToolbar(),
    getMobileToolbar(),
    getContent(),
  ];
  getAncestor(Fifteen).create.call(this, extendedProps, children);

  this.props.field = new Field();
  this.element.querySelector('.main-content').append(
    this.props.field.getElement()
  );
  this.props.field.render();

  return this;

  function getToolbar() {
    const element = document.createElement('div');
    element.classList.add('main-toolbar');
    element.innerHTML = `
      <span class="main-toolbar-section main-menu">
        <button id="MainMenuButton" class="toolbar-button">
          ${menuIcon({
            className: 'main-menu-icon',
          })}
        </button>
      </span>
      <span class="main-toolbar-section best-result">
        <span id="toolbarBestCount" class="toolbar-info-item">
          The best: 0
        </span>
      </span>
      <span class="main-toolbar-section count">
        <span id="toolbarCount" class="toolbar-info-item">
          Count: 0
        </span>
      </span>
      <span class="main-toolbar-section account-menu">
        <button id="AccountButton" class="toolbar-button">
          ${accountIcon({
            className: 'account-icon',
          })}
        </button>
      </span>
    `;

    element.querySelector('#MainMenuButton')
      .addEventListener('click', function() {
        store.dispatch(acOpenModal(MODALS.MAIN_MENU));
      });

    element.querySelector('#AccountButton')
      .addEventListener('click', function() {
        store.dispatch(acOpenModal(MODALS.ACCOUNT_MENU));
      });

    return element;
  }

  function getMobileToolbar() {
    const element = document.createElement('div');
    element.classList.add('mobile-toolbar');
    element.innerHTML = `
      <span class="mobile-toolbar-section undo-redo">
        <button id="undoButton" class="toolbar-button mobile-toolbar-button">
          Undo
        </button>
        <button id="redoButton" class="toolbar-button mobile-toolbar-button">
          Redo
        </button>
      </span>
    `;

    [
      { id: 'undoButton', action: acUndoAction },
      { id: 'redoButton', action: acRedoAction },
    ]
      .forEach(function(item) {
        element.querySelector(`#${item.id}`)
          .addEventListener('click', function(e) {
            store.dispatch(item.action());
            e.target.blur();
          });
      });

    return element;
  }

  function getContent() {
    const element = document.createElement('div');
    element.classList.add('main-content');

    return element;
  }
}

Fifteen.prototype.resize = function fifteenResize() {
  const pageHeight = this.element.offsetHeight;
  const pageWidth = this.element.offsetWidth;
  const contentSection = this.element.querySelector('.main-content');

  const toolbar = this.element.querySelector('.main-toolbar');
  const toolbarMarginBottom = parseInt(getComputedStyle(toolbar)['margin-bottom'], 10);
  const deltaHeight = pageHeight - toolbar.offsetHeight - toolbarMarginBottom;
  const size = Math.min(deltaHeight, pageWidth);
  contentSection.style.width = `${size}px`;
  contentSection.style.height = `${size}px`;

  return this;
}

Fifteen.prototype.render = function fifteenRender(props) {
  this.props = Object.assign({}, this.props, props);

  this.element.querySelector('#toolbarCount')
    .innerHTML = `Count: ${this.props.count}`;

  const toolbarBestCount = this.element.querySelector('#toolbarBestCount');
  toolbarBestCount.innerHTML = `Best: ${this.props.bestResult}`;
  const action = this.props.bestResult > 0 ? 'remove' : 'add';
  toolbarBestCount.parentElement.classList[action]('toolbar-info-item--disabled');

  const mobileToolbarVisible = document.querySelector(
    '.mobile-toolbar--visible'
  );

  if (this.props.isTouchScreen && !mobileToolbarVisible) {
    document.querySelector('.mobile-toolbar').classList
      .add('mobile-toolbar--visible');
  }

  return this.resize();
}

Fifteen.prototype.getContext = function fifteenGetContext() {
  return document.getElementById('field').getContext('2d');
}
function App(props) {
  this.create(props);
}

App.prototype.create = function appCreate(props) {
  const newProps = props || {};
  this.props = Object.assign({}, newProps);

  return this;
}

App.prototype.start = function appStart() {
  this.props.page = new Fifteen();
  this.props.mainMenu = new MainMenu();
  this.props.accountMenu = new AccountMenu();
  this.props.loginForm = new LoginForm();
  this.props.gallery = new Gallery();
  this.props.errorMessages = new ErrorMessage();
  this.props.preloader = new Preloader();

  store.dispatch(acSetLayout());
}
document.addEventListener('DOMContentLoaded', start, false);

let eventEmitter;
let store;
let networkService;
let controller;

function start() {
  eventEmitter = new EventEmitter();
  store = new Store(reducer, initialState);
  networkService = new NetworkService();
  controller = new Controller().startListen();

  new App({ temp: 'temp' }).start();
}
