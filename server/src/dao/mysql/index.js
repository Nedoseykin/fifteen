const mysql = require('mysql');

const config = require('../../../configs/db.json');
const { getSqlQuery } = require('../../utils/dao');

const INSERT_USER = 'insert into users (login, password, email) values (?, ?, ?)';
const SELECT_USERS = 'select * from users';
const SELECT_USER_BY_LOGIN = 'select * from users where `login` = ? limit 1';
const SELECT_USER_BY_LOGIN_AND_PASSWORD = 'select * from users where `login` = ? and `password` = ? limit 1';
const INSERT_SESSION = 'insert into sessions (user_id, token) values (?, ?)';
const UPDATE_SESSION = 'update sessions set requests_count=?, closed=? where id=?';
const SELECT_SESSION_BY_TOKEN = 'select * from sessions where `token` = ? limit 1';
const INSERT_GAME = 'insert into games (cells_in_row, state, count, hash, user_id) values (?, ?, ?, ?, ?)';
const SELECT_GAME_BY_STATE_AND_USER_ID = 'select * from games where `state` = ? and `user_id` = ? limit 1';
const SELECT_GAMES_BY_HASH = 'select games.id, games.hash, games.count, users.login from games, users '
  + 'where games.user_id = users.id and games.hash = ? order by games.count asc limit 20';
const SELECT_GAMES_BY_CELLS_IN_ROW = 'select games.id, games.hash, games.count, users.login '
  + 'from games, users where games.user_id = users.id and games.cells_in_row = ?';
const SELECT_BEST_GAME_BY_CELLS_COUNT = 'select `count` from games where `cells_in_row` = ? order by `count` asc limit 1';

const sqlQuery = getSqlQuery(mysql.createPool(config));

exports.createUser = function(login, password, email) {
  return sqlQuery(INSERT_USER, [login, password, email]);
};

exports.getUsers = function() {
  return sqlQuery(SELECT_USERS, []);
};

exports.getUserByLogin = function(login) {
  return sqlQuery(SELECT_USER_BY_LOGIN, [login])
};

exports.getUserByLoginAndPassword = function(login, password) {
  return sqlQuery(SELECT_USER_BY_LOGIN_AND_PASSWORD, [login, password]);
};

exports.createSession = function(userId, token) {
  return sqlQuery(INSERT_SESSION, [userId, token]);
};

exports.updateSession = function(session) {
  return sqlQuery(UPDATE_SESSION, [
    session.requests_count,
    session.closed,
    session.id,
  ]);
};

exports.getSessionByToken = function(token) {
  return sqlQuery(SELECT_SESSION_BY_TOKEN, [token]);
};

exports.createGame = function(cellsInRow, state, count, hash, userId) {
  return sqlQuery(INSERT_GAME, [cellsInRow, state, count, hash, userId]);
};

exports.getGameByStateAndUserId = function(state, userId) {
  return sqlQuery(SELECT_GAME_BY_STATE_AND_USER_ID, [state, userId]);
};

/**
 * Returns list of games with given layout
 * @param {string} hash Hash of game - string like `0-1-15-3-...`
 * @returns {Promise} Promise resolved to array or error object
 */
exports.getGamesByHash = function(hash) {
  return sqlQuery(SELECT_GAMES_BY_HASH, [hash]);
};

/**
 * Returns list of games with given cells in row
 * @param {number} cellsInRow
 * @returns {Promise} Promise resolved to array or error object
 */
exports.getGamesByCellsInRow = function(cellsInRow) {
  return sqlQuery(SELECT_GAMES_BY_CELLS_IN_ROW, [cellsInRow]);
};

exports.getBestGameByCellsInRow = function(cellsInRow) {
  return sqlQuery(SELECT_BEST_GAME_BY_CELLS_COUNT, [cellsInRow]);
};
