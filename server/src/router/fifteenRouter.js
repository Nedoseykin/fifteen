const router = require('express').Router();
const fifteenController = require('../controller/fifteen/v1');
const checkSession = require('../controller/middleware/auth/check-session');

router.use(checkSession);

router.post('/game', fifteenController.createNewGame);

router.get('/game/best', fifteenController.getBestGameByCellsInRow);

router.get('/games/hash/:hash', fifteenController.getGamesByHash);

router.get('/games/size/:cellsInRow', fifteenController.getGamesByCellsInRow);

module.exports = router;
