const dao = require('../../../dao/mysql');
const { catchApiError } = require('../../../utils/controller');
const { isFilledString } = require('../../../utils/common');
const log = require('../../../utils/log');

exports.createNewGame = function fifteenCreateNewGame(req, res) {
  const { start, movements } = req.body;

  const hash = start.matrix.join('-');
  const cellsInRow = Math.sqrt(start.matrix.length);
  const count = movements.length;
  const userId = req.session.user_id;
  const state = JSON.stringify(req.body);

  dao.getGameByStateAndUserId(state, userId)
    .then(result => {
      if (result.length > 0) {
        throw { status: 400, message: 'The game is already saved for this user' };
      }

      return dao.createGame(cellsInRow, state, count, hash, userId)
    })
    .then(async result => {
      if (!result.affectedRows) {
        throw { message: 'Game was not created', status: 500 };
      }
      await log(`${req.method} - ${req.originalUrl} - 201 - Game was created successfully`, new Date(), true);

      res.sendStatus(201);
    })
    .catch(catchApiError(req, res));
};

/**
 * Returns list of objects like { id, hash, count, login } for given hash
 * @param {Request} req
 * @param {Response} res
 * @returns {void} Responses with success or error. Content type JSON.
 */
exports.getGamesByHash = function fifteenGetGamesByHash(req, res) {
  const { hash: hashInBase64 } = req.params;

  new Promise((resolve, reject) => {
    try {
      const hashInBase64Buffer = Buffer.from(hashInBase64, 'base64');
      const hash = hashInBase64Buffer.toString('utf-8');

      resolve(hash);
    } catch (e) {
      reject({ status: 400, message: 'Wrong `hash` parameter for request' });
    }
  })
    .then(dao.getGamesByHash)
    .then(async games => {
      await log(`${req.method} - ${req.originalUrl} - 200 - Games found: ${games.length}`, new Date(), true);

      const mapHashToLayout = games.map(({ hash, ...rest }) => ({
        ...rest,
        layout: hash.split('-').map(str => parseInt(str, 10)),
      }));

      res.status(200).json(mapHashToLayout);
    })
    .catch(catchApiError(req, res));
};

/**
 * GameItem
 * @typedef {Object} GameItem
 * @property {number} id - `id` in `games` table
 * @property {number} count - `count` in `games` table
 * @property {string} login - `login` in `users` table for `user_id` in `games` table
 */

/**
 * GameBySize
 * @typedef {Object} GameBySize
 * @property {string} hash - hash for game layout
 * @property {GameItem[]} items - list of GameItem for given hash
 */

/**
 * Returns array of GameBySize items
 * response successful: status 200
 * response error: status 400 Validation error
 * response error: status 500 Internal server error
 */
exports.getGamesByCellsInRow = function(req, res) {
  const cellsInRow = req.params.cellsInRow
    ? parseInt(req.params.cellsInRow, 10)
    : 0;

  new Promise((resolve, reject) => {
    if (cellsInRow === 0) {
      reject({ message: 'Wrong parameter `cellsInRow`', status: 400 });
    } else {
      resolve(cellsInRow);
    }
  })
    .then(dao.getGamesByCellsInRow)
    .then(async result => {
      await log(`${req.method} - ${req.originalUrl} - 200 - Rows found: ${ result.length }`, new Date(), true);

      let games = [];

      result.forEach(({ id, hash, count, login }) => {
        const item = { hash, items: [ { id, count, login } ] };
        const i = games.findIndex(({ hash: gamesHash }) => gamesHash === hash);
        if (i > -1) {
          games[i].items.push(item.items[0]);
        } else {
          games.push(item);
        }
      });

      games = games.map(({ hash, items }) => ({
        hash,
        items: items.sort((item1, item2) => item1.count - item2.count),
      }));

      res.status(200).json(games);
    })
    .catch(catchApiError(req, res));
};

/**
 * Returns best result for given cellsInRow
 * response successful: status 200
 * response error: status 400 Validation error
 * response error: status 500 Internal server error
 */
exports.getBestGameByCellsInRow = function fifteenGetBestGameBeCellsInRow(req, res) {
  const cellsInRow = req.query.cellsInRow
    ? parseInt(req.query.cellsInRow, 10)
    : 0;

  new Promise((resolve, reject) => {
    if (cellsInRow === 0) {
      reject({ message: 'Wrong parameter `cellsInRow`', status: 400 });
    } else {
      resolve(cellsInRow);
    }
  })
    .then(dao.getBestGameByCellsInRow)
    .then(async result => {
      const count = Array.isArray(result) && result.length
        ? result[0].count
        : 0;

      await log(`${req.method} - ${req.originalUrl} - 200 - Count: ${count}`, new Date(), true);

      res.status(200).json({ cellsInRow, count });
    })
    .catch(catchApiError(req, res))
};
