const { sessionTimeoutMin } = require('../../../../configs/server.json');
const dao = require('../../../dao/mysql');
const { catchApiError } = require('../../../utils/controller');

module.exports = function checkSession(req, res, next) {
  const token = req.headers['session-token'] || '';

  dao.getSessionByToken(token)
    .then(result => {
      if (!result.length) {
        throw { message: 'Unauthorized request', status: 401 };
      }

      req.session = result[0];
      const { update_ts, closed } = req.session;
      const timeOutMin = (new Date().getTime() - new Date(update_ts).getTime()) / 60000;

      if (closed === 1 || timeOutMin >= sessionTimeoutMin) {
        throw { message: 'Unauthorized request. Session is expired', status: 401 };
      }

      req.session.requests_count += 1;

      return dao.updateSession(req.session);
    })
    .then(() => { next(); })
    .catch(catchApiError(req, res));
}
