module.exports = class BasicError {
  constructor(message, status, date) {
    this.message = message || 'Internal server error';
    this.status = status || 500;
    this.date = date || new Date();
  }
}
