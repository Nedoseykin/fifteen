const BasicError = require('./BasicError');

module.exports = class ValidationError extends BasicError {
  constructor(validation, date) {
    super('Validation fault', 200, date);
    this.validation = validation;
  }
}
