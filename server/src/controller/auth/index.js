const { sha256 } = require('js-sha256');

const dao = require('../../dao/mysql');
const { catchApiError } = require('../../utils/controller');
const log = require('../../utils/log');
const { passwordEncrypt } = require('../../utils/auth');
const validators = require('./validators');

/**
 * Registers new user
 * response successful: status 201
 * response error: status 400 Validation error
 * response error: status 500 Internal server error
 */
exports.register = function authRegister(req, res) {
  const { login, password, email } = req.body;
  let token = '';

  validators.register(req.body)
    .then(() => {
      const encryptedPassword = passwordEncrypt(password);

      return dao.createUser(login, encryptedPassword, email);
    })
    .then(async result => {
      const userId = result.insertId;

      if (!userId) {
        throw { message: 'User was not created' };
      }

      await log(
        `${req.method} - ${req.originalUrl} - User has been created successfully`,
        new Date(),
        true,
      );

      token = sha256(`${Math.round(Math.random() * 1000000)}`);

      return dao.createSession(userId, token);
    })
    .then(async function(result) {
      if (!result.affectedRows) {
        throw { message: 'Session was not created', status: 500 };
      }

      await log(
        `${req.method} - ${req.originalUrl} - 201 - Session created with token: ${token}`,
        new Date(),
        true,
      );

      res.append('access-control-expose-headers', 'session-token');
      res.append('session-token', token);
      res.sendStatus(201);
    })
    .catch(catchApiError(req, res));
};

/**
 * Authorizes user and creates session
 * response successful: status 201, header session-token contains token
 * response error: status 404, User was not found
 * response error: status 500, Internal server error
 */
exports.login = function authLogin(req, res) {
  let token = '';

  validators.login(req.body)
    .then(() => {
      const { login, password } = req.body;

      return dao.getUserByLoginAndPassword(login, passwordEncrypt(password));
    })
    .then(async result => {
      if (!result.length) {
        throw { message: 'Authorization fault', status: 401 };
      }

      const user = result[0];
      token = sha256(`${Math.round(Math.random() * 1000000)}`);

      return dao.createSession(user.id, token);
    })
    .then(async result => {
      if (!result.affectedRows) {
        throw { message: 'Session was not created', status: 500 };
      }

      await log(
        `${req.method} - ${req.originalUrl} - 201 - Session created with token: ${token}`,
        new Date(),
        true,
      );

      res.append('access-control-expose-headers', 'session-token');
      res.append('session-token', token);
      res.sendStatus(201);
    })
    .catch(catchApiError(req, res));
};

/**
 * Closes session with given token
 * response successful: status 200
 * response error: status 404, Session was not found or is already closed
 * response error: status 500, Internal server error
 */
exports.logout = function(req, res) {
  const token = req.headers['session-token'] || '';

  dao.getSessionByToken(token)
    .then(result => {
      if (!result.length || result[0].closed === 1) {
        throw { message: 'Not found', status: 404 };
      }

      return dao.updateSession({ ...result[0], closed: 1 });
    })
    .then(async result => {
      if (!result.changedRows) {
        throw { message: 'Session was not changed', status: 500 };
      }

      await log(`${req.method} - ${req.originalUrl} - 200 - Logout - token: ${token}`, new Date(), true);

      res.sendStatus(200);
    })
    .catch(catchApiError(req, res));
}
