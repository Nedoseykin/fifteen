const dao = require('../../../dao/mysql');
const { isFilledString } = require('../../../utils/common');
const { getFields, validate } = require('../../../utils/controller');

const LOGIN = 'login';
const PASSWORD = 'password';
const CONFIRM_PASSWORD = 'confirmPassword';
const EMAIL = 'email';

const OPERATION = {
  REGISTRATION: 'OPERATION_REGISTRATION',
  LOGIN: 'OPERATION_LOGIN',
};

const fields = [
  {
    key: LOGIN,
    defaultValue: '',
    validators: [
      {
        action: value => Promise.resolve(isFilledString(value) ? '' : 'Field is required'),
        operations: [OPERATION.LOGIN, OPERATION.REGISTRATION],
      },
      {
        action: value => dao.getUserByLogin(value)
          .then(result => Promise.resolve(result.length === 0 ? '' : 'Login should be unique')),
        operations: [OPERATION.REGISTRATION],
      },
    ],
  },
  {
    key: PASSWORD,
    defaultValue: '',
    validators: [
      {
        action: value => Promise.resolve(isFilledString(value) ? '' : 'Field is required'),
        operations: [OPERATION.LOGIN, OPERATION.REGISTRATION],
      },
      {
        action: (value, payload) => Promise.resolve(
          value === payload[CONFIRM_PASSWORD] ? '' : 'Password is differ from confirmed one',
        ),
        operations: [OPERATION.REGISTRATION],
      },

    ],
  },
  {
    key: CONFIRM_PASSWORD,
    defaultValue: '',
    validators: [
      {
        action: value => Promise.resolve(isFilledString(value) ? '' : 'Field is required'),
        operations: [OPERATION.REGISTRATION],
      },

    ],
  },
  {
    key: EMAIL,
    defaultValue: '',
    validators: [
      {
        action: value => Promise.resolve(isFilledString(value) ? '' : 'Field is required'),
        operations: [OPERATION.REGISTRATION],
      },
    ],
  },
];

exports.register = function validatorRegister(payload) {
  return validate(payload, getFields(fields, OPERATION.REGISTRATION));
};

exports.login = function validationLogin(payload) {
  return validate(payload, getFields(fields, OPERATION.LOGIN))
};
