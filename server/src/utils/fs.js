const fs = require('fs');
const path = require('path');

const log = require('./log');

exports.writeLineToFile = (line, fileName) => {
  new Promise((resolve, reject) => {
    fs.writeFile(fileName, line, 'utf-8', (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  })
    .catch(async err => {
      await log(`Error: failed to write in file: ${err.message}`, new Date(), true);
    });
};

exports.savePid = async pid => {
  const fileName = path.resolve(__dirname, '..', '..', 'process', 'server.pid');

  await exports.writeLineToFile(pid, fileName);

  await log(`Pid was successfully saved: ${pid}`, new Date(), true);
};
