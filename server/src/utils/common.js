exports.isFilledString = v => (typeof v === 'string') && v.length > 0;

exports.isFilledArray = v => Array.isArray(v) && v.length > 0;
