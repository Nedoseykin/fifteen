const get = require('lodash/get');

const log = require('./log');
const { isFilledString } = require('./common');
const ValidationError = require('../controller/domen/errors/ValidationError');

/**
 * Sends HTTP response with an error
 * or sends validation object if error.validation.isValid === false
 * @param {Request} req
 * @param {Response} res
 * @returns {function} Resolves to void. Sends HTTP response with error
 */
exports.catchApiError = function catchApiError(req, res) {
  return async function(error) {
    const status = error.status || 500;
    const message = error.message || 'Internal server error';
    const date = error.date || new Date();

    await log(`${req.method} - ${req.originalUrl} - ${status} - Error: ${message}`, date, true);

    const payload = error.validation && error.validation.isValid === false
      ? error.validation
      : {
        message: status !== 500 ? message : 'Internal server error',
        status,
      };

    res.status(status).json(payload);
  };
};

/**
 * FieldRule
 * @typedef {Object} FieldRule
 * @property {string} key
 * @property {any} defaultValue
 * @property {Array} validators
 */

/**
 * Returns filtered field rules list with mapped validators
 * @param {FieldRule[]} fields
 * @param {string} operation
 * @returns {FieldRule[]} Filtered field rules list
 */
exports.getFields = function controllerFilterFields(fields, operation) {
  return fields.reduce((acc, { key, defaultValue, validators }) => {
    const filteredValidators = validators
      .filter(({ operations }) => operations.includes(operation))
      .map(({ action }) => action);

    return filteredValidators.length > 0
      ? acc.concat([{
          key,
          defaultValue,
          validators: filteredValidators,
        }])
      : acc;
  }, []);
};

/**
 * Returns a list of validators (array of functions which returns Promises)
 * @param {Object} payload Usually request.body
 * @param {FieldRule[]} fields
 */
exports.getValidatorsList = function controllerGetValidatorsList(payload, fields) {
  const validatorsList = [];
  fields.forEach(({ key, defaultValue, validators }) => {
    const value = get(payload, key, defaultValue);
    validators.forEach(validator => {
      validatorsList
        .push(async () => ({ key, message: await validator(value, payload) }));
    });
  });

  return validatorsList;
};

/**
 * Returns array of Promise objects resolved to { key, message }
 * @param {FieldRule[]} validatorsList
 * @returns {Promise} Resolves to array of objects { key, message }
 */
exports.getValidationObjList = async function(validatorsList) {
  return await Promise.all(validatorsList.map(item => item()));
};

/**
 * Returns a validation object { isValid: false, field1: 'message1', ... }
 * @param {Array} validationObjList
 * @returns {Object} Validation object { isValid: boolean, field1: message1, ... }
 */
exports.getValidation = function(validationObjList) {
  return validationObjList.reduce((acc, { key, message }) => {
    const result = { ...acc };
    if (isFilledString(message) && !isFilledString(result[key])) {
      result.isValid = false;
      result[key] = message;
    }

    return result;
  }, { isValid: true })
};

/**
 * Automates basic operations for creation of validation object
 * Returns a validation object
 * @param {Object} payload Usually request.body
 * @param {FieldRule[]} fields Array of rules objects
 * @returns {Object} Validation object { isValid, field1: message1, ... }
 */
exports.basicValidation = async function controllerBasicValidation(payload, fields) {
  const validationObjList = await exports.getValidationObjList(
    exports.getValidatorsList(payload, fields),
  );

  return exports.getValidation(validationObjList);
};

/**
 * Returns Promise. It resolves to valid validation object or rejects to invalid validation objects
 * @param {Object} payload Usually request.body
 * @param {FieldRule[]} fields Array of rules objects
 * @returns {Promise} Resolves or rejects to validation object
 */
exports.validate = function controllerValidate(payload, fields) {
  return new Promise(async (resolve, reject) => {
    const validation = await exports.basicValidation(payload, fields);

    if (validation.isValid) {
      resolve(validation);
    } else {
      reject(new ValidationError(validation));
    }
  });
};
