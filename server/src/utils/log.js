const fs = require('fs');
const path = require('path');
const { EOL } = require('os');

/**
 * Logs in file and terminal
 * @param {string} message
 * @param {Date} date
 * @param {boolean} useDate
 */
function log(message, date, useDate) {
  const text = useDate
    ? `${date.toUTCString()} - ${message}`
    : `${message}`;

  return new Promise((resolve, reject) => {
    const fileName = path.resolve(__dirname, '..', '..', 'logs', 'server.log');
    const isExist = fs.existsSync(fileName);
    let action = isExist ? 'appendFile' : 'writeFile';
    fs[action](fileName, `${text}${EOL}`, 'utf-8', (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  })
    .then(() => {
      console.log(text);
    })
    .catch(err => {
      console.log(err.message);
    });
}

module.exports = log;
