exports.getSqlQuery = function getSqlQuery(pool) {
  return (sql, values) => new Promise((resolve, reject) => {
    pool.query(
      { sql },
      [...values],
      cbResult(resolve, reject),
    );
  });
};


function cbResult(resolve, reject) {
  return (error, result) => {
    if (error) {
      console.log('error: ', error.message);
      reject({
        message: error.message,
        date: new Date(),
        status: 500,
      });
    } else {
      // console.log('result: ', result);
      resolve(result);
    }
  };
}
