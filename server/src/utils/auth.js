const { sha256 } = require('js-sha256');

const { s } = require('../constants/auth');

exports.passwordEncrypt = p => sha256(`${p}${s}`).substr(0, 63);
