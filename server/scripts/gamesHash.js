const mysql = require('mysql');

const config = require('../configs/db.json');
const { getSqlQuery } = require('../src/utils/dao');

const SELECT_GAMES = 'select * from games';
const UPDATE_GAMES = 'update games set hash=? where id=?';

const sqlQuery = getSqlQuery(mysql.createPool(config));

/**
 * Filters
 */
const EMPTY_HASH = ({ hash }) => !hash;
const ALL_ROWS = row => true;

sqlQuery(SELECT_GAMES, [])
  .then(games => {
    /**
     * Select existed filter or add new one
     */
    const filteredGames = games.filter(ALL_ROWS);

    if (!filteredGames.length) {
      return Promise.resolve(filteredGames);
    }

    return Promise.all(filteredGames.map(({ state, id }) => {
      const { matrix } = JSON.parse(state).start;
      const hash = matrix.join('-');

      return sqlQuery(UPDATE_GAMES, [hash, id]);
    }));
  })
  .then(rows => {
    console.log(`--- Rows updated: ${rows.length} ---`);
    console.log('=== Operation is completed ===');
    process.exit(0);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });