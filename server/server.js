const express = require('express');
const path = require('path');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const serveStatic = require('serve-static');

const { port } = require('./configs/server.json');
const authRouter = require('./src/router/authRouter');
const fifteenRouter = require('./src/router/fifteenRouter');
const { savePid } = require('./src/utils/fs');

const pathToPublic = path.resolve(__dirname, 'public');
const pid = process.pid;

savePid(pid).catch(err => { console.log(err) });

const server = express();

// const pathToStatic = path.resolve(__dirname, 'static');

const corsOptions = {
  allowedHeaders: '*',
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
};

server.set('etag', 'strong');

server.use(helmet());
server.use(cors(corsOptions));
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.use(serveStatic(pathToPublic, {
  setHeaders: (res) => {
    res.setHeader('Content-Security-Policy', 'self');
  },
}));

// server.use('/', express.static(pathToPublic));

server.use('/auth', authRouter);

server.use('/api/fifteen/v1', fifteenRouter);

// server.use('/test-request', express.static(pathToStatic));

// server.get('/test', (req, res) => {
//   res.append('location', '/location');
//   res.append('Access-Control-Allow-Origin', '*');
//   res.sendStatus(302);
// });

server.listen(port, () => {
  console.log(`Server listen on port: ${port}`);
});
