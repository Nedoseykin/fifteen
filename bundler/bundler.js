const path = require('path');

const { readFile, writeFile } = require('./utils/fs-utils');
const { error } = require('./utils/error-utils');
const { getConfig } = require('./utils/config-utils');

const SCRIPTS_REGEX = /<script.*src="(.*)".*\/script>$/mgi;

const args = process.argv.splice(2);

const { entry, output, template } = getConfig(args);
const entryPath = path.dirname(entry);
const bundleFileName = path.resolve(output.path, output.bundle);
const stylesFileName = path.resolve(output.path, output.styles);
const htmlFileName = path.resolve(output.path, output.html);

let indexHtml = '';

readFile(entry)
  .then(data => {
    indexHtml = data;
    let scriptsList = indexHtml.matchAll(SCRIPTS_REGEX);
    scriptsList = [...scriptsList].map(item => path.resolve(entryPath, item[1]));

    return Promise.all(scriptsList.map(jsScriptName => readFile(jsScriptName)));
  })
  .then(scripts => {
    let data = scripts.reduce((acc, curr) => {
      return acc.concat(curr);
    }, '');

    return writeFile(bundleFileName, data);
  })
  .then(result => {
    if (result) {
      console.log('Bundle is created successfully');
    }
    let styleList = indexHtml.matchAll(/<link.*href="(.*)".*>$/igm);
    styleList = [...styleList].map(item => path.resolve(entryPath, item[1]));

    return Promise.all(styleList.map(styleSheetName => readFile(styleSheetName)));
  })
  .then(styleSheetList => {
    const styles = styleSheetList.reduce((acc, curr) => {
      return acc.concat(curr);
    }, '');

    return writeFile(stylesFileName, styles);
  })
  .then(result => {
    if (result) {
      console.log('Styles file is created successfully');
    }

    return readFile(template);
  })
  .then(data => {
    let newData = data.replace('{styles}', 'assets/css/styles.css');
    newData = newData.replace('{bundle}', 'assets/js/bundle.js');

    return writeFile(htmlFileName, newData);
  })
  .then(result => {
    if (result) {
      console.log('Index.html file is created successfully');
    }
  })
  .catch(err => {
    error(err.message);
  });
