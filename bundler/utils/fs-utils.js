const fs = require('fs');
const path = require('path');

exports.readFile = (fileName, options = 'utf-8') => new Promise((resolve, reject) => {
  fs.readFile(fileName, options, (err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data);
    }
  });
});

exports.writeFile = (fileName, data, options = 'utf-8') => new Promise((resolve, reject) => {
  const dirName = path.dirname(fileName);
  if (!fs.existsSync(dirName)) {
    try {
      fs.mkdirSync(dirName, { recursive: true });
    } catch (e) {
      reject(e);
    }
  }
  fs.writeFile(fileName, data, options, (err) => {
    if (err) {
      reject(err);
    } else {
      resolve(true);
    }
  });
});
