const path = require('path');
const fs = require('fs');

const error = require('./error-utils');
const CONFIG_REGEX = /^-config=/gi;

exports.getConfig = args => {
  const configArg = args.find(arg => CONFIG_REGEX.test(arg));
  if (!configArg) {
    error('Error: `-config` argument is required. Add `-config=path/to/config.js`');
  }
  const configRelativeFileName = configArg.replace(CONFIG_REGEX, '');
  const configFileName = path.resolve(__dirname, '..', '..', configRelativeFileName);
  if (!fs.existsSync(configFileName)) {
    error(`Error: file does not exist: ${configFileName}`);
  } else {
    return require(configFileName)();
  }
};
