const { createProxyMiddleware } = require('http-proxy-middleware');

const authProxy = createProxyMiddleware('/auth', {
  target: 'http://127.0.0.1:6080',
  changeOrigin: true,
});

const apiProxy = createProxyMiddleware('/api', {
  target: 'http://127.0.0.1:6080',
  changeOrigin: true,
});

module.exports = {
  port: 4001,
  browser: ['chrome'],
  server: {
    baseDir: './public',
    middleware: {
      10: authProxy,
      11: apiProxy,
    },
  }
};
