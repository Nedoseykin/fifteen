const path = require('path');

module.exports = () => {
  return {
    entry: path.resolve(__dirname, '..', 'public', 'index.html'),
    output: {
      path: path.resolve(__dirname, '..', 'server', 'public'),
      bundle: path.join('assets', 'js', 'bundle.js'),
      styles: path.join('assets', 'css', 'styles.css'),
      html: 'index.html',
    },
    template: path.resolve(__dirname, '..', 'template', 'index.html'),
  };
};
