document.addEventListener('DOMContentLoaded', start, false);

let eventEmitter;
let store;
let networkService;
let controller;

function start() {
  eventEmitter = new EventEmitter();
  store = new Store(reducer, initialState);
  networkService = new NetworkService();
  controller = new Controller().startListen();

  new App({ temp: 'temp' }).start();
}
