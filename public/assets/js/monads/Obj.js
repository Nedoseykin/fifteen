function obj(value) {
  return new Obj(value);
}

function Obj(value) {
  this.create(value);
}

Obj.prototype.create = function objCreate(value) {
  this.value = value || {};

  return this.value;
}

Obj.prototype.set = function objSet(model, value) {
  this.value[model] = value;

  return this.value;
};
