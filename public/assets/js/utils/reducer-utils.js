/**
 * Sets plates and adds new action to state
 * @param {Object} state
 * @param {Plate[]} plates
 * @returns {Object} New state
 */
function setPlates(state, plates) {
  const newState = Object.assign({}, state, {
    plates: plates,
  });
  return Object.assign({}, newState, {
    actions: addAction(newState),
  });
}

function setLayout(state, layout) {
  const newLayout = layout || getValidLayout(state.cellsInRow);

  return Object.assign({}, state, {
    plates: getPlates(newLayout),
    actions: [{ selectedValue: 1, matrix: newLayout }],
    canceledActions: [],
    gameMode: GAME_MODES.STARTED,
    layoutType: getTypeOfLayout(newLayout),
    isSaved: false,
  });
}

function moveSelection(state, deltaX, deltaY) {
  return state.gameMode === GAME_MODES.STARTED
    ? Object.assign({}, state, {
        selectedValue: moveSelectedValue(state, deltaX, deltaY),
      })
    : state;
}

function movePlatesValue(state, deltaX, deltaY) {
  if (state.gameMode === GAME_MODES.COMPLETED) {
    return state;
  }

  let plates = state.plates.slice();

  const selectedPlate = findByProp(plates, 'value', state.selectedValue);

  // get group
  const groupFilter = deltaX !== 0
    ? createFilterByProp('fieldY')
    : createFilterByProp('fieldX');
  const group = filterByProp(plates, groupFilter(selectedPlate));

  // get movable values
  const delta = deltaX !== 0 ? deltaX : deltaY;
  const field = deltaX !== 0 ? 'fieldX' : 'fieldY';
  const movableValues = getMovableValues(selectedPlate, group, field, delta);

  // change props in movable plates in plates
  movableValues.forEach(function(value) {
    const i = findIndexByProp(plates, 'value', value);
    const patch = {};
    patch[field] = plates[i].props[field] + delta;
    plates[i].setProps(patch);
  });

  const gameMode = (state.layoutType === LAYOUT_TYPE.MIXED)
    && (getTypeOfLayout(platesToMatrix(state.plates)) === LAYOUT_TYPE.IN_ORDER)
      ? GAME_MODES.COMPLETED
      : state.gameMode;

  // return plates;
  return Object.assign({}, state, {
    plates: plates,
    actions: movableValues.length > 0
      ? addAction(state)
      : state.actions,
    gameMode: gameMode,
  });
}

function addAction(state) {
  return state.actions.slice().concat([{
    selectedValue: state.selectedValue,
    matrix: platesToMatrix(state.plates),
  }]);
}

function undoAction(state) {
  let actions = state.actions;
  const canceledActions = state.canceledActions;
  if (actions.length > 1) {
    const canceled = getLast(actions);
    actions = actions.slice(0, actions.length - 1);
    canceledActions.push(canceled);
  }

  const selectedValue = doLastAction(actions, state.plates);

  return Object.assign({}, state, {
    selectedValue: selectedValue,
    actions: actions,
    canceledActions: canceledActions,
  });
}

function redoAction(state) {
  let canceledActions = state.canceledActions;
  const actions = state.actions;
  if (canceledActions.length > 0) {
    const renewed = getLast(canceledActions);
    canceledActions = canceledActions.slice(0, canceledActions.length - 1);
    actions.push(renewed);
  }

  const selectedValue = doLastAction(actions, state.plates);

  return Object.assign({}, state, {
    selectedValue: selectedValue,
    canceledActions: canceledActions,
    actions: actions,
  });
}

/**
 * Sets props to plates based on last action matrix
 * Matrix items should have proper indexes to calculate x and y
 * @param {Object[]} actions
 * @param {Plate[]} plates
 * @returns {number} Selected value
 */
function doLastAction(actions, plates) {
  const action = getLast(actions);
  const cellsInRow = Math.sqrt(action.matrix.length);

  if (action) {
    action.matrix
      .forEach(function(value, i) {
        if (value > 0) {
          const fieldY = Math.floor(i / cellsInRow);
          const fieldX = i - fieldY * cellsInRow;

          const plateIndex = findIndexByProp(plates, 'value', value);

          plates[plateIndex].setProps({
            value: value,
            fieldX: fieldX,
            fieldY: fieldY,
          });
        }
      });
  }

  return action && action.selectedValue;
}

function moveByClick(state, fieldCoordinates) {
  const selectedPlate = findItemByProps(state.plates, {
    fieldX: fieldCoordinates.x,
    fieldY: fieldCoordinates.y,
  })

  if (!selectedPlate) {
    return state;
  }

  return getMovedPlates(state, selectedPlate);
}

function getMovedPlates(state, selectedPlate) {
  let newState = Object.assign({}, state, {
    selectedValue: selectedPlate.props.value,
  });

  const options = [
    { deltaX: -1, deltaY: 0 }, // left
    { deltaX: 1, deltaY: 0 }, // right
    { deltaX: 0, deltaY: -1 }, // up
    { deltaX: 0, deltaY: 1 }, // down
  ];

  for (let i = 0; i < options.length; i++) {
    const deltaX = options[i].deltaX;
    const deltaY = options[i].deltaY;
    const actionsLength = newState.actions.length;
    newState = movePlatesValue(newState, deltaX, deltaY);
    if (newState.actions.length > actionsLength) {
      break;
    }
  }

  return newState;
}

function setLoadedGame(state, loadedGame) {
  let newState = Object.assign({}, state, {
    selectedValue: 1,
    plates: getPlates(loadedGame.start.matrix),
    actions: [{
      selectedValue: 1,
      matrix: loadedGame.start.matrix,
    }],
    canceledActions: [],
  });

  loadedGame.movements.forEach(function(value) {
    newState = getMovedPlates(newState, findByProp(newState.plates, 'value', value))
  });

  return newState;
}
