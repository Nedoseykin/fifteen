/**
 * Layout - Array of numbers. Length equals to square of `cellsInRow`
 * @typedef {number[]} Layout
 */

/**
 * Creates random layout
 * @param {number} cellsInRow
 * @returns {number[]} Random layout
 */
function createRandomLayout(cellsInRow) {
  const count = Math.pow(cellsInRow, 2);
  const layout = Array(count).fill(0);
  for (let i = 0; i < count - 1; i++) {
    let value = 0;
    do {
      value = randomFromRange(1, count - 1);
    } while(layout.includes(value));

    let j = 0;
    do {
      j = randomFromRange(0, count - 1);
    } while (layout[j] > 0);
    layout[j] = value;
  }

  return layout;
  // return [4, 12, 9, 3, 10, 8, 13, 2, 1, 7, 0, 14, 6, 11, 5, 15];
}

/**
 * Validates given layout whether it solvable or not
 * @param {number[]} layout
 * @returns {boolean} Validation result
 */
function validateLayout(layout) {
  const cellsInRow = Math.sqrt(layout.length);
  const k = Math.floor(layout.indexOf(0) / cellsInRow) + 1;

  const n = layout
    .filter(function(item) { return item > 0; })
    .reduce(function(acc, value, i, arr) {
      const inc = arr
        .slice(i + 1, arr.length)
        .filter(function(item) {
          return item < arr[i];
        }).length;

      return acc + inc;
    }, 0);

  return  cellsInRow % 2 === 0
    ? (n + k) % 2 === 0
    : n % 2 === 0;
}

/**
 * Returns random solvable layout
 * @param {number} cellsInRow
 * @returns {number[]} Solvable layout
 */
function getValidLayout(cellsInRow) {
  let layout = createRandomLayout(cellsInRow);

  while(!validateLayout(layout)) {
    const i = layout.indexOf(0);
    layout.splice(i, 1);
    const lastIndex = layout.length - 1;
    const beforeLastIndex = layout.length - 2;
    const last = getLast(layout);
    layout[lastIndex] = layout[beforeLastIndex];
    layout[beforeLastIndex] = last;
    layout.splice(i, 0, 0);
  }

  return layout;
}

function getHashFromLayout(layout) {
  return isFilledArray(layout) ? layout.join('-') : '';
}

function getLayoutFromHash(hash) {
  return isFilledString(hash)
    ? hash.split('-').map(function(value) { return parseInt(value, 10) })
    : [];
}

/* function getLastLayoutFromState(state) {
  const lastAction = getLast(state.actions) || { matrix: [] };

  return lastAction.matrix;
} */

function getFirstLayoutFromState(state) {
  const lastAction = getFirst(state.actions) || { matrix: [] };

  return lastAction.matrix;
}
