function isExist(v) {
  return v !== undefined && v !== null;
}

// strings
function isFilledString(str) {
  return (typeof str === 'string') && str.length > 0;
}

// arrays
function isFilledArray(arr) {
  return Array.isArray(arr) && arr.length > 0;
}

function getLast(arr) {
  return isFilledArray(arr) ? arr[arr.length - 1] : null;
}

function getFirst(arr) {
  return isFilledArray(arr) ? arr[0] : null;
}

function addIfNotIncluded(arr, item) {
  return arr.includes(item) ? arr : arr.concat([item]);
}

function removeItem(arr, item) {
  return arr.filter(function(arrItem) { return arrItem !== item });
}

function removeLast(arr) {
  return arr.slice(0, arr.length - 1);
}

// asynchronous
function delay(ms) {
  return new Promise(function(resolve) {
    setTimeout(resolve, ms);
  });
}

// classNames
function getClassNamesList(namesList) {
  const list = Array.isArray(namesList)
    ? namesList
    : [namesList];
  return list.map(function(name) {
    return [name, true];
  });
}

/**
 * Checker - checks if key is in item and item[key] value equals to filter[key]
 * @typedef {function} Checker
 * @param {string} key
 * @param {Object} item
 * @param {Object} filter
 * @returns {boolean} The result of checking
 */

/**
 * findIndex
 * @param {Array} arr
 * @param {Object} filter
 * @param {Checker|undefined} checker
 * @returns {number} Index of item in the array or -1
 */
function findIndex(arr, filter, checker) {
  if (!(isFilledArray(arr) && filter)) {
    return -1;
  }

  const keys = Object.keys(filter);

  for (let i = 0; i < arr.length; i++) {
    let test = true;

    for (let j = 0; j < keys.length; j++) {
      const key = keys[j];
      if (checker) {
        test = checker(key, arr[i], filter);
      } else {
        test = (key in arr[i]) && (filter[key] === arr[i][key]);
      }
      if (test === false) {
        break;
      }
    }

    if (test === true) {
      return i;
    }
  }

  return -1;
}

function findItem(arr, filter, checker) {
  const i = findIndex(arr, filter, checker);

  return i > -1 ? arr[i] : null;
}

function randomFromRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function platesToMatrix(plates) {
  const cellsInRow = Math.sqrt(plates.length + 1);

  return Array(Math.pow(cellsInRow, 2))
    .fill(0)
    .map(function(value, i) {
      const fieldY = Math.floor(i / cellsInRow);
      const fieldX = i - fieldY * cellsInRow;
      const plate = findItemByProps(
        plates,
        { fieldX: fieldX, fieldY: fieldY },
      );

      return plate ? plate.props.value : value;
    });
}
