/**
 * returns children of menu item by item`s id
 * @param {Array} items
 * @param {string} parentId
 * @returns {Array} Children of item by item`s id
 */
function getItemsByParentId(items, parentId) {
  if (!parentId) {
    return items;
  }

  let result = [];

  if (isFilledArray(items)) {
    for (let i = 0; i < items.length; i++) {
      if (items[i].id === parentId) {
        result = isFilledArray(items[i].children)
          ? items[i].children
          : [];
      } else if (Array.isArray(items[i].children)) {
        result = getItemsByParentId(items[i].children, parentId);
      }
      if (isFilledArray(result) || items[i].id === parentId) {
        break;
      }
    }
  }

  return result;
}
