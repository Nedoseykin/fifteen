/**
 * Layout - array of numbers
 * @typedef {number[]} Layout
 */

/**
 * Converts Layout to array of plates or creates array of plates in order
 * @param{Layout|undefined} layout
 * @returns {Plate[]} Array of plates
 */
function getPlates(layout) {
  const cellsInRow = store.getState().cellsInRow;
  let arr = layout
    || new Array(Math.pow(cellsInRow, 2) - 1).fill(null);

  const size = FIELD.SIZE / cellsInRow;

  return arr
    .map(function(item, i) {
      if (item === 0) {
        return null;
      }
      const value = item || i + 1;
      const y = Math.floor(i / cellsInRow);
      const x = i - cellsInRow * y;

      return new Plate({
        cx: x * size + size / 2,
        cy: y * size + size / 2,
        fieldX: x,
        fieldY: y,
        size: size,
        value: value,
      });
    })
    .filter(Boolean);
}

function moveSelectedValue(state, deltaX, deltaY) {
  let selectedValue = state.selectedValue;
  let plate = state.plates.find(function(item) {
    return item.props.value === selectedValue;
  });
  let x = plate.props.fieldX;
  let y = plate.props.fieldY;
  do {
    x = correctCoordinate(x + deltaX);
    y = correctCoordinate(y + deltaY);
    plate = state.plates.find(function(item) {
      return item && item.props.fieldX === x && item.props.fieldY === y;
    });
  } while (!plate);

  return plate.props.value;
}

function correctCoordinate(coordinate, checkEdge) {
  const cellsInRow = store.getState().cellsInRow;
  let result = coordinate;
  if (result < 0) {
    result = checkEdge ? 0 : cellsInRow - 1;
  }
  if (result > cellsInRow - 1) {
    result = checkEdge ? cellsInRow - 1 : 0;
  }

  return result;
}

function filterByProp(items, filter) {
  return items.filter(filter);
}

function findIndexByProp(items, field, value) {
  return items.findIndex(function(item) {
    return item.props[field] === value;
  });
}

function findByProp(items, field, value) {
  return items.find(function(item) {
    return item.props[field] === value;
  });
}

function createFilterByProp(prop) {
  return function(selectedItem) {
    return function(item) {
      return item.props[prop] === selectedItem.props[prop];
    }
  };
}

function propsChecker(key, item, filter) {
  return (key in item.props) && (item.props[key] === filter[key]);
}

function findItemByProps(arr, filter) {
  return findItem(arr, filter, propsChecker);
}

function getMovableValues(selectedPlate, group, field, delta) {
  let movableGroup = [selectedPlate];
  let nextCoordinate = selectedPlate.props[field];
  let nextPlate = null;

  do {
    nextPlate = findByProp(group, field, nextCoordinate + delta);
    if (nextPlate) {
      movableGroup.push(nextPlate);
      nextCoordinate = nextPlate.props[field];
    }
  } while (nextPlate);

  const lastPlate = getLast(movableGroup);
  const newCoordinate = correctCoordinate(lastPlate.props[field] + delta, true);

  return newCoordinate !== lastPlate.props[field]
    ? movableGroup.map(function(item) { return item.props.value })
    : [];
}

function getFullscreenElement() {
  return  document.fullscreenElement
    || document.mozFullScreenElement
    || document.msFullscreenElement
    || document.webkitFullscreenElement;
}

function toggleScreenMode() {
  const fullscreenElement = getFullscreenElement();

  if (!fullscreenElement) {
    document.documentElement
      .requestFullscreen()
      .then(() => {});
  } else {
    if (document.exitFullscreen) {
      document
        .exitFullscreen()
        .then(() => {});
    }
  }
}

/**
 * Action
 * @typedef {Object} Action
 * @property {number} selectedValue
 * @property {number[]} matrix
 */

/**
 * Encodes actions
 * @param {Action[]} actions
 * @returns {string} Encoded string
 */
function prepareToSave(actions) {
  const prepared = {
    start: actions[0],
    movements: actions
      .slice(1, actions.length)
      .map(function(action) { return action.selectedValue; }),
    end: getLast(actions),
  };

  return JSON.stringify(prepared);
}

/**
 * Returns a type of layout. Whether it is in order or mixed
 * @param {number[]} layout Layout
 * @returns {string} A type of layout
 */
function getTypeOfLayout(layout) {
  let result = LAYOUT_TYPE.IN_ORDER;

  for (let i = 0; i < layout.length; i++) {
    const value = layout[i];

    if (value > 0 && value !== i + 1) {
      result = LAYOUT_TYPE.MIXED;
      break;
    }
  }

  return result;
}
