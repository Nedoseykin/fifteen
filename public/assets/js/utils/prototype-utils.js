function getAncestor(descendant) {
  return Object.getPrototypeOf(descendant.prototype);
}
