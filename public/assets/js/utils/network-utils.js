function request(url, options) {
  const cbSuccess = options.cbSuccess;
  const cbError = options.cbError;
  const fetchOptions = {
    method: options.method || 'GET',
    headers: options.headers || {},
    body: options.body,
  };

  fetchOptions.headers.accepts = 'application/json';

  let responseOk;
  let responseStatus;

  return fetch(url, fetchOptions)
    .then(function(res) {
      responseOk = res.ok;
      responseStatus = res.status;
      const contentType = res.headers.get('content-type');
      const sessionToken = res.headers.get('session-token');

      if (contentType.includes('json')) {
        return res.json();
      }
      if (contentType.includes('text') && !sessionToken) {
        return res.text();
      }
      return Promise.resolve(sessionToken);
    })
    .then(loadedData => {
      if (!responseOk) {
        throw {
          status: responseStatus || 500,
          message: loadedData && loadedData.message || 'Network error',
        };
      }
      return cbSuccess && cbSuccess(loadedData);
    })
    .catch(err => {
      return cbError ? cbError(err) : console.error(err);
    });
}

function catchNetworkError(payload) {
  const _payload = payload || {};
  return function(err) {
    const state = store.getState();
    Object.keys(_payload).forEach(function(key) {
      state[key] = payload[key];
    });

    const errorTitle = 'Network request error';

    if (err.status === 401) {
      state.auth.sessionToken = '';
      localStorage.removeItem('sessionToken');
    }

    const modals = state.modals.includes(MODALS.ERROR_MESSAGE)
      ? state.modals
      : state.modals.concat([MODALS.ERROR_MESSAGE]);

    return Object.assign({}, state, {
      errorMessages: state.errorMessages.concat([{
        title: errorTitle,
        message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
      }]),
      modals: modals,
    });
  }
}

function setAuthData(state, payload) {
  return request(URLS.LOGIN, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
    cbSuccess: function(loadedData) {
      const sessionToken = typeof loadedData === 'string' ? loadedData : '';
      localStorage.setItem('login', payload.login);
      localStorage.setItem('sessionToken', sessionToken)

      return loadedData && loadedData.isValid === false
        ? Object.assign({}, state, {
          auth: {
            login: payload.login,
            validation: loadedData,
          },
        })
        : Object.assign({}, state, {
          modals: [],
          auth: {
            login: payload.login,
            sessionToken: loadedData,
          },
        });
    },
    cbError: function(err) {
      return Object.assign({}, state, {
        auth: {
          login: payload.login,
          sessionToken: '',
          error: err,
        },
        errorMessages: state.errorMessages.concat([{
          title: 'Error',
          message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
        }]),
        modals: state.modals.concat([MODALS.ERROR_MESSAGE]),
      });
    }
  });
}

function setRegisterData(state, payload) {
  return request(
    URLS.REGISTER,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
      cbSuccess: function (loadedData) {
        const sessionToken = typeof loadedData === 'string' ? loadedData : '';
        localStorage.setItem('login', payload.login);
        localStorage.setItem('sessionToken', sessionToken)

        return loadedData && loadedData.isValid === false
          ? Object.assign({}, state, {
            auth: {
              login: payload.login,
              email: payload.email,
              password: payload.password,
              confirmPassword: payload.confirmPassword,
              validation: loadedData,
            },
          })
          : Object.assign({}, state, {
            modals: [],
            auth: {
              login: payload.login,
              sessionToken: loadedData,
            },
          });
      },
      cbError: function (err) {
        return Object.assign({}, state, {
          auth: {
            login: payload.login,
            sessionToken: '',
            error: err,
          },
          errorMessages: state.errorMessages.concat([{
            title: 'Error',
            message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
          }]),
          modals: state.modals.concat([MODALS.ERROR_MESSAGE]),
        });
      },
    },
  )
}

function logout(state) {
  localStorage.removeItem('sessionToken');
  const sessionToken = state.auth.sessionToken;
  return request(URLS.LOGOUT, {
    method: 'PUT',
    headers: {
      'session-token': sessionToken,
    },
    cbSuccess: function() {
      const currState = store.getState();

      return Object.assign({}, currState, {
        auth: {
          login: currState.auth.login,
          sessionToken: '',
          error: null,
        },
        games: [],
      });
    },
    cbError: function(err) {
      return Object.assign({}, store.getState(), {
        auth: {
          login: state.auth.login,
          sessionToken: '',
          error: err,
        },
        errorMessages: state.errorMessages.concat([{
          title: 'Network error',
          message: `${err.status} - ${err.message} - ${new Date().toUTCString()}`,
        }]),
        modals: state.modals.concat([MODALS.ERROR_MESSAGE]),
      })
    },
  });
}

function saveGame(state, game) {
  const sessionToken = state.auth.sessionToken;
  const gameMode = state.gameMode;
  const layoutType = state.layoutType;
  const isSaved = state.isSaved;
  const isValid = gameMode === GAME_MODES.COMPLETED
    && layoutType === LAYOUT_TYPE.MIXED
    && isSaved === false;

  return isValid
    ? request(URLS.SAVE_GAME, {
      method: 'POST',
      headers: {
        'session-token': sessionToken,
        'Content-Type': 'application/json',
      },
      body: game,
      cbSuccess: function() {
        return Object.assign({}, store.getState(), {
          modals: [],
          isSaved: true,
        });
      },
      cbError: catchNetworkError(),
    })
    : state;
}

function getGamesByCurrentLayout(state) {
  const layout = isFilledArray(state.actions)
    ? state.actions[0].matrix
    : [];

  const hashBase64 = btoa(getHashFromLayout(layout));

  const url = `${URLS.GET_GAMES_BY_HASH}/${hashBase64}`;

  return request(url, {
    headers: {
      'session-token': state.auth.sessionToken,
    },
    cbSuccess: function(games) {
      return Object.assign({}, store.getState(), {
        games: games,
        gamesLoadStatus: 2
      })
    },
    cbError: catchNetworkError({
      games: [],
      gamesLoadStatus: 3,
    }),
  });
}

function getGamesBySize(state) {
  const url = `${URLS.GET_GAMES_BY_SIZE}/${state.cellsInRow}`;
  return request(url, {
    headers: {
      'session-token': state.auth.sessionToken,
    },
    cbSuccess: function(gallery) {
      return Object.assign({}, store.getState(), {
        gallery: gallery,
        galleryLoadStatus: 2,
      });
    },
    cbError: catchNetworkError({
      gallery: [],
      galleryLoadStatus: 3,
    }),
  });
}
