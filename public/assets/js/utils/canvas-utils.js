function roundedRect(context, cx, cy, width, height, radius, isFilled) {
  const x = cx - width / 2;
  const y = cy - height / 2;

  context.beginPath();
  context.moveTo(x, y + radius);
  context.lineTo(x, y + height - radius);
  context.arcTo(x, y + height, x + radius, y + height, radius);
  context.lineTo(x + width - radius, y + height);
  context.arcTo(x + width, y + height, x + width, y + height-radius, radius);
  context.lineTo(x + width, y + radius);
  context.arcTo(x + width, y, x + width - radius, y, radius);
  context.lineTo(x + radius, y);
  context.arcTo(x, y, x, y + radius, radius);
  context.closePath();
  context.stroke();
  if (isFilled) {
    context.fill();
  }
}

function getFieldCoordinates(clientX, clientY, element) {
  const cellsInRow = store.getState().cellsInRow;
  const rect = element.getBoundingClientRect();
  const x = clientX - rect.left;
  const y = clientY - rect.top;
  const size = rect.width / cellsInRow;
  const fieldX = Math.floor(x / size);
  const fieldY = Math.floor(y / size);

  return { x: fieldX, y: fieldY };
}

/**
 * Draw field with squares by hash in given context
 * @param {string} hash Hash of layout
 * @param {Object} ctx Context of canvas
 * @returns {void}
 */
function drawField(hash, ctx) {
  const layout = hash.split('-')
    .map(function(value) { return parseInt(value, 10); });
  const cellsInRow = Math.sqrt(layout.length);
  const size = 800 / cellsInRow;
  layout.forEach(function(value, i) {
    if (value > 0) {
      const row = Math.floor(i / cellsInRow)
      const cy = row * size + size / 2;
      const cx = (i - row * cellsInRow) * size + size / 2;
      ctx.fillStyle = 'rgba(255, 255, 255, 1)';
      roundedRect(ctx, cx, cy, size - 4, size - 4, 8, true);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.font = `bold ${size / 2}px sans-serif`;
      ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
      ctx.fillText(`${value}`, cx, cy + 2);
      const isProperPosition = value === i + 1;
      if (isProperPosition) {
        ctx.fillStyle = 'gold';
        ctx.fillText(`${value}`, cx - 2, cy);
      }
    }
  });
}
