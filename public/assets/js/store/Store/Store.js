/**
 * Dispatcher
 * @typedef Dispatcher
 * @property {number} id
 * @property {function} handler
 */

function Store(reducer, initialState) {
  this.configureStore(reducer, initialState)
}

Store.prototype.configureStore = function configureStore(reducer, initialState) {
  if (!reducer || (typeof reducer !== 'function')) {
    throw new Error('Reducer should be a function');
  }

  this.state = initialState || {};

  this.reducer = reducer;

  this.dispatchers = [];

  return this;
}

Store.prototype.getState = function storeGetState() {
  return this.state;
}

Store.prototype.dispatch = function storeDispatch(action) {
  // console.log('dispatch: action: ', action);
  if (action instanceof Promise) {
    action.then(resolvedAction => this.dispatch(resolvedAction));
  } else if (!('type' in action)) {
    return this.getState();
  } else if (action.type === AC_SET_STATE
    && action.payload instanceof Promise) {
    action.payload.then(resolvedPayload => {
      action.payload = resolvedPayload;
      this.dispatch(action);
    });
  }

  const newState = this.reducer(this.getState(), action);

  if (newState instanceof Promise) {
    newState.then(resolvedNewState => this.dispatch(acSetState(resolvedNewState)));
  } else {
    this.state = Object.assign({}, this.state, newState);
    this.dispatchers.forEach(function(dispatcher) {
      dispatcher.handler(this.state);
    }.bind(this));
  }

  return this;
}

Store.prototype.subscribe = function storeSubscribe(handler) {
  let id = 0;
  this.dispatchers.forEach(function(dispatcher) {
    id = Math.max(id, dispatcher.id);
  }.bind(this));
  id += 1;

  this.dispatchers.push({ id: id, handler: handler });

  handler(this.state);

  return function storeUnsubscribe() {
    this.dispatchers = this.dispatchers.filter(function(dispatcher) {
      return dispatcher.id !== id;
    });
  }.bind(this);
}
