function reducer(state, action) {
  const type = action.type;
  const payload = action.payload;

  switch (type) {
    case AC_SET_STATE:
      return Object.assign({}, state, payload);

    case AC_RESET_APP:
      return Object.assign({}, initialState, {
        cellsInRow: state.cellsInRow,
        modals: state.modals,
        auth: state.auth,
      });

    case AC_SET_PLATES:
      return setPlates(state, payload);

    case AC_SET_LAYOUT:
      return setLayout(state, payload);

    case AC_SELECTED_TO_LEFT:
      return moveSelection(state, -1, 0);

    case AC_SELECTED_TO_RIGHT:
      return moveSelection(state, 1, 0);

    case AC_SELECTED_TO_UP:
      return moveSelection(state, 0, -1);

    case AC_SELECTED_TO_DOWN:
      return moveSelection(state, 0, 1);

    case AC_PLATES_TO_LEFT:
      return movePlatesValue(state, -1, 0);

    case AC_PLATES_TO_RIGHT:
      return movePlatesValue(state, 1, 0);

    case AC_PLATES_TO_UP:
      return movePlatesValue(state, 0, -1);

    case AC_PLATES_TO_DOWN:
      return movePlatesValue(state, 0, 1);

    case AC_UNDO_ACTION:
      return undoAction(state);

    case AC_REDO_ACTION:
      return redoAction(state);

    case AC_MOVE_BY_CLICK:
      return moveByClick(state, payload);

    case AC_OPEN_MODAL:
      return Object.assign({}, state, {
        modals: addIfNotIncluded(state.modals, payload),
      });

    case AC_CLOSE_MODAL:
      return Object.assign({}, state, {
        modals: removeItem(state.modals, payload),
      });

    case AC_CLOSE_LAST_MODAL:
      return Object.assign({}, state, {
        modals: removeLast(state.modals),
      });

    case AC_CLOSE_ALL_MODALS:
      return Object.assign({}, state, {
        modals: [],
      });

    case AC_REMOVE_LAST_ERROR_MESSAGE: {
      const errorMessages = removeLast(state.errorMessages);
      const i = state.modals.indexOf(MODALS.ERROR_MESSAGE);
      const modals = state.modals.slice();
      if (!isFilledArray(errorMessages)) {
        modals.splice(i, 1);
      }

      return Object.assign({}, state, {
        errorMessages: errorMessages,
        modals: modals,
      });
    }

    case AC_SET_FIELD_SIZE:
      return Object.assign({}, state, {
        cellsInRow: payload,
      });

    case SET_LOADED_GAME:
      return setLoadedGame(state, payload);

    case SET_AUTH_LOAD_STATUS: {
      const auth = Object.assign({}, state.auth, {
        loadStatus: payload,
      });

      return Object.assign({}, state, {
        auth: auth,
      });
    }

    case SET_AUTH_DATA:
      return setAuthData(state, payload);

    case SET_REGISTER_DATA:
      return setRegisterData(state, payload);

    case AC_LOGOUT:
      return logout(state);

    case AC_SAVE_GAME:
      return saveGame(state, payload);

    case AC_GET_GAMES_BY_CURRENT_LAYOUT:
      return getGamesByCurrentLayout(state);

    case AC_SET_GAMES_LOAD_STATUS:
      return Object.assign({}, state, {
        gamesLoadStatus: payload,
      });

    case AC_GET_GALLERY:
      return getGamesBySize(state);

    case AC_SET_GALLERY_LOAD_STATUS:
      return Object.assign({}, state, {
        galleryLoadStatus: payload,
      });

    case AC_SET_IS_TOUCH_SCREEN:
      return Object.assign({}, state, {
        isTouchScreen: payload,
      });

    default:
      return state;
  }
}
