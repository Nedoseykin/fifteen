const AC_SET_STATE = 'AC_SET_STATE';
const AC_RESET_APP = 'AC_RESET_APP';
const AC_SET_PLATES = 'AC_SET_PLATES';
const AC_SET_LAYOUT = 'AC_SET_LAYOUT';
const AC_SELECTED_TO_LEFT = 'AC_SELECTED_TO_LEFT';
const AC_SELECTED_TO_RIGHT = 'AC_SELECTED_TO_RIGHT';
const AC_SELECTED_TO_UP = 'AC_SELECTED_TO_UP';
const AC_SELECTED_TO_DOWN = 'AC_SELECTED_TO_DOWN';
const AC_PLATES_TO_LEFT = 'AC_PLATES_TO_LEFT';
const AC_PLATES_TO_RIGHT = 'AC_PLATES_TO_RIGHT';
const AC_PLATES_TO_UP = 'AC_PLATES_TO_UP';
const AC_PLATES_TO_DOWN = 'AC_PLATES_TO_DOWN';
const AC_UNDO_ACTION = 'AC_UNDO_ACTION';
const AC_REDO_ACTION = 'AC_REDO_ACTION';
const AC_MOVE_BY_CLICK = 'AC_MOVE_BY_CLICK';
const AC_OPEN_MODAL = 'AC_OPEN_MODAL';
const AC_CLOSE_MODAL = 'AC_CLOSE_MODAL';
const AC_CLOSE_LAST_MODAL = 'AC_CLOSE_LAST_MODAL';
const AC_CLOSE_ALL_MODALS = 'AC_CLOSE_ALL_MODALS';
const AC_REMOVE_LAST_ERROR_MESSAGE = 'AC_REMOVE_LAST_ERROR_MESSAGE';
const AC_SET_FIELD_SIZE = 'AC_SET_FIELD_SIZE';
const SET_LOADED_GAME = 'SET_LOADED_GAME';
const SET_AUTH_LOAD_STATUS = 'SET_AUTH_LOAD_STATUS';
const SET_AUTH_DATA = 'SET_AUTH_DATA';
const SET_REGISTER_DATA = 'SET_REGISTER_DATA';
const AC_LOGOUT = 'AC_LOGOUT';
const AC_SAVE_GAME = 'AC_SAVE_GAME';
const AC_GET_GAMES_BY_CURRENT_LAYOUT = 'AC_GET_GAMES_BY_CURRENT_LAYOUT';
const AC_SET_GAMES_LOAD_STATUS = 'AC_SET_GAMES_LOAD_STATUS';
const AC_GET_GALLERY = 'AC_GET_GALLERY';
const AC_SET_GALLERY_LOAD_STATUS = 'AC_SET_GALLERY_LOAD_STATUS';
const AC_SET_IS_TOUCH_SCREEN = 'AC_SET_IS_TOUCH_SCREEN';

function acSetState(state) {
  return { type: AC_SET_STATE, payload: state };
}

function acResetApp() {
  return { type: AC_RESET_APP };
}

function acSetPlates(plates) {
  return { type: AC_SET_PLATES, payload: plates };
}

function acSetLayout(layout) {
  return { type: AC_SET_LAYOUT, payload: layout };
}

function acSelectedToLeft() {
  return { type: AC_SELECTED_TO_LEFT };
}

function acSelectedToRight() {
  return { type: AC_SELECTED_TO_RIGHT };
}

function acSelectedToUp() {
  return { type: AC_SELECTED_TO_UP };
}

function acSelectedToDown() {
  return { type: AC_SELECTED_TO_DOWN };
}

function acPlatesToLeft() {
  return { type: AC_PLATES_TO_LEFT };
}

function acPlatesToRight() {
  return { type: AC_PLATES_TO_RIGHT };
}

function acPlatesToUp() {
  return { type: AC_PLATES_TO_UP };
}

function acPlatesToDown() {
  return { type: AC_PLATES_TO_DOWN };
}

function acUndoAction() {
  return { type: AC_UNDO_ACTION };
}

function acRedoAction() {
  return { type: AC_REDO_ACTION };
}

function acSelectByCoordinate(x, y) {
  return { type: AC_MOVE_BY_CLICK, payload: { x: x, y: y } };
}

function acOpenModal(modalId) {
  return { type: AC_OPEN_MODAL, payload: modalId };
}

function acCloseModal(modalId) {
  return { type: AC_CLOSE_MODAL, payload: modalId };
}

function acCloseLastModal() {
  return { type: AC_CLOSE_LAST_MODAL };
}

function acCloseAllModals() {
  return { type: AC_CLOSE_ALL_MODALS };
}

function acRemoveLastErrorMessage() {
  return { type: AC_REMOVE_LAST_ERROR_MESSAGE };
}

function acSetFieldSize(size) {
  return { type: AC_SET_FIELD_SIZE, payload: size };
}

function acSetLoadedGame(loadedGame) {
  return { type: SET_LOADED_GAME, payload: loadedGame };
}

function acSetAuthLoadStatus(status) {
  return {
    type: SET_AUTH_LOAD_STATUS,
    payload: status,
  };
}

function acSetAuthData(login, password) {
  return {
    type: SET_AUTH_DATA,
    payload: {
      login: login,
      password: password,
    },
  };
}

function acSetRegisterData(login, email, password, confirmPassword) {
  return {
    type: SET_REGISTER_DATA,
    payload: {
      login: login,
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    },
  };
}

function acLogout() {
  return { type: AC_LOGOUT };
}

function acSaveGame(game) {
  return { type: AC_SAVE_GAME, payload: game };
}

function acGetGamesByCurrentLayout() {
  return { type: AC_GET_GAMES_BY_CURRENT_LAYOUT };
}

function acSetGamesLoadStatus(loadStatus) {
  return { type: AC_SET_GAMES_LOAD_STATUS, payload: loadStatus };
}

function acGetGallery() {
  return { type: AC_GET_GALLERY };
}

function acSetGalleryLoadStatus(loadStatus) {
  return { type: AC_SET_GALLERY_LOAD_STATUS, payload: loadStatus };
}

function acSetIsTouchScreen(isTouchScreen) {
  return { type: AC_SET_IS_TOUCH_SCREEN, payload: isTouchScreen };
}
