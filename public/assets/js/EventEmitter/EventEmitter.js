function EventEmitter() {
  this.handlers = {};
}

EventEmitter.prototype.emit = function eventEmitterEmit(eventName, value) {
  const eventHandlers = this.handlers[eventName];
  if (Array.isArray(eventHandlers)) {
    eventHandlers.forEach(function(handler) {
      handler(value);
    });
  }

  return this;
}

EventEmitter.prototype.addEventListener = function eventEmitterAddEventListener(eventName, handler) {
  if (!Array.isArray(this.handlers[eventName])) {
    this.handlers[eventName] = [handler];
  } else if (!this.handlers[eventName].includes(handler)) {
    this.handlers[eventName].push(handler);
  }

  return this;
}

EventEmitter.prototype.clear = function eventEmitterClear() {
  this.handlers = {};

  return this;
}
