function menuIcon(props) {
  const className = props.className;

  return `
    <svg
      class="icon${className ? ` ${className}` : ''}"
      width="32px"
      height="32px"
      viewBox="0 0 32 32"
      preserveAspectRatio="xMidYMid meet"
      fill="transparent"
      stroke="currentcolor"
      stroke-width="2"
      stroke-linejoin="round"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
    >
      <path d="M1 4 H31 M1 12 H31 M1 20 H31 M1 28 H31" />
    </svg>
  `;
}
