function spinnerIcon(props) {
  const className = props.className;

  return `
    <svg
      class="icon${className ? ` ${className}` : ''}"
      width="3rem"
      height="3rem"
      viewBox="0 0 48 48"
      preserveAspectRatio="xMidYMid meet"
      fill="transparent"
      stroke="currentcolor"
      stroke-width="4"
      stroke-linejoin="round"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
    >
      <path d="M2 22 A22 22 0 1 1 23 46" />
    </svg>
  `;
}
