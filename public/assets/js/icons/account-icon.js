function accountIcon(props) {
  const className = props.className;

  return `
    <svg
      class="icon${className ? ` ${className}` : ''}"
      width="32px"
      height="32px"
      viewBox="0 0 32 32"
      preserveAspectRatio="xMidYMid meet"
      fill="transparent"
      stroke="currentcolor"
      stroke-width="2"
      stroke-linejoin="round"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
    >
      <circle cx="15" cy="8" r="7" />
      <path d="M1 28 A15 10 0 0 1 31 28 M1 28 A15 3 0 0 0 31 28" />
    </svg>
  `;
}
