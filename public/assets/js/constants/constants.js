const REM = parseInt(getComputedStyle(document.documentElement)['font-size'], 10);

const FIELD = {
  CELLS_IN_ROW: 4,
  SIZE: 800,
};

const PLATE = {
  MAX_SPEED: 16,
};

const LAYOUT_TYPE = {
  IN_ORDER: 'IN_ORDER',
  MIXED: 'MIXED',
};

const GAME_MODES = {
  NOT_STARTED: 'NOT_STARTED',
  STARTED: 'STARTED',
  COMPLETED: 'COMPLETED',
};

const LOGIN_FORM_MODES = {
  LOGIN: 'LOGIN',
  REGISTRATION: 'REGISTRATION',
};
