const URLS = {
  LOGIN: '/auth/login',
  REGISTER: '/auth/register',
  LOGOUT: '/auth/logout',
  SAVE_GAME: '/api/fifteen/v1/game',
  GET_GAMES_BY_HASH: '/api/fifteen/v1/games/hash',
  GET_GAMES_BY_SIZE: '/api/fifteen/v1/games/size'
};
