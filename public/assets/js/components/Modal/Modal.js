/**
 * ModalProps
 * @typedef ModalProps
 * @property {string} id Id of the modal. Required for proper work
 * @property {boolean} isCentered
 * @property {Array} children
 * @property {boolean} isOpen
 * @property {function} onClose
 */

/**
 * Modal
 * @param {Object} props
 * @param {Array} children
 */
function Modal(props, children) {
  this.create(props, children);
}

Modal.prototype = Object.create(Component.prototype);

Modal.prototype.constructor = Modal;

Modal.prototype.create = function modalCreate(props, children) {
  if (!props.id) {
    throw new Error('Modal does not have required prop `id`');
  }
  const superclass = getAncestor(Modal);
  superclass.create.call(this, 'div', props, children);
  [
    ['modal', true],
    ['modal--centered', this.props.isCentered],
    ['modal--visible', this.props.isOpen],
  ]
    .concat(this.props.classNamesList)
    .filter(Boolean)
    .forEach(function(item) {
      if (item[1]) {
        this.element.classList.add(item[0]);
      }
    }.bind(this));

  this.element.addEventListener('click', function() {
    this.props.onClose && this.props.onClose();
    store.dispatch(acCloseModal(this.props.id));
  }.bind(this), false);

  document.getElementById('modal-root').append(this.element);

  const mapStateToProps = function(state) {
    const newProps = {
      isOpen: getLast(state.modals) === this.props.id,
    };

    this.render(newProps)
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this;
}

Modal.prototype.destroy = function modalDestroy() {
  this.element.removeEventListener('click', this.props.onClose);
  document.getElementById('modal-root').removeChild(this.element);
}

Modal.prototype.show = function modalShow() {
  this.element.classList.add('modal--visible');
}

Modal.prototype.hide = function modalHide() {
  this.element.classList.add('modal--is-hiding');
  delay(300)
    .then(function() {
      this.element.classList.remove('modal--visible');
      this.element.classList.remove('modal--is-hiding');
    }.bind(this));
}

Modal.prototype.render = function modalRender(props) {
  const newProps = props || { isOpen: this.props.isOpen };
  if (this.props.isOpen !== newProps.isOpen) {
    this.props = Object.assign({}, this.props, newProps);
    if (this.props.isOpen === true) {
      this.show();
    } else {
     this.hide();
    }
  }

  return this;
}
