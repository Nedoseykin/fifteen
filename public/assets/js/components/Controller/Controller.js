function Controller() {
  this.startListen = function startListen() {
    window.addEventListener('resize', handleResize, false);
    document.addEventListener('keydown', handleKeyDown, false);
    // document.addEventListener('fullscreenchange ', handleFullscreenChange, false);
    document.onfullscreenchange = handleFullscreenChange;
    document.addEventListener('touchend', handleTouchEnd, false);

    return this;
  }.bind(this);

  this.stopListen = function stopListen() {
    window.removeEventListener('resize', handleResize);
    document.removeEventListener('keydown', handleKeyDown);
    document.removeEventListener('fullscreenchange', handleFullscreenChange);

    return this;
  }

  function handleResize() {
    eventEmitter.emit(EVENTS.RESIZE_WINDOW);
  }

  function handleFullscreenChange() {
    const isFullscreen = getFullscreenElement();
    eventEmitter.emit(EVENTS.FULLSCREEN_CHANGE, Boolean(isFullscreen));
  }

  function handleTouchEnd() {
    document.removeEventListener('touchend', handleTouchEnd);
    store.dispatch(acSetIsTouchScreen(true));
  }

  function handleKeyDown(event) {
    event.stopPropagation();

    const keyCode = event.keyCode;
    const altKey = event.altKey;
    const ctrlKey = event.ctrlKey;

    if (event.isComposing || keyCode === 229) {
      return;
    }

    const isModal = isFilledArray(store.getState().modals);

    switch (true) {
      case keyCode === 37 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToLeft());
        break;

      case keyCode === 37 && !isModal:
        store.dispatch(acSelectedToLeft());
        break;

      case keyCode === 39 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToRight());
        break;

      case keyCode === 39 && !isModal:
        store.dispatch(acSelectedToRight());
        break;

      case keyCode === 38 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToUp());
        break;

      case keyCode === 38 && !isModal:
        store.dispatch(acSelectedToUp());
        break;

      case keyCode === 40 && ctrlKey === true && !isModal:
        store.dispatch(acPlatesToDown());
        break;

      case keyCode === 40 && !isModal:
        store.dispatch(acSelectedToDown());
        break;

      case keyCode === 90 && ctrlKey && !isModal:
        store.dispatch(acUndoAction());
        break;

      case keyCode === 89 && ctrlKey && !isModal:
        store.dispatch(acRedoAction());
        break;

      case keyCode === 77 && ctrlKey:
        store.dispatch(acOpenModal(MODALS.MAIN_MENU));
        break;

      case keyCode === 27:
        store.dispatch(acCloseLastModal());
        break;

      case keyCode === 186:
        console.log(store.getState());
        break;

      case keyCode === 17:
        // ctrl
        break;

      default:
        // console.log('document: key down: keyCode: ', keyCode);
    }
  }
}
