function Field(props, children) {
  this.create(props, children);

  const handleMouseDown = function mouseDown(e) {
    const fieldCoordinates = getFieldCoordinates(e.clientX, e.clientY, this.element);

    store.dispatch(acSelectByCoordinate(
      fieldCoordinates.x,
      fieldCoordinates.y,
    ));
  }.bind(this);

  this.element.addEventListener('mousedown', handleMouseDown, false);

  const mapStateToProps = function(state) {
    this.props.plates = state.plates;
    this.props.selectedValue = state.gameMode === GAME_MODES.STARTED
      ? state.selectedValue
      : 0;
  }.bind(this);

  store.subscribe(mapStateToProps);
}

Field.prototype = Object.create(Component.prototype);

Field.prototype.constructor = Field;

Field.prototype.create = function fieldCreate(props, children) {
  const superclass = Object.getPrototypeOf(Field.prototype);
  superclass.create.call(
    this,
    'canvas',
    Object.assign({ plates: [] }, props),
    children,
  );
  this.element.classList.add('field');
  [
    [ 'id', 'field' ],
    [ 'height', FIELD.SIZE ],
    [ 'width', FIELD.SIZE ],
  ]
    .forEach(function(attr) {
      this.element.setAttribute(attr[0], attr[1])
    }.bind(this));

  return this;
}

Field.prototype.render = function fieldRender() {
  const context = this.element.getContext('2d');

  const boundRenderer = function renderer() {
    const plates = this.props.plates;
    const selectedValue = this.props.selectedValue;

    context.clearRect(0, 0, FIELD.SIZE, FIELD.SIZE);
    if (Array.isArray(plates)) {
      plates.forEach(function(item) {
        item.setProps({ isSelected: selectedValue === item.props.value });
        if (item.render) {
          item.render(context, plates);
        }
      }.bind(this));
    }
    requestAnimationFrame(boundRenderer);
  }.bind(this);

  requestAnimationFrame(boundRenderer);
}
