function Fifteen(props = {}) {
  this.create(props);

  const resize = function() { this.resize(); }.bind(this);

  eventEmitter
    .addEventListener(EVENTS.RESIZE_WINDOW, resize);

  const mapStateToProps = function(state) {
    const games = state.games || [];
    const count = isFilledArray(state.actions)
      ? state.actions.length - 1
      : 0;

    const newProps = {
      count: count,
      bestResult: isFilledArray(games) ? games[0].count : 0,
      isTouchScreen: state.isTouchScreen,
    };

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);
}

Fifteen.prototype = Object.create(Page.prototype);

Fifteen.prototype.constructor = Fifteen;

Fifteen.prototype.create = function fifteenCreate(props) {
  const extendedProps = Object.assign(
    {}, props, { className: 'page-fifteen' },
  );
  const children = [
    getToolbar(),
    getMobileToolbar(),
    getContent(),
  ];
  getAncestor(Fifteen).create.call(this, extendedProps, children);

  this.props.field = new Field();
  this.element.querySelector('.main-content').append(
    this.props.field.getElement()
  );
  this.props.field.render();

  return this;

  function getToolbar() {
    const element = document.createElement('div');
    element.classList.add('main-toolbar');
    element.innerHTML = `
      <span class="main-toolbar-section main-menu">
        <button id="MainMenuButton" class="toolbar-button">
          ${menuIcon({
            className: 'main-menu-icon',
          })}
        </button>
      </span>
      <span class="main-toolbar-section best-result">
        <span id="toolbarBestCount" class="toolbar-info-item">
          The best: 0
        </span>
      </span>
      <span class="main-toolbar-section count">
        <span id="toolbarCount" class="toolbar-info-item">
          Count: 0
        </span>
      </span>
      <span class="main-toolbar-section account-menu">
        <button id="AccountButton" class="toolbar-button">
          ${accountIcon({
            className: 'account-icon',
          })}
        </button>
      </span>
    `;

    element.querySelector('#MainMenuButton')
      .addEventListener('click', function() {
        store.dispatch(acOpenModal(MODALS.MAIN_MENU));
      });

    element.querySelector('#AccountButton')
      .addEventListener('click', function() {
        store.dispatch(acOpenModal(MODALS.ACCOUNT_MENU));
      });

    return element;
  }

  function getMobileToolbar() {
    const element = document.createElement('div');
    element.classList.add('mobile-toolbar');
    element.innerHTML = `
      <span class="mobile-toolbar-section undo-redo">
        <button id="undoButton" class="toolbar-button mobile-toolbar-button">
          Undo
        </button>
        <button id="redoButton" class="toolbar-button mobile-toolbar-button">
          Redo
        </button>
      </span>
    `;

    [
      { id: 'undoButton', action: acUndoAction },
      { id: 'redoButton', action: acRedoAction },
    ]
      .forEach(function(item) {
        element.querySelector(`#${item.id}`)
          .addEventListener('click', function(e) {
            store.dispatch(item.action());
            e.target.blur();
          });
      });

    return element;
  }

  function getContent() {
    const element = document.createElement('div');
    element.classList.add('main-content');

    return element;
  }
}

Fifteen.prototype.resize = function fifteenResize() {
  const pageHeight = this.element.offsetHeight;
  const pageWidth = this.element.offsetWidth;
  const contentSection = this.element.querySelector('.main-content');

  const toolbar = this.element.querySelector('.main-toolbar');
  const toolbarMarginBottom = parseInt(getComputedStyle(toolbar)['margin-bottom'], 10);
  const deltaHeight = pageHeight - toolbar.offsetHeight - toolbarMarginBottom;
  const size = Math.min(deltaHeight, pageWidth);
  contentSection.style.width = `${size}px`;
  contentSection.style.height = `${size}px`;

  return this;
}

Fifteen.prototype.render = function fifteenRender(props) {
  this.props = Object.assign({}, this.props, props);

  this.element.querySelector('#toolbarCount')
    .innerHTML = `Count: ${this.props.count}`;

  const toolbarBestCount = this.element.querySelector('#toolbarBestCount');
  toolbarBestCount.innerHTML = `Best: ${this.props.bestResult}`;
  const action = this.props.bestResult > 0 ? 'remove' : 'add';
  toolbarBestCount.parentElement.classList[action]('toolbar-info-item--disabled');

  const mobileToolbarVisible = document.querySelector(
    '.mobile-toolbar--visible'
  );

  if (this.props.isTouchScreen && !mobileToolbarVisible) {
    document.querySelector('.mobile-toolbar').classList
      .add('mobile-toolbar--visible');
  }

  return this.resize();
}

Fifteen.prototype.getContext = function fifteenGetContext() {
  return document.getElementById('field').getContext('2d');
}
