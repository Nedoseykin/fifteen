function Page(props, children) {
  this.create(props, children);
}

Page.prototype = Object.create(Component.prototype);

Page.prototype.constructor = Page;

Page.prototype.create = function pageCreate(props, children) {
  const superclass = Object.getPrototypeOf(Page.prototype);
  superclass.create.call(this, 'div', props, children);
  this.element.classList.add('page');
  if (this.props.className) {
    this.element.classList.add(this.props.className);
  }

  const root = document.getElementById('app-root');
  root.innerHTML = '';
  root.appendChild(this.element);

  return this;
}

Page.prototype.render = function pageRender() {
  return this;
}
