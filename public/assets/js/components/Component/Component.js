function Component(tagName, props, children) {
  this.create(tagName, props, children);
}

Component.prototype.create = function componentCreate(tagName, props, children) {
  this.props = Object.assign({
    children: children || [],
  }, props || {});
  this.element = tagName
    ? document.createElement(tagName)
    : null;
  this.props.children.forEach(function(child) {
    switch (true) {
      case (child instanceof Component):
        this.element.append(child.getElement());
        break;

      case isExist(child):
        this.element.append(child);
        break;

      default:
    }
  }.bind(this));

  return this;
}

Component.prototype.getElement = function componentGetElement() {
  return this.element;
}

Component.prototype.render = function componentRender() {
  return this;
}

Component.prototype.setProps = function componentSetProps(props) {
  this.props = Object.assign({}, this.props, props);

  return this;
}

