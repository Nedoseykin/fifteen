/**
 * Plate
 * @param {Object} props Props
 * @property {number} cx Center X
 * @property {number} cy Center Y
 * @property {number} size Size of side
 * @property {number} value Value
 * @property {boolean} isSelected
 * @property {number} fieldX Coordinate X in field`s model
 * @property {number} fieldY Coordinate Y in field`s model
 */
function Plate(props) {
  this.create(props);
}

Plate.prototype = Object.create(Component.prototype);

Plate.prototype.constructor = Plate;

Plate.prototype.create = function plateCreate(props) {
  this.props = Object.assign({}, props);

  const mapStateToProps = function mapStateToProps(state) {
    const matrix = isFilledArray(state.actions)
      ? getLast(state.actions).matrix
      : null;

    this.props.isProperPosition = !!matrix
      && matrix[this.props.value - 1] === this.props.value;
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this;
}

Plate.prototype.render = function plateRender(context) {
  // calculation of final coords
  const cx = this.props.fieldX * this.props.size + this.props.size / 2;
  const cy = this.props.fieldY * this.props.size + this.props.size / 2;

  // calculation of next coords in canvas
  if (cx !== this.props.cx || cy !== this.props.cy) {
    let deltaX = cx - this.props.cx;
    let deltaY = cy - this.props.cy;
    deltaX = deltaX !== 0 ? deltaX / Math.abs(deltaX) * PLATE.MAX_SPEED : 0;
    deltaY = deltaY !== 0 ? deltaY / Math.abs(deltaY) * PLATE.MAX_SPEED : 0;
    const newCx = this.props.cx + deltaX;
    const newCy = this.props.cy + deltaY;

    let newDeltaX = cx - newCx;
    let newDeltaY = cy - newCy;
    newDeltaX = newDeltaX !== 0 ? newDeltaX / Math.abs(newDeltaX) * PLATE.MAX_SPEED : 0;
    newDeltaY = newDeltaY !== 0 ? newDeltaY / Math.abs(newDeltaY) * PLATE.MAX_SPEED : 0;

    this.props.cx = newDeltaX / deltaX > 0 ? newCx : cx;
    this.props.cy = newDeltaY / deltaY > 0 ? newCy : cy;
  }

  // gradient
  const gradient = context.createRadialGradient(
    this.props.cx,
    this.props.cy,
    this.props.size * 0.425,
    this.props.cx,
    this.props.cy,
    this.props.size * 0.375,
  );

  const fill0 = 'rgba(255, 255, 255, 0.8)';

  const fill1 = 'rgba(255, 255, 255, 1)';

  gradient.addColorStop(0, fill0);
  gradient.addColorStop(1, fill1);

  // context.fillStyle = gradient;

  context.fillStyle = this.props.isSelected
    ? gradient // 'rgba(255, 255, 255, 0.8)'
    : 'rgba(255, 255, 255, 1)';
  context.strokeStyle = 'rgba(0, 0, 0, 0.5)';
  roundedRect(
    context,
    this.props.cx,
    this.props.cy,
    this.props.size,
    this.props.size,
    8,
    true,
  );
  context.textAlign = 'center';
  context.textBaseline = 'middle';
  context.font = `bold ${this.props.size / 2}px sans-serif`;
  context.fillStyle = 'rgba(0, 0, 0, 0.5)';
  context.fillText(`${this.props.value}`, this.props.cx, this.props.cy + 2);
  if (this.props.isProperPosition) {
    context.fillStyle = 'gold';
    context.fillText(`${this.props.value}`, this.props.cx - 2, this.props.cy);
  }

  return this;
}

Plate.prototype.getElement = function() {
  return null;
}
