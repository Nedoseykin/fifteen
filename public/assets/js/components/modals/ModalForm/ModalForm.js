function ModalForm(props, children) {
  this.create(props, children);
}

ModalForm.prototype = Object.create(Modal.prototype);

ModalForm.prototype.constructor = ModalForm;

ModalForm.prototype.create = function modalFormCreate(props, children) {
  this.form = document.createElement('form');
  this.form.classList.add('modal-form__form');
  this.form.setAttribute('autocomplete', 'off');
  this.form.addEventListener('click', function(e) {
    e.stopPropagation();
  });
  this.form.name = props.name;

  if (isFilledArray(children)) {
    children.forEach(function(child) {
      this.form.append(child);
    }.bind(this));
  }

  const classNamesList = getClassNamesList(['modal-form'])
    .concat(props.classNamesList)
    .filter(Boolean);

  getAncestor(ModalForm).create.call(
    this,
    {
      id: props.id,
      isOpen: props.isOpen,
      isCentered: props.isCentered,
      classNamesList: classNamesList,
    },
    [this.form],
  );
}
