const mainMenuItems = [
  {
    id: 'SelectNewLayout',
    title: 'Select new layout',
    children: layoutsMenuItems.slice(),
  },
  {
    id: 'FieldSizeMenu',
    title: 'Change field size',
    children: fieldSizeItems,
  },
  {
    id: 'ChangeScreenMode',
    title: 'Fullscreen mode',
    onClick: function() {
      toggleScreenMode();
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
    }.bind(this),
  },
  /* {
    id: 'QuickSaveLocally',
    title: 'Quick save locally',
    onClick: function() {
      const payload = prepareToSave(store.getState().actions);
      console.log('Save game...', payload);
      localStorage.setItem(`${store.getState().cellsInRow}`, payload);
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
    },
  },
  {
    id: 'LoadLocallySaved',
    title: 'Load locally saved game',
    onClick: function() {
      try {
        let payload = JSON.parse(localStorage.getItem(`${store.getState().cellsInRow}`));
        console.log('Load last game: ', payload);
        store.dispatch(acSetLoadedGame(payload));
        store.dispatch(acCloseModal(MODALS.MAIN_MENU));
      } catch (e) {
        console.log('The last game for current field`s size was not found');
      }
    },
  }, */
  {
    id: 'HelpMenu',
    title: 'Help',
    children: helpMenuItems,
  },
];
