function toRootOfMenu(goTo) {
  goTo('');
}

function toItem(itemId) {
  return function(goTo) {
    goTo(itemId);
  }
}

function goBackItem(onClick) {
  return {
    id: 'Back',
    title: '..',
    className: 'modal-menu__list-item',
    onClick: onClick,
  };
}

function gotItAndCloseItem() {
  return {
    id: 'GotItAndClose',
    title: 'Got it!',
    className: 'modal-menu__list-item',
    onClick: function(goTo) {
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
      delay(200)
        .then(function() {
          toRootOfMenu(goTo);
        });
    },
  };
}