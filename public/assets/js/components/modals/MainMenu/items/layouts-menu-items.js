const layoutsMenuItems = [
  goBackItem(toRootOfMenu),
  {
    id: 'LayoutInOrder',
    title: 'Layout in order',
    className: 'modal-menu__list-item',
    onClick: function(goTo) {
      store.dispatch(acResetApp());
      store.dispatch(acSetPlates(getPlates()));
      store.dispatch(acCloseAllModals());
      delay(200)
        .then(function() { goTo(''); });
    },
  },
  {
    id: 'RandomLayout',
    title: 'Random layout',
    className: 'modal-menu__list-item',
    onClick: function(goTo) {
      const layout = getValidLayout(store.getState().cellsInRow);

      store.dispatch(acResetApp());
      store.dispatch(acSetLayout(layout));
      store.dispatch(acCloseAllModals());
      delay(200)
        .then(function() { goTo(''); });
    },
  }
];
