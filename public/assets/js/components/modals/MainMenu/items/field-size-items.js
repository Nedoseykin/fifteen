const fieldSizeItems = [goBackItem(toRootOfMenu)]
  .concat([3, 4, 5, 6, 8].map(function(count) {
    return {
      id: `size-${count}`,
      title: `${count} x ${count}`,
      className: 'modal-menu__list-item',
      onClick: function(goTo) {
        store.dispatch(acResetApp());
        store.dispatch(acSetFieldSize(count));
        goTo('SelectNewLayout');
      },
    };
  }));
