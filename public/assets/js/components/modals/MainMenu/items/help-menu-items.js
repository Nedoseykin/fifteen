const helpMenuItems = [
  goBackItem(toRootOfMenu),
  {
    id: 'Control',
    title: 'Control',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'Mouse',
        title: 'Mouse',
        className: 'modal-menu__list-item',
        children: [
          goBackItem(toItem('Control')),
          {
            id: 'AboutMouse',
            title: 'Click on square which you want to move. To move a group of squares - just click on a last one and it will push others in line.',
            className: 'modal-menu__text-item',
          },
          gotItAndCloseItem(),
        ],
      },
      {
        id: 'Keyboard',
        title: 'Keyboard',
        className: 'modal-menu__list-item',
        children: [
          goBackItem(toItem('Control')),
          {
            id: 'MoveSelection',
            title: 'Press key with <b>arrows</b> and select a square which you want to move.',
            className: 'modal-menu__text-item',
          },
          {
            id: 'MoveSquares',
            title: 'To move squares just hold <b>Ctrl</b> and press an <b>arrow</b>. You can move one squares or a group depends on selection.',
            className: 'modal-menu__text-item',
          },
          {
            id: 'Undo',
            title: 'To <b>Undo</b> / <b>Redo</b> movement - press <b>Ctrl-Z</b> / <b>Ctrl-Y</b>.',
            className: 'modal-menu__text-item',
          },
          gotItAndCloseItem(),
        ],
      },
    ],
  },
  {
    id: 'FieldSize',
    title: 'Field size',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'ChangeFieldSize',
        title: 'To change field size just select <b>Menu</b> -> <b>Change field size</b> and press button with necessary count of squares.',
        className: 'modal-menu__text-item',
      },
      gotItAndCloseItem(),
    ],
  },
  {
    id: 'Layout',
    title: 'Layout',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'ChangeLayout',
        title: 'To change layout just select <b>Menu</b> -> <b>Select new layout</b> and press button with layout in order or random one.',
        className: 'modal-menu__text-item',
      },
      {
        id: 'ChangeLayoutFromGallery',
        title: 'Registered users can select layout from gallery <b>Account menu</b> -> <b>Load layout from gallery</b> and click on you like one. '
          + 'Also they can save the result after game is completed <b>Account menu</b> -> <b>Save game</b>',
        className: 'modal-menu__text-item',
      },
      gotItAndCloseItem(),
    ],
  },
  {
    id: 'About',
    title: 'About the game',
    className: 'modal-menu__list-item',
    children: [
      goBackItem(toItem('HelpMenu')),
      {
        id: 'Goal',
        title: 'Move squares to get it in right order. Try to use minimal count of movements.',
        className: 'modal-menu__text-item',
      },
      {
        id: 'Wikipedia',
        title: '<a href="https://en.wikipedia.org/wiki/15_puzzle" target="_blank">15 Puzzle in Wikipedia</a>',
        className: 'modal-menu__text-item',
      },
      gotItAndCloseItem(),
    ],
  },
];
