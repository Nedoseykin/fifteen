function MainMenu() {
  this.create();
}

MainMenu.prototype = Object.create(ModalMenu.prototype);

MainMenu.prototype.constructor = MainMenu;

MainMenu.prototype.create = function mainMenuCreate() {
  getAncestor(MainMenu).create.call(this, {
    id: MODALS.MAIN_MENU,
    items: mainMenuItems,
    isFullscreen: Boolean(getFullscreenElement()),
    classNamesList: getClassNamesList(['main-menu']),
  });

  const changeFullscreen = function changeFullscreenMode(isFullscreen) {
    this.render({ isFullscreen: isFullscreen });
  }.bind(this);

  eventEmitter.addEventListener(EVENTS.FULLSCREEN_CHANGE, changeFullscreen);

  return this;
}

MainMenu.prototype.render = function mainMenuRender(props) {
  const superclass = getAncestor(MainMenu);
  superclass.render.call(this, props);

  const screenModeItem = this.element.querySelector('#ChangeScreenMode');
  if (screenModeItem) {
    screenModeItem.innerHTML = `${this.props.isFullscreen ? 'Windowed' : 'Fullscreen'} mode`;
  }

  return this;
}
