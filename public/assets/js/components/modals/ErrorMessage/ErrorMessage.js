function ErrorMessage(props) {
  this.create(props);
}

ErrorMessage.prototype = Object.create(ModalForm.prototype);

ErrorMessage.prototype.constructor = ErrorMessage;

ErrorMessage.prototype.create = function errorMessageCreate(props) {
  this.props = props || {};
  this.props.children = getChildren();

  getAncestor(ErrorMessage).create.call(
    this,
    {
      id: MODALS.ERROR_MESSAGE,
      classNamesList: getClassNamesList(['error-message']),
      isOpen: false,
      isCentered: true,
      name: 'errorMessageForm',
    },
    this.props.children,
  );

  const mapStateToProps = function(state) {
    const errorMessage = isFilledArray(state.errorMessages)
      ? getLast(state.errorMessages)
      : { title: 'Error', message: 'Something went wrong!' };
    const props = {
      title: errorMessage.title,
      message: errorMessage.message,
    };

    this.render(props);
  }.bind(this);

  store.subscribe(mapStateToProps);

  function getChildren() {
    const header = document.createElement('h3');
    header.classList.add('error-message__header');
    header.id = 'errorMessageHeader';
    // header.innerHTML = 'Error';

    const content = document.createElement('section');
    content.classList.add('error-message__content');

    const message = document.createElement('p');
    message.classList.add('modal-form__control');
    message.id = 'errorMessageContentText';
    // message.innerHTML = `Something went wrong!`;
    content.append(message);

    const toolbar = document.createElement('div');
    toolbar.classList.add('modal-form__toolbar');
    toolbar.innerHTML = `
      <button type="button" id="errorMessageGotItButton" class="form-toolbar-button">
        Got it!
      </button>
    `;
    toolbar.querySelector('#errorMessageGotItButton')
      .addEventListener('click', function() {
        store.dispatch(acRemoveLastErrorMessage());
      });

    return [header, content, toolbar];
  }
}

ErrorMessage.prototype.render = function errorMessageRender(props) {
  getAncestor(ErrorMessage).render.call(this, props);

  this.props = Object.assign({}, this.props, props);

  document.getElementById('errorMessageHeader')
    .innerHTML = this.props.title;

  document.getElementById('errorMessageContentText')
    .innerHTML = this.props.message;
}
