function Preloader() {
  this.create();
}

Preloader.prototype = Object.create(Component.prototype);

Preloader.prototype.constructor = Preloader;

Preloader.prototype.create = function preloaderCreate() {
  this.content = document.createElement('div');
  this.content.classList.add('preloader-content');
  this.content.innerHTML = `
    ${spinnerIcon({ className: 'preloader-icon' })}
    <span class="preloader-message">Fetching...</span>
  `;

  getAncestor(Preloader).create.call(
    this,
    'div',
    {
      id: MODALS.PRELOADER,
      isOpen: false,
      isCentered: true,
      classNamesList: getClassNamesList(['preloader', 'modal', 'modal--centered']),
    },
    [this.content],
  );

  this.props.classNamesList.forEach(function(item) {
    let action = 'add';
    if (item[1] === false) {
      action = 'remove';
    }
    this.getElement().classList[action](item[0]);
  }.bind(this));

  document.getElementById('modal-root')
    .append(this.getElement());

  const mapStateToProps = function(state) {
    const newProps = Object.assign({}, this.props, {
      isOpen: state.galleryLoadStatus === 1
        || state.gamesLoadStatus === 1
        || state.auth.loadStatus === 1,
    });

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this.render(this.props);
};

Preloader.prototype.show = function preloaderShow() {
  Modal.prototype.show.call(this);
};

Preloader.prototype.hide = function preloaderHide() {
  Modal.prototype.hide.call(this);
};

Preloader.prototype.render = function preloaderRender(props) {
  getAncestor(Preloader).render.call(this, props);

  if (this.props.isOpen !== props.isOpen) {
    this.props.isOpen = props.isOpen;
    if (this.props.isOpen) {
      this.show();
    } else {
      this.hide()
    }
  }
};
