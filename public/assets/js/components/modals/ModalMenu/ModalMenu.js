/**
 * ModalMenuProps
 * @typedef {Object} ModalMenuProps
 * @property {string} id Id of modal. Required
 * @property {boolean} isOpen
 * @property {boolean} isCentered
 * @property {string} className
 * @property {string[]} classNamesList
 * @property {function} onClose
 */
function ModalMenu(props) {
  this.create(props);
}

ModalMenu.prototype = Object.create(Modal.prototype);

ModalMenu.prototype.constructor = ModalMenu;

ModalMenu.prototype.create = function modalMenuCreate(props) {
  this.list = new ListMenu({
    className: 'modal-menu__list',
    items: [],
  }).render({
    items: props.items.map(function(item) {
      return Object.assign({}, item, {
        className: item.className || 'modal-menu__list-item',
        disabled: item.disabled === true,
      });
    }),
  });

  this.form = document.createElement('form');
  this.form.classList.add('modal-menu__form');
  this.form.append(this.list.element);
  this.form.addEventListener('click', function(e) {
    e.stopPropagation();
  });

  const classNamesList = getClassNamesList(['modal-menu'])
    .concat(props.classNamesList)
    .filter(Boolean);

  getAncestor(ModalMenu).create.call(
    this,
    {
      id: props.id,
      isOpen: false,
      isCentered: true,
      classNamesList: classNamesList,
    },
    [this.form],
  );
}
