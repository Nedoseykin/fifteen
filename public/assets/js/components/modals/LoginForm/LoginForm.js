function LoginForm() {
  this.create();
}

LoginForm.prototype = Object.create(ModalForm.prototype);

LoginForm.prototype.constructor = LoginForm;

LoginForm.prototype.create = function loginFormCreate() {
  this.props = {
    // mode: LOGIN_FORM_MODES.LOGIN,
    // children: getChildren(),
  };

  getAncestor(LoginForm).create.call(
    this,
    {
      id: MODALS.LOGIN_FORM,
      classNamesList: getClassNamesList(['login-form']),
      isOpen: false,
      isCentered: true,
      name: 'loginForm',
    },
    [],
    // this.props.children,
  );

  this.props.mode = LOGIN_FORM_MODES.LOGIN;

  const mapStateToProps = function(state) {
    const props = {
      validation: state.auth.validation || { login: '', password: '' },
    };
    this.render(props);
  }.bind(this);

  store.subscribe(mapStateToProps);
}

LoginForm.prototype.render = function loginFormRender(props) {
  getAncestor(LoginForm).render.call(this, props);

  this.props = Object.assign({}, this.props, props);

  function getFieldValue(form, field) {
    const _form = document.forms[form];

    return _form[field] ? _form[field].value : '';
  }

  const values = {
    login: getFieldValue('loginForm', 'login'),
    email: getFieldValue('loginForm', 'email'),
    password: getFieldValue('loginForm', 'password'),
    confirmPassword: getFieldValue('loginForm', 'confirmPassword'),
  };

  this.form.innerHTML = '';

  this.getChildren(values).forEach(function(child) {
    this.form.append(child);
  }.bind(this));

  ['login', 'email', 'password', 'confirmPassword'].forEach(function(id) {
    let elm = document.getElementById(`error-message-${id}`);
    if (elm) {
      elm.innerHTML = (this.props.validation && this.props.validation[id]) || '';
    }
  }.bind(this));
}

LoginForm.prototype.getChildren = function loginFormGetChildren(values) {
  const mode = this.props.mode;
  const LOGIN = LOGIN_FORM_MODES.LOGIN;
  const REGISTRATION = LOGIN_FORM_MODES.REGISTRATION;

  // content
  const content = document.createElement('div');
  content.classList.add('modal-form__content');

  // controls
  [
    { id: 'login', type: 'text', label: 'Login', value: values.login, modes: [LOGIN, REGISTRATION] },
    { id: 'email', type: 'text', label: 'e-mail', value: values.email, modes: [REGISTRATION] },
    { id: 'password', type: 'password', label: 'Password', value: values.password, modes: [LOGIN, REGISTRATION] },
    { id: 'confirmPassword', type: 'password', label: 'Confirm password', value: values.confirmPassword, modes: [REGISTRATION] },
  ]
    .filter(function (item) {
      return item.modes.includes(mode);
    })
    .forEach(function(item) {
      const elm = document.createElement('div');
      elm.classList.add('modal-form__control');
      elm.innerHTML = `
          <label for="${item.id}">${item.label}</label>
          <input type="${item.type}" id="${item.id}" name="${item.id}" value="${item.value}" placeholder="enter your ${item.id} here">
          <p id="error-message-${item.id}" class="control-error-message"></p>
        `;

      content.append(elm);
    });

  // toolbar
  const switchModeLabel = mode === LOGIN ? 'Registration' : 'Sign in';
  const toolbar = document.createElement('div');
  toolbar.classList.add('modal-form__toolbar');
  toolbar.innerHTML = `
      <span id="modeButton" class="form-toolbar-text-button">${switchModeLabel}</span>
      <button type="button" id="loginButton" class="form-toolbar-button">
        Submit
      </button>
    `;
  toolbar.querySelector('#modeButton').addEventListener('click', function() {
    this.props.mode = mode === LOGIN ? REGISTRATION : LOGIN;
    delay(100)
      .then(function() {
        this.render({});
      }.bind(this))
  }.bind(this));
  toolbar.querySelector('#loginButton').addEventListener('click', function() {
    const form = document.forms.loginForm;
    const login = form.login.value || '';
    const email = form.email ? form.email.value : '';
    const password = form.password.value || '';
    const confirmPassword = form.confirmPassword ? form.confirmPassword.value : '';
    form.password.value = '';
    if (form.confirmPassword) {
      form.confirmPassword.value = '';
    }
    if (mode === LOGIN) {
      store.dispatch(acSetAuthData(login, password));
    } else {
      store.dispatch(acSetRegisterData(
        login,
        email,
        password,
        confirmPassword,
      ));
    }
  });

  return [content, toolbar];
}
