function AccountMenu() {
  this.create();
}

AccountMenu.prototype = Object.create(ModalMenu.prototype);

AccountMenu.prototype.constructor = AccountMenu;

AccountMenu.prototype.create = function accountMenuCreate() {
  getAncestor(AccountMenu).create.call(this, {
    id: MODALS.ACCOUNT_MENU,
    items: accountMenuItems,
    isFullscreen: Boolean(getFullscreenElement()),
    classNamesList: getClassNamesList(['main-menu']),
  });

  const mapStateToProps = function(state) {
    const newProps = {
      login: state.auth.login || '',
      isLogged: isFilledString(state.auth.sessionToken),
      isReadyToSave: state.gameMode === GAME_MODES.COMPLETED
        && state.layoutType === LAYOUT_TYPE.MIXED
        && !state.isSaved,
      isGalleryAvailable: isFilledArray(state.gallery),
    };

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this.render({});
};

AccountMenu.prototype.render = function accountMenuRender(props) {
  getAncestor(AccountMenu).render.call(this, props);
  this.props = Object.assign({}, this.props, props);
  const isLogged = this.props.isLogged;
  const isReadyToSave = this.props.isReadyToSave;
  const login = this.props.login;
  const isGalleryAvailable = this.props.isGalleryAvailable;

  [
    {
      id: 'AccountMenuLogin',
      isVisible: !isLogged,
    },
    {
      id: 'AccountMenuRegister',
      isVisible: !isLogged,
    },
    {
      id: 'AccountMenuSaveInService',
      isVisible: isLogged && isReadyToSave,
    },
    {
      id: 'AccountMenuLoadFromGallery',
      isVisible: isLogged && isGalleryAvailable,
    },
    {
      id: 'AccountMenuLogout',
      isVisible: isLogged,
      title: `Logout: ${login}`,
    },
  ]
    .forEach(function(item) {
      const action = item.isVisible ? 'remove' : 'add';
      const li = this.element.querySelector(`#${item.id}`);
      li && li.classList[action]('list-menu__item--disabled');
      if (li && 'title' in item) {
        li.innerHTML = item.title;
      }
    }.bind(this));

  return this;
};
