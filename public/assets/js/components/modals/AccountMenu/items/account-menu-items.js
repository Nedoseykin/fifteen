const accountMenuItems = [
  {
    id: 'AccountMenuLogin',
    title: 'Login',
    onClick: function() {
      store.dispatch(acOpenModal(MODALS.LOGIN_FORM));
    }.bind(this),
  },
  {
    id: 'QuickSaveLocally',
    title: 'Quick save locally',
    onClick: function() {
      const payload = prepareToSave(store.getState().actions);
      console.log('Save game...', payload);
      localStorage.setItem(`${store.getState().cellsInRow}`, payload);
      store.dispatch(acCloseModal(MODALS.MAIN_MENU));
    },
  },
  {
    id: 'LoadLocallySaved',
    title: 'Load locally saved game',
    onClick: function() {
      try {
        let payload = JSON.parse(localStorage.getItem(`${store.getState().cellsInRow}`));
        console.log('Load last game: ', payload);
        store.dispatch(acSetLoadedGame(payload));
        store.dispatch(acCloseModal(MODALS.MAIN_MENU));
      } catch (e) {
        console.log('The last game for current field`s size was not found');
      }
    },
  },
  {
    id: 'AccountMenuSaveInService',
    title: 'Save game',
    onClick: function() {
      const payload = prepareToSave(store.getState().actions);
      store.dispatch(acSaveGame(payload));
      delay(100)
        .then(function() {
          store.dispatch(acSetGamesLoadStatus(0));
          return delay(100)
        })
        .then(function() {
          store.dispatch(acSetGalleryLoadStatus(0));
        })
    }.bind(this),
  },
  {
    id: 'AccountMenuLoadFromGallery',
    title: 'Load layout from gallery',
    onClick: function() {
      store.dispatch(acOpenModal(MODALS.GALLERY));
    },
  },
  {
    id: 'AccountMenuLogout',
    title: 'Logout',
    onClick: function() {
      store.dispatch(acLogout());
      store.dispatch(acCloseAllModals());
    }.bind(this),
  },
];
