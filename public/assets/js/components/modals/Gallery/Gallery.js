function Gallery() {
  this.create();
}

Gallery.prototype = Object.create(ModalMenu.prototype);

Gallery.prototype.constructor = Gallery;

Gallery.prototype.create = function galleryCreate() {
  getAncestor(Gallery).create.call(this, {
    id: MODALS.GALLERY,
    items: [],
    classNamesList: getClassNamesList(['modal-menu-gallery']),
  });

  this.form.classList.add('gallery-form');
  this.list.element.classList.add('gallery-list');

  const mapStateToProps = function(state) {
    const newProps = {
      gallery: state.gallery,
      isOpened: state.modals.includes(MODALS.GALLERY),
    };

    this.render(newProps);
  }.bind(this);

  store.subscribe(mapStateToProps);

  return this;
};

Gallery.prototype.render = function galleryRender(props) {
  getAncestor(Gallery).render.call(this, props);
  this.props = Object.assign({}, this.props, props);

  if (this.props.isOpened) {
    this.list.element.innerHTML = '';
    Array.isArray(this.props.gallery)
    && this.props.gallery.forEach(function(item) {
      const hash = item.hash;
      const items = item.items;
      const li = document.createElement('li');
      li.classList.add('gallery-item');
      li.dataset.hash = hash;
      // todo make constant for canvas size
      li.innerHTML = `
        <canvas width="800" height="800" class="gallery-item__field" />
      `;
      // todo separate handler
      li.addEventListener('click', function(e) {
        const liHash = e.currentTarget.dataset.hash;
        const layout = getLayoutFromHash(liHash);
        store.dispatch(acResetApp());
        store.dispatch(acSetLayout(layout));
        store.dispatch(acCloseAllModals());
      })
      const canvas = li.querySelector('canvas');
      const ctx = canvas.getContext('2d');

      drawField(hash, ctx);
      // todo make function for rectangle with text below
      const bestCount = items[0].count;
      const bestLogin = items[0].login;
      const cx = 800 / 2;
      const cy = 800 / 2;
      const fontSize = 800 * 0.05;
      ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
      roundedRect(ctx, cx, cy, 0.9 * 800, 0.25 * 800, 8, true);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.font = `bold ${fontSize}px sans-serif`;
      ctx.fillStyle = 'gold';
      ctx.fillText(`Games played: ${items.length}`, cx, cy - fontSize);
      ctx.fillText(`Best result: ${bestCount} by ${bestLogin}`, cx, cy + fontSize);

      this.list.element.append(li);
    }.bind(this));
  }
};
