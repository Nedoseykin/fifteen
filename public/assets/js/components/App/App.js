function App(props) {
  this.create(props);
}

App.prototype.create = function appCreate(props) {
  const newProps = props || {};
  this.props = Object.assign({}, newProps);

  return this;
}

App.prototype.start = function appStart() {
  this.props.page = new Fifteen();
  this.props.mainMenu = new MainMenu();
  this.props.accountMenu = new AccountMenu();
  this.props.loginForm = new LoginForm();
  this.props.gallery = new Gallery();
  this.props.errorMessages = new ErrorMessage();
  this.props.preloader = new Preloader();

  store.dispatch(acSetLayout());
}
