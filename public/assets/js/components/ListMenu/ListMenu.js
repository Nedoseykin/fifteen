/**
 * ListMenuItem
 * @typedef {Object} ListMenuItem
 * @property {string} className
 * @property {string} id
 * @property {string} title
 * @property {boolean} disabled
 * @property {function} onClick
 */

/**
 * ListMenuProps
 * @typedef {Object} ListMenuProps
 * @property {string} activeParentId
 * @property {string} className
 * @property {ListMenuItem[]|[]} items
 */

/**
 * ListMenu
 * @param {ListMenuProps} props
 */
function ListMenu(props) {
  this.create(props);
}

ListMenu.prototype = Object.create(Component.prototype);

ListMenu.prototype.constructor = ListMenu;

ListMenu.prototype.create = function listCreate(props) {
  getAncestor(ListMenu).create.call(this, 'ul', props, []);
  [
    ['list-menu', true],
    [this.props.className, Boolean(this.props.className)],
  ]
    .forEach(function(item) {
      if (item[1] === true) {
        this.element.classList.add(item[0]);
      }
    }.bind(this));

  return this;
}

ListMenu.prototype.render = function(props) {
  this.props = Object.assign({}, this.props, props);
  // console.log('=================================');
  // console.log('ListMenu: render: this.props', { ...this.props });

  const activeParentId = this.props.activeParentId;
  const items = this.props && isFilledArray(this.props.items)
    ? getItemsByParentId(this.props.items, activeParentId)
    : [];

  // console.log('ListMenu: render: items', [...items]);

  this.props.children = items.map(function(item) {
    const li = document.createElement('li');
    li.id = item.id;
    li.innerHTML = `${item.title}`;
    [
      ['list-menu__item', true],
      ['list-menu__item--disabled', item.disabled],
      [item.className, Boolean(item.className)],
    ]
      .forEach(function(className) {
        if (className[1]) {
          li.classList.add(className[0]);
        }
      }.bind(this));
    if (isFilledArray(item.children)) {
      li.addEventListener('click', function handleGroupMenuClick() {
        this.render({
          activeParentId: item.id,
        });
      }.bind(this));
    }
    if (typeof item.onClick === 'function') {
      const cbGoToItem = function goToItem(itemId) {
        this.render({
          activeParentId: itemId,
        })
      }.bind(this);
      li.addEventListener('click', function() {  item.onClick(cbGoToItem); });
    }
    return li;
  }.bind(this));

  this.element.innerHTML = '';
  this.props.children
    .forEach(function(item) {
      this.element.append(item);
    }.bind(this));

  // console.log('ListMenu: render: this.props', { ...this.props });
  // console.log('------------------------------------');

  return this;
}
