function NetworkService() {
  const layout = getFirstLayoutFromState(store.getState());

  this.props = {
    layout: layout,
    cellsInRow: store.getState().cellsInRow,
  };

  this.update = function(props) {
    this.props = Object.assign({}, this.props, props);

    let newState = null;

    if (this.props.galleryLoadStatusShouldBeReset) {
      newState = obj(newState).set('galleryLoadStatus',0);
    }

    if (this.props.gamesLoadStatusShouldBeReset) {
      newState = obj(newState).set('gamesLoadStatus', 0);
    }

    if (this.props.gamesShouldBeLoaded) {
      newState = obj(newState).set('gamesLoadStatus',1);
      store.dispatch(acGetGamesByCurrentLayout());
    }

    if (props.galleryShouldBeLoaded) {
      newState = obj(newState).set('galleryLoadStatus',1);
      store.dispatch(acGetGallery());
    }

    if (newState) {
      store.dispatch(acSetState(newState));
    }
  }.bind(this);

  const mapStateToProps = function(state) {
    const isLogged = isFilledString(state.auth.sessionToken);

    const layout = getFirstLayoutFromState(state);
    const previousHash = getHashFromLayout(this.props.layout);
    const currentHash = getHashFromLayout(layout);
    const isLayoutChanged = previousHash !== currentHash;

    const isSizeChanged = (state.cellsInRow !== this.props.cellsInRow)
      || (isLayoutChanged && !isFilledString(previousHash));

    const props = {
      cellsInRow: state.cellsInRow,
      gamesShouldBeLoaded: isLogged
        && isFilledArray(state.actions)
        && state.gamesLoadStatus === 0,
      gamesLoadStatusShouldBeReset: state.gamesLoadStatus > 0
        && (!isLogged || isLayoutChanged),
      galleryShouldBeLoaded: isLogged && state.galleryLoadStatus === 0,
      galleryLoadStatusShouldBeReset: isSizeChanged
        || (state.galleryLoadStatus > 0 && !isLogged),
      isLayoutChanged: isLayoutChanged,
      layout: layout,
    };

    this.update(props);
  }.bind(this);

  store.subscribe(mapStateToProps);
}
